#include "../key_verification_common.h"

#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>

#include <zlib.h>
#include <zconf.h>

#include "apmib.h"
#include "asym_key.h"
#include "uei_online_enc.h"


/*
 ********************************************************************************
 * Constants.
 ********************************************************************************
 */
const char key_table[256] = { 244,109,99,245,106,179,252,163,250,70,255,177,24,149,91,77,187,66,44,24,113,127,230,220,159,191,196,179,230,31,101,81,101,179,106,121,198,5,158,86,100,233,18,167,98,115,28,238,70,177,181,133,187,8,53,174,71,68,189,8,236,253,251,102,66,246,111,48,223,219,57,23,147,120,201,197,127,54,199,13,109,114,178,94,91,100,206,21,191,113,30,30,7,122,11,63,242,158,247,183,33,159,15,157,199,180,27,136,255,147,123,145,122,13,42,181,179,17,77,182,183,117,170,124,242,132,71,17,122,125,210,123,47,166,44,83,22,242,60,150,122,186,178,166,178,86,194,146,140,141,14,52,202,90,176,253,164,187,204,9,62,92,85,180,2,54,138,172,132,228,62,97,255,182,115,241,16,149,103,214,138,169,87,123,226,93,222,105,5,189,132,29,2,26,244,70,252,14,176,174,75,8,190,242,35,59,116,105,126,152,252,162,93,130,1,125,225,70,249,167,164,148,225,11,110,194,78,116,121,164,99,213,191,173,100,214,244,195,37,34,13,59,123,141,147,60,91,112,59,225,56,201,79,77,145,246 };/* randomly generated key table */

#define GET_MAC_ADDR_FROM_FLASH


/*
 ********************************************************************************
 * Function definitions.
 ********************************************************************************
 */
static int get_key_from_mac(char *out_buf)
{
    int i;
    int ret = 0;

#ifdef GET_MAC_ADDR_FROM_FLASH
    unsigned char mac[ENC_BUF_LEN + 1] = { 0 };

    apmib_get(MIB_HW_NIC0_ADDR, (void*)mac);
#else
    int fd;
    struct ifreq ifr;
    char *iface = "br0";
    unsigned char *mac = NULL;

    memset(&ifr, 0, sizeof(ifr));
    fd = socket(AF_INET, SOCK_DGRAM, 0);

    ifr.ifr_addr.sa_family = AF_INET;
    strncpy(ifr.ifr_name, iface, IFNAMSIZ - 1);

    if ((i = ioctl(fd, SIOCGIFHWADDR, &ifr)) != 0)
    {
        fprintf(stderr, "ioctl return value %d is not zero\n", i);
        ret = -1;
    }
    else
    {
        mac = (unsigned char*)ifr.ifr_hwaddr.sa_data;
#endif

        for (i = 0; i < 6; i++)
        {
            out_buf[i] = mac[i];
        }

        for (i = 6; i < 32; i++)
        {
            out_buf[i] = key_table[(unsigned char)out_buf[i - 6]];
        }
#ifndef GET_MAC_ADDR_FROM_FLASH
    }

    close(fd);
#endif

    return ret;
}

static char *decrypt_key(char *enced_key, int key_len)
{
    char enc_vector[ENC_VECTOR_LEN] = { 0 };
    char *decrypt_data_ptr = NULL;

    get_key_from_mac(enc_vector);

    if ((decrypt_data_ptr = (char*)malloc(ENC_BUF_LEN + 1)) == NULL)
    {
        fprintf(stderr, "Error: out of memory!\n");
    }
    else
    {
        memset(decrypt_data_ptr, 0, ENC_BUF_LEN);
        ueic_encrypt_with_vector_large(0, enced_key, key_len, enc_vector, decrypt_data_ptr, ENC_BUF_LEN);
    }

    return decrypt_data_ptr;
}

static char* uncompress_buffer(char *in_str, int in_len, int out_len)
{
    z_stream instream;
    instream.zalloc = Z_NULL;
    instream.zfree = Z_NULL;
    instream.opaque = Z_NULL;
    char *uncompressed_str = NULL;

    if ((uncompressed_str = (char*)malloc(out_len + 1)) == NULL)
    {
        fprintf(stderr, "Error: out of memory!\n");
    }
    else
    {
        instream.avail_in  = (uInt)in_len;
        instream.next_in   = (Bytef*)in_str;
        instream.avail_out = (uInt)(out_len);
        instream.next_out  = (Bytef*)uncompressed_str;

        inflateInit(&instream);
        inflate(&instream, Z_NO_FLUSH);
        inflateEnd(&instream);
    }

    return uncompressed_str;
}

static char *decrypt_secret()
{
    char tmp_ptr[ENC_BUF_LEN + 1] = { 0 };
    char *decrypt_ptr = NULL;
    char *uncompressed_ptr = NULL;

    apmib_get(MIB_UEI_KEY, tmp_ptr);

    decrypt_ptr = decrypt_key(tmp_ptr, ENC_BUF_LEN);

    if (decrypt_ptr != NULL)
    {
        uncompressed_ptr = uncompress_buffer(decrypt_ptr, ENC_BUF_LEN, MAX_IN_BUF_SZ);
        
        if (uncompressed_ptr == NULL)
        {
            fprintf(stderr, "Uncompress failed\n");
        }
            
        free(decrypt_ptr);
    }
    
    return uncompressed_ptr;
}

/*
 * Allocates a new string and copies src to it.
 *
 * @param src     The source string
 * @param src_len Length of the source string
 * @return A new copy of 'src' on success, else NULL.
 */
static char* string_dup(const char *src, size_t src_len)
{
    char *dest = NULL;

    if ((dest = (char*)malloc(sizeof(char) * (src_len + 1))) == NULL)
    {
        fprintf(stderr, "Error: out of memory!\n");
    }
    else
    {
        int i;

        strcpy(dest, src);

        for (i = 0; i < src_len; i++)
        {
            dest[i] &= 0x00ff;
        }
    }

    return dest;
}

/*
 * Reads the RSA key and certificate to verify from flash using apmib.
 *
 * @param key      [OUT] String where key will be written to
 * @param key_len  [OUT] Length of the key
 * @param cert     [OUT] String where cert will be written to
 * @param cert_len [OUT] Length of the cert
 * @return 0 on success, else -1.
 */
int read_RSA_key_and_certificate(char **key, size_t *key_len, char **cert, size_t *cert_len)
{
    char *decrypted_data = NULL;
    char *data_ptr = NULL;
    size_t str_len = 0;
    int ret = SUCCESS;

    if (apmib_init() == 0)
    {
        fprintf(stderr, "Error: apmib_init failed!\n");
        return ERROR_CODE;
    }

    decrypted_data = decrypt_secret();

    if (decrypted_data == NULL)
    {
        return ERROR_CODE;
    }

    /* Read data order (as strings)
     * 1. private RSA key
     * 2. public certificate
     * 3. host name
     */
    data_ptr = decrypted_data;
    str_len  = strlen(decrypted_data);

    if ((*key = string_dup(data_ptr, str_len)) == NULL)
    {
        fprintf(stderr, "Error: out of memory!\n");
        ret = ERROR_CODE;
    }
    else
    {
        *key_len = str_len;

        /* Move past the key and it's null terminator to the cert. */
        data_ptr = decrypted_data + str_len + 1;
        str_len  = strlen(decrypted_data);

        if ((*cert = string_dup(data_ptr, str_len)) == NULL)
        {
            fprintf(stderr, "Error: out of memory!\n");
            free(*key);
            ret = ERROR_CODE;
        }
        else
        {
            *cert_len = str_len;
        }
    }

    free(decrypted_data);

    return ret;
}

