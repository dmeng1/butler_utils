#ifndef __KEY_VERIFICATION_COMMON_H__
#define __KEY_VERIFICATION_COMMON_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/*
 ********************************************************************************
 * Constants.
 ********************************************************************************
 */
#define SUCCESS       0
#define NO_MATCH_CODE 1
#define ERROR_CODE    255

#endif /* __KEY_VERIFICATION_COMMON_H__ */

