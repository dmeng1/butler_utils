#include "key_verification_common.h"

#include <openssl/bio.h>
#include <openssl/pem.h>
#include <openssl/md5.h>
#include <openssl/x509.h>
#include <openssl/evp.h>


/*
 ********************************************************************************
 * Constants.
 ********************************************************************************
 */
#define MD5_ARG_IDX    1
#define MODULUS_PREFIX "Modulus="


/*
 ********************************************************************************
 * Macros.
 ********************************************************************************
 */
/* Compatibility macros. */
#if OPENSSL_VERSION_NUMBER < 0x10100000
#define EVP_PKEY_get0_RSA(ssl_pkey) \
    ssl_pkey->pkey.rsa
#define RSA_get0_key(rsa, n_ptr, e_not_used, d_not_used) \
    *n_ptr = rsa->n
#endif


/*
 ********************************************************************************
 * Externs.
 ********************************************************************************
 */
/*
 * Reads the RSA key and certificate to verify. This is to be defined depending
 * on how they will be read.
 *
 * @param key      [OUT] String where key will be written to
 * @param key_len  [OUT] Length of the key
 * @param cert     [OUT] String where cert will be written to
 * @param cert_len [OUT] Length of the cert
 * @return 0 on success, else -1.
 */
int read_RSA_key_and_certificate(char **key, size_t *key_len, char **cert, size_t *cert_len);


/*
 ********************************************************************************
 * Structures.
 ********************************************************************************
 */
/* Holds the SSL key. This data is used to create an md5. */
typedef struct
{
    int key_src;
    union
    {
        RSA      *rsa;
        EVP_PKEY *pkey;
    } key_ptr;
} ssl_key_type_t;

/* The supported sources of the key. */
enum
{
    KEY_VERIFY_KEY_SRC_RSA,
    KEY_VERIFY_KEY_SRC_CERT,

    KEY_VERIFY_KEY_SRC_END,
};


/*
 ********************************************************************************
 * Function definitions.
 ********************************************************************************
 */
/*
 * Initializes the SSL data structure.
 *
 * @param ssl_key The SSL data structure
 * @param data    The data to create the key with
 * @param key_src The source of the key data
 * @return 0 on success, else -1.
 */
static int ssl_key_type_init(ssl_key_type_t *ssl_key, const char* data, int key_src)
{
    BIO *bio;
    int ret = SUCCESS;

    if (ssl_key == NULL || data == NULL)
    {
        fprintf(stderr, "Error: invalid arguments! NULL pointers provided!\n");
        return ERROR_CODE;
    }

    bio = BIO_new(BIO_s_mem());
    BIO_puts(bio, data);
    ssl_key->key_src = key_src;

    switch (ssl_key->key_src)
    {
        case KEY_VERIFY_KEY_SRC_RSA:
        {
            if ((ssl_key->key_ptr.rsa = PEM_read_bio_RSAPrivateKey(bio, NULL, NULL, NULL)) == NULL)
            {
                fprintf(stderr, "Error: could not load RSA key!\n");
                ret = ERROR_CODE;
            }

            break;
        }
        case KEY_VERIFY_KEY_SRC_CERT:
        {
            X509 *cert = NULL;

            if ((cert = PEM_read_bio_X509(bio, NULL, NULL, NULL)) == NULL)
            {
                fprintf(stderr, "Error: could not load certificate!\n");
                ret = ERROR_CODE;
            }
            else
            {
                if ((ssl_key->key_ptr.pkey = X509_get_pubkey(cert)) == NULL)
                {
                    fprintf(stderr, "Error: could not load certificate!\n");
                    ret = ERROR_CODE;
                }

                X509_free(cert);
            }

            break;
        }
        default:
        {
            fprintf(stderr, "Error: invalid SSL key source %d!\n", ssl_key->key_src);
            ret = ERROR_CODE;
            break;
        }
    }

    BIO_free_all(bio);

    return ret;
}

/*
 * Resets the SSL data stored in the provided ssl_key_type_t.
 *
 * @param ssl_key The SSL data structure
 * @return 0 on success, else -1.
 */
static int ssl_key_type_reset(ssl_key_type_t *ssl_key)
{
    int ret = SUCCESS;

    if (ssl_key == NULL)
    {
        fprintf(stderr, "Error: invalid arguments! NULL pointers provided!\n");
        return ERROR_CODE;
    }

    switch (ssl_key->key_src)
    {
        case KEY_VERIFY_KEY_SRC_RSA:
        {
            RSA_free(ssl_key->key_ptr.rsa);
            break;
        }
        case KEY_VERIFY_KEY_SRC_CERT:
        {
            EVP_PKEY_free(ssl_key->key_ptr.pkey);
            break;
        }
        default:
        {
            fprintf(stderr, "Error: invalid SSL key source %d!\n", ssl_key->key_src);
            ret = ERROR_CODE;
            break;
        }
    }

    return ret;
}

/*
 * Gets the modulus from the SSL data and converts it to a hex string.
 *
 * @param ssl_key      The SSL data structure
 * @param str_ptr      [OUT] The pre-allocated hex string of the modulus
 * @param str_size_ptr [OUT] The size of the hex string
 * @return 0 on success, else -1.
 */
static int ssl_key_type_get_modulus_as_hex_str(ssl_key_type_t *ssl_key, char **str_ptr, int *str_size_ptr)
{
    RSA *rsa = NULL;
    const BIGNUM *rsa_modulus = NULL;
    int mod_size;
    char *mod_hex = NULL;
    int i;

    if (ssl_key == NULL || str_ptr == NULL || str_size_ptr == NULL)
    {
        fprintf(stderr, "Error: invalid arguments! NULL pointers provided!\n");
        return ERROR_CODE;
    }

    /* Get RSA according to SSL type stored. */
    switch (ssl_key->key_src)
    {
        case KEY_VERIFY_KEY_SRC_RSA:
        {
            rsa = ssl_key->key_ptr.rsa;
            break;
        }
        case KEY_VERIFY_KEY_SRC_CERT:
        {
            /* Only supporting RSA public key. */
            if (EVP_PKEY_id(ssl_key->key_ptr.pkey) != EVP_PKEY_RSA)
            {
                fprintf(stderr, "Error: unsupported public key type!\n");
                return ERROR_CODE;
            }
            else
            {
                rsa = EVP_PKEY_get0_RSA(ssl_key->key_ptr.pkey);
                break;
            }
        }
        default:
        {
            fprintf(stderr, "Error: invalid SSL key source %d!\n", ssl_key->key_src);
            return ERROR_CODE;
        }
    }

    if (rsa == NULL)
    {
        fprintf(stderr, "Error: RSA key is NULL!\n");
        return ERROR_CODE;
    }

    RSA_get0_key(rsa, &rsa_modulus, NULL, NULL);
    mod_size = BN_num_bytes(rsa_modulus);

    if (mod_size <= 0)
    {
        fprintf(stderr, "Error: invalid modulus size in RSA key!\n");
        return ERROR_CODE;
    }

    unsigned char mod_buff[mod_size];

    if (BN_bn2bin(rsa_modulus, mod_buff) != mod_size)
    {
        fprintf(stderr, "Error: BN_bn2bin failed!\n");
        return ERROR_CODE;
    }

    int prefix_size = strlen(MODULUS_PREFIX);
    /* prefix + modulus size as byte pairs + new line */
    int mod_hex_size = prefix_size + mod_size * 2 + 1;

    mod_hex = (char*)malloc(sizeof(char) * (mod_hex_size + 1));

    if (mod_hex == NULL)
    {
        fprintf(stderr, "Error: out of memory!\n");
        return ERROR_CODE;
    }

    strcpy(mod_hex, "Modulus=");
    for (i = 0; i < mod_size; i++)
    {
        sprintf(&mod_hex[prefix_size + i * 2], "%02X", mod_buff[i]);
    }
    strcat(mod_hex, "\n");

    *str_ptr      = mod_hex;
    *str_size_ptr = mod_hex_size;

    return SUCCESS;
}

/*
 * Generates the md5 value of the provided RSA key.
 *
 * @param ssl_key   Key to create the md5 for
 * @param hex_value [OUT] Hex value of the key's md5
 * @return 0 on success, else -1.
 */
static int generate_md5_for_key(ssl_key_type_t *ssl_key, char hex_value[])
{
    char *mod_hex = NULL;
    int mod_hex_size;
    int ret = SUCCESS;
    int i;

    if (ssl_key == NULL || hex_value == NULL)
    {
        fprintf(stderr, "Error: invalid arguments! NULL pointers provided!\n");
        return ERROR_CODE;
    }

    if (ssl_key_type_get_modulus_as_hex_str(ssl_key, &mod_hex, &mod_hex_size) != SUCCESS)
    {
        ret = ERROR_CODE;
    }
    else
    {
        unsigned char result[MD5_DIGEST_LENGTH];

        MD5((unsigned char*)mod_hex, mod_hex_size, result);

        for (i = 0; i < MD5_DIGEST_LENGTH; i++)
        {
            sprintf(&hex_value[i * 2], "%02x", result[i]);
        }

        free(mod_hex);
    }

    return ret;
}

/*
 * Usage print.
 */
static void usage()
{
    printf("Usage: key_verification md5\n");
    printf("where:\n");
    printf("\tmd5\n");
    printf("\t\tThe calculated md5 to compare against the stored RSA private key and public cert.\n");
    printf("\t\tThe md5 is expected to be all lowercase.\n");
    printf("Exit Codes:\n");
    printf("\t%3s - md5's match\n", "0");
    printf("\t%3s - md5's differ\n", "1");
    printf("\t%3s - error\n", "255");
}

/*
 * Main.
 */
int main(int argc, char *argv[])
{
    char *key = NULL;
    size_t key_len = 0;
    char *cert = NULL;
    size_t cert_len = 0;
    int ret = SUCCESS;

    if (argc != 2)
    {
        fprintf(stderr, "Error: wrong number of arguments!\n");
        usage();
        ret = ERROR_CODE;
    }
    else if (read_RSA_key_and_certificate(&key, &key_len, &cert, &cert_len) != SUCCESS)
    {
        ret = ERROR_CODE;
    }
    else
    {
        ssl_key_type_t rsa_key = { 0 };
        ssl_key_type_t cert_key = { 0 };
        char key_md5_value[MD5_DIGEST_LENGTH * 2 + 1];
        char cert_md5_value[MD5_DIGEST_LENGTH * 2 + 1];

        if (ssl_key_type_init(&rsa_key,  key,  KEY_VERIFY_KEY_SRC_RSA)  == SUCCESS &&
            ssl_key_type_init(&cert_key, cert, KEY_VERIFY_KEY_SRC_CERT) == SUCCESS)
        {
            if (generate_md5_for_key(&rsa_key, key_md5_value) != SUCCESS)
            {
                ret = ERROR_CODE;
            }
            else if (generate_md5_for_key(&cert_key, cert_md5_value) != SUCCESS)
            {
                ret = ERROR_CODE;
            }
            else if (strlen(key_md5_value) != strlen(argv[MD5_ARG_IDX]) ||
                strlen(key_md5_value) != strlen(cert_md5_value)         ||
                strcmp(key_md5_value, argv[MD5_ARG_IDX]) != 0           ||
                strcmp(key_md5_value, cert_md5_value) != 0)
            {
                fprintf(stderr, "Error: md5's do not match!\n");
                fprintf(stderr, "Received:           %s\n", argv[MD5_ARG_IDX]);
                fprintf(stderr, "Generated for Key:  %s\n", key_md5_value);
                fprintf(stderr, "Generated for Cert: %s\n", cert_md5_value);
                ret = NO_MATCH_CODE;
            }
            else
            {
                printf("md5's match!\n");
            }
        }

        /* Clean up. */
        ssl_key_type_reset(&rsa_key);
        ssl_key_type_reset(&cert_key);
        free(key);
        free(cert);
    }

    return ret;
}

