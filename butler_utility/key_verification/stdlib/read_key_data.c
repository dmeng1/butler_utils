#include "../key_verification_common.h"


/*
 ********************************************************************************
 * Constants.
 ********************************************************************************
 */
#define RSA_KEY_FILE "private_clear.key"
#define CERT_FILE    "public.cert"


/*
 ********************************************************************************
 * Function definitions.
 ********************************************************************************
 */
/*
 * Reads data from a given file and provides it in newly allocated string form.
 *
 * @param file_name Name of the file to read from
 * @param data      [OUT] String where data will be written to
 * @param data_len  [OUT] Length of the data
 * @return 0 on success, else -1.
 */
static int read_data_from_file(const char *file_name, char **data, size_t *data_len)
{
    FILE *fptr = NULL;
    unsigned int size;
    int ret = SUCCESS;

    if ((fptr = fopen(file_name, "r")) == NULL)
    {
        fprintf(stderr, "Error: could not open %s\n", file_name);
        return ERROR_CODE;
    }

    fseek(fptr, 0, SEEK_END);
    size = ftell(fptr);
    fseek(fptr, 0, SEEK_SET);

    if ((*data = (char*)malloc(sizeof(char) * (size + 1))) == NULL)
    {
        fprintf(stderr, "Error: out of memory!\n");
        ret = ERROR_CODE;
    }
    else
    {
        fread(*data, 1, size, fptr);
        (*data)[size] = '\0';
        *data_len = size;
    }

    fclose(fptr);

    return ret;
}

/*
 * Reads the RSA key and certificate to verify from RSA_KEY_FILE and CERT_FILE,
 * respectively.
 *
 * @param key      [OUT] String where key will be written to
 * @param key_len  [OUT] Length of the key
 * @param cert     [OUT] String where cert will be written to
 * @param cert_len [OUT] Length of the cert
 * @return 0 on success, else -1.
 */
int read_RSA_key_and_certificate(char **key, size_t *key_len, char **cert, size_t *cert_len)
{
    int ret = SUCCESS;

    if (key == NULL || key_len == NULL || cert == NULL || cert_len == NULL)
    {
        fprintf(stderr, "Error: invalid arguments! NULL pointers provided!\n");
        ret = ERROR_CODE;
    }
    else if ((ret = read_data_from_file(RSA_KEY_FILE, key, key_len)) != SUCCESS)
    {
        fprintf(stderr, "Error: read RSA key data!\n");
    }
    else if ((ret = read_data_from_file(CERT_FILE, cert, cert_len)) != SUCCESS)
    {
        fprintf(stderr, "Error: read certificate data!\n");
    }

    return ret;
}

