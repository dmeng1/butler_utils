#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <mtd/mtd-user.h>
#include <unistd.h>


#include "uei_image.h"

#define MB 0x100000
#define FLASH_BLOCK_FILE "block_file.bin"


//#define __DEBUG__ 	1 

int endian_adjust(int input)
{
	int out1, out2;

	out1 =(input >> 16);
	out1 = (out1 >> 8)|(out1 & 0xff) << 8;

	out2 = (input & 0xffff);
        out2 = (out2 >> 8) | (out2 & 0xff) << 8;
	return (out2 << 16| out1);
}

void read_flash(char * dev_name, int flash_offset, unsigned char *buffer, int length)
{
	int fm, i, j, flash_len;

        fm = open(dev_name, O_RDWR);
	if(fm <= 0){
	   printf("open %s fail\n", dev_name);
	   return; 
	}
	
	lseek(fm,flash_offset, SEEK_SET);

	if(buffer == NULL){
	    printf("No memory buffer\n");
	    close(fm);
	    return; 
	}

        flash_len = read(fm, buffer, length);
        if(flash_len != length){
	    printf("There are less than 0x%x bytes readable from flash. Actual read bytes is 0x%x\n", length, flash_len);
	}
#ifdef __DEBUG__
	j = 0;
	for(i = 0; i <  256; i++){	
	      printf("%2x ", buffer[i]);
	      if(j == 15){
		      printf("\n");
		      j=0;
	      }
	      else 
		      j++;
	}
        printf("\n\n");
#endif 
        close(fm);
}

int write_flash(char *dev_name, int flash_offset, unsigned char *buffer, int buffer_offset, int length)
{
	int fm, flash_len;
        mtd_info_t mtd_info;
        erase_info_t ei;
	unsigned char *tmp;

        fm = open(dev_name, O_RDWR);
	if(fm <= 0){
	   printf("open %s fail\n", dev_name);
	   return(-1); 
	}
	
	ioctl(fm, MEMGETINFO, &mtd_info);
	printf("MTD Type: %x\n MTD total size: %x bytes\n MTD erase size:%x bytes\n", mtd_info.type, mtd_info.size, mtd_info.erasesize);

        if(flash_offset % mtd_info.erasesize){
	    printf("The flash_offset(0x%x) is NOT multiply of erase_size(0x%x)\n", flash_offset, mtd_info.erasesize);
	}

	ei.length = mtd_info.erasesize;

	for(ei.start = flash_offset; ei.start < mtd_info.size; ei.start += ei.length)
	{
		ioctl(fm, MEMUNLOCK, &ei);
		ioctl(fm, MEMERASE, &ei);
		printf(".");
	}
        printf("\n\n");

	lseek(fm, flash_offset, SEEK_SET);

	if(buffer == NULL){
	    printf("No memory buffer\n");
	    close(fm);
	    return; 
	}

	tmp = (unsigned char *) (buffer + buffer_offset);

        flash_len = write(fm, tmp, length);
        if(flash_len != length){
	    printf("There are less than 0x%x bytes writeable to flash wirte length:0x%x\n", length, flash_len);
	}
        close(fm);
	return(flash_len);
}


unsigned char *read_file(char * file_name,  int in_offset, int *out_len)
{
	FILE *fd;
	int file_len, i, j;
	unsigned char hbuf[16];
	IMG_HEADER_T *header;
	unsigned char *out_buf = NULL;

	fd = fopen(file_name, "rb");

	if(fd ==NULL ){
		printf("The file %s is not available\n", file_name);
		return NULL;
	}
        
	fseek(fd, 0L, SEEK_SET);
	fread(hbuf, sizeof(IMG_HEADER_T), 1, fd);
	header = (IMG_HEADER_T *)hbuf;
        
	header->startAddr = endian_adjust(header->startAddr);
	header->burnAddr = endian_adjust(header->burnAddr);
	header->len = endian_adjust(header->len);
        
	out_buf = (unsigned char *)malloc(header->len + 16);

	if(out_buf == NULL)
	{
		printf("Out of system memory\n");
		return NULL;
	}

#ifdef __DEBUG__
	printf("The file %s signature =%4s, start_addr =0x%x, burn_address=0x%x length is 0x%x\n", file_name, header->signature, header->startAddr, header->burnAddr, header->len);
#endif

	fseek(fd, in_offset, SEEK_SET);
	fread(out_buf, header->len + 16, 1, fd);
	fclose(fd);

	*out_len = header->len;
        
	return(out_buf);
}

int upgrade_rootfs_from_mem(unsigned char * buffer, int len)
{
    if(buffer == NULL ){
        printf(" rootfs buffer is NULL\n");
        return(-1);
    }

    write_flash(rootfs_dev_write, 0, buffer, ROOTFS_BUFFER_OFFSET, len);
}

int  upgrade_rootfs_from_file(void)
{
    unsigned char *buffer;
    int len;

    buffer = read_file(ROOT_BIN, 0, &len);

    if(buffer == NULL ){
        printf("Outof memory\n");
        return(-1);
    }

    upgrade_rootfs_from_mem(buffer, len);

    free(buffer);
}

#if 0 // This is 16MB flash image
int main()
{
    unsigned char *buffer = NULL;
    FILE *fd;
    int i, len;

    fd = fopen(FLASH_BLOCK_FILE, "wb");
    if(fd ==NULL ){
        printf("The file %s is not available\n", FLASH_BLOCK_FILE);
	return(0);
    }

    len = 2 * MB ;
    buffer = (unsigned char *) malloc(len);
    read_flash(linux_dev, 0, buffer, len);
    fwrite(buffer, len, 1, fd);

    len = 10 * MB;
    buffer = (unsigned char *) realloc(buffer, len);
    read_flash(rootfs_dev, 0, buffer, len);
    fwrite(buffer, len, 1, fd);

    len = 1 * MB;
    buffer = (unsigned char *) realloc(buffer, len);
    read_flash(jffs_dev, 0, buffer, len);
    fwrite(buffer, len, 1, fd);

    len = 2 * MB ;
    buffer = (unsigned char *) malloc(len);
    read_flash(linux_backup, 0, buffer, len);
    fwrite(buffer, len, 1, fd);

    len = 1 * MB;
    buffer = (unsigned char *) realloc(buffer, len);
    read_flash(rootfs_backup, 0, buffer, len);
    fwrite(buffer, len, 1, fd);

    free(buffer);
    fclose(fd);

    return 1;
}
#endif

int main() //This will get 32MB bits 
{
    unsigned char *buffer = NULL;
    FILE *fd;
    int i, len;

    fd = fopen(FLASH_BLOCK_FILE, "wb");
    if(fd ==NULL ){
        printf("The file %s is not available\n", FLASH_BLOCK_FILE);
	return(0);
    }

    printf("Read Linux section\n");
    len = 2.5 * MB ;
    buffer = (unsigned char *) malloc(len);
    read_flash(linux_dev, 0, buffer, len);
    fwrite(buffer, len, 1, fd);

    printf("Read rootfs section\n");
    len = 16 * MB;
    buffer = (unsigned char *) realloc(buffer, len);
    read_flash(rootfs_dev, 0, buffer, len);
    fwrite(buffer, len, 1, fd);

    printf("Read jffs2 section\n");
    len = 9 * MB;
    buffer = (unsigned char *) realloc(buffer, len);
    read_flash(jffs_dev, 0, buffer, len);
    fwrite(buffer, len, 1, fd);

    printf("Read backup linux section\n");
    len = 2.5 * MB ;
    buffer = (unsigned char *) malloc(len);
    read_flash(linux_backup, 0, buffer, len);
    fwrite(buffer, len, 1, fd);

    printf("Read backup rootfs section\n");
    len = 2 * MB;
    buffer = (unsigned char *) realloc(buffer, len);
    read_flash(rootfs_backup, 0, buffer, len);
    fwrite(buffer, len, 1, fd);

    free(buffer);
    fclose(fd);

    return 1;
}
