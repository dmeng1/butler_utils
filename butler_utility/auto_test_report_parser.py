# This is auto test result parser python script
# usage auto_test_report_parser input_log_file_name outputfile_name(.csv)
# This is 
# Tested with report.log provided by https://universal.atlassian.net/wiki/spaces/QS/pages/1715961979/Automated+Testing+and+Profiling+of+Command+Execution
#

COMMAND_KEY = "RESULT:"
DEVICE_CMD = "device cmd"
RUN_KEY ="RUN:"

ERROR = "error"
import linecache
import sys
import re
import json

title = [ "Command NAME,"
          "Execution,"
          "Success,"
          "Failed,"
          "Line of error"          
]

command = []
command_idx = []
num_cmd_idx = 0
totalLines = 0
count =[]
fail = []
succ =[]
error_line = []
count_index = 0

def parser_delay(infile, outfile):
    with open(outfile, "w")as f:
        for item in title:
            f.write(str(item))
        f.write("\n")
        
        getAllCmdLinesFromFile(infile)
        for i in range(0, num_cmd_idx):
            #print("line", command_idx[i])
            status, e_line = isCmdSuccess(command_idx[i] + 1, command_idx[i+1], infile)
            #print("status=", status)
            getCmdFromFile(command_idx[i], infile, status, e_line)
        #print(count)
        #print (command_idx)
        writeToResultFile(f)
    f.close()
    print("..........................Parsing is done.................")

def isCmdSuccess(start_idx, end_idx, infile):
    obj = getCmdJsonStr(start_idx, end_idx, infile)
    if 'error' in obj:
        if(obj['error']['code'] == 0):
            return 0, 0
        else:
            return 1, start_idx
    else:
        if 'v' in obj:
            v1 = obj['v']
            #print("v1=", v1)
            if 'response' in v1:
                v2 = v1['response']
                #print("v2=", v2)
                if 'v' in v2:
                    v3 = v2['v']
                    for item in v3:
                        if 'error' in item:
                            return 1, start_idx
        return 0, 0            


def writeToResultFile(fp):
    global count_index
    
    for i in range (count_index):
        a_list = []
        a_list.append(command[i])
        a_list.append(count[i])
        a_list.append(succ[i])
        a_list.append(fail[i])
        a_list.append(error_line[i])
        for item in a_list:
            fp.write(str(item)+",")
        fp.write("\n")
    
def parser_main():
    in_file = ""
    arg_num = len(sys.argv)
         
    if (arg_num == 3):
        in_file = sys.argv[1]
        out_file = sys.argv[2]
        print("input file:" + in_file)
        print("output file:" + out_file)
        print(".....................Start parsing......................")
    else:
        print(".............Usage: streaming_parser.py input_file_name output_file_name.csv................")
        exit(0)

    parser_delay(in_file, out_file)
    exit(0)
    
def getCmdJsonStr(startIdx, endIdx, infile):
    tmp_str='{'
    str_1 =''
    for i in range(startIdx, endIdx):
        str_1 = linecache.getline(infile, i)
        if RUN_KEY in str_1:
            break
        else:
            tmp_str = tmp_str + str_1
    json_obj = json.loads(tmp_str)
    #print(json_obj)
    return json_obj

def isCmdInList(cmd, status, e_line):
    global count_index
    try:
        x = int(command.index(cmd))
        #print("Index", x)
        count[x] = int(count[x]) + 1
        if status == 0:
            succ[x] = int(succ[x]) + 1
        else:
            fail[x] = int(fail[x]) + 1
            
    except(ValueError):
        #print("Command",cmd, "is not in command list, add to the comand list")
        command.append(cmd)
        count_index = count_index +1
        count.append(1)
        if status == 0:
            succ.append(1)
            fail.append(0)
            error_line.append(0)
        else:
            fail.append(1)
            error_line.append(e_line)
            succ.append(0)
        

def getAllCmdLinesFromFile(file):
    global num_cmd_idx
    
    command_str = ""
    i=0;   
    for line in open(file, "r"):
        i = i+1
        if(re.search(DEVICE_CMD,line) or re.search(COMMAND_KEY,line)):
            #print(line)
            command_idx.append(i);
            num_cmd_idx = num_cmd_idx + 1
    command_idx.append(i+1)
    
def getCmdFromFile(index, infile, status, e_line):
    
    line = linecache.getline(infile, index)
    command_str = ""
    if DEVICE_CMD in line:
        #print(line)
        tmpStr =line.split()
        #print("Length of line", len(tmpStr))
        if(len(tmpStr) > 6):
            command_str = tmpStr[4][23:]
            x = command_str.find(",")
            command_str = "sub_cmd_" + command_str[:x-1]
            #print(command_str)
    else:
        if COMMAND_KEY in line:
            #print(line)
            tmpStr =line.split()
            #print("Length of line", len(tmpStr))
            if(len(tmpStr) > 2):    
                command_str = tmpStr[0]
                #print(command_str)
    isCmdInList(command_str, status, e_line)
if __name__ == '__main__':
    parser_main()


