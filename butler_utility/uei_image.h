#ifndef __UEI_IMAGE_H__
#define __UEI_IMAGE_H__
#include "apmib.h"

#define FW_SIGNATURE			((char *)"cs6c")	// fw signature
#define FW_SIGNATURE_WITH_ROOT		((char *)"cr6c")	// fw signature with root fs
#define ROOT_SIGNATURE          	((char *)"r6cr")
#define BOOT_SIGNATURE          	((char *)"boot")

#define SIG_LEN		4
#define HEADER_LEN	16 

/* Firmware image header */
#if 0
typedef struct _header_ {
	unsigned char signature[SIG_LEN];
	unsigned long startAddr;
	unsigned long burnAddr;
	unsigned long len;
} IMG_HEADER_T, *IMG_HEADER_Tp;
#endif

enum file_type {
    NONE,	
    ROOT_TYPE,
    LINUX_TYPE,
    FW_TYPE,
    BOOT_TYPE
};

#define linux_dev 		"/dev/mtdblock0"
#define boot_dev 		"/dev/mtdblock0"
#define rootfs_dev		"/dev/mtdblock1"
#define jffs_dev		"/dev/mtdblock2"
#define linux_backup 		"/dev/mtdblock3"
#define rootfs_backup		"/dev/mtdblock4"
#define linux_dev_write 	"/dev/mtd0"
#define rootfs_dev_write	"/dev/mtd1"
#define jffs_dev_write		"/dev/mtd2"
#define linux_backup_write	"/dev/mtd3"
#define rootfs_backup_write	"/dev/mtd4"
#define BOOT_LOADER_OFFSET	0x0
#define LINUX_FLASH_OFFSET	0x80000
#define LINUX_BACKUP_OFFSET	0x0
#define MP_LINUX_BACKUP_OFFSET	0x01B80000
#define ROOTFS_OFFSET		0
#define ROOTFS_BUFFER_OFFSET	16
#define LINUX_BIN		"/tmp/linux.bin"
#define ROOT_BIN		"root.bin"
#define FW_BIN			"/tmp/fw.bin"  //This will be general file name downloaded from cloud server 
#define BOOT_BIN		"boot.bin"//boot.bin is the boot loader image need to write to mtdblok0 
#define SECURE_BOOT_BIN		"secure_boot.bin"//boot.bin is the secure boot loader image need to write to mtdblok0 
#define FW_SECURE_SMALL		"fw_secure_small.bin"//fw_secure_small.bin is the secure small image need to write to /dev/mtd3 and /dev/mtd4 
extern int flash_image(void);
extern flash_imagei_buffer(char * buf, int buf_sz);
extern int checkFwMd5Buffer(unsigned char *orig_md5,char *buf, int  buffer_sz);
#endif
