#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <mtd/mtd-user.h>
#include <linux/fs.h>
#include <errno.h>
#include "apmib.h"

#define linux_dev               "/dev/mtdblock0"
#define linux_dev_write         "/dev/mtd0"
#define KERNEL_OFFSET		0x80000
#define FLASH_WRITE_LENGTH	16

int crash_kernel(void)
{
    int fm, flash_len;
    mtd_info_t mtd_info;
    erase_info_t ei;
    unsigned char tmp[16];
    int flash_offset = KERNEL_OFFSET;

    fm = open(linux_dev, O_RDWR);
    if(fm <= 0){
        printf("open %s fail\n", linux_dev);
        return(-1);
    }

    lseek(fm, flash_offset, SEEK_SET);
    memset(tmp, 0, FLASH_WRITE_LENGTH);
    flash_len = write(fm, tmp, FLASH_WRITE_LENGTH);
    if(flash_len != FLASH_WRITE_LENGTH){
       printf("There are less than 0x%x bytes writeable to flash wirte length:0x%x\n", FLASH_WRITE_LENGTH, flash_len);
    }
    sync();
    if(ioctl(fm, BLKFLSBUF, NULL) < 0){
        printf("flush mtd system cache error\n");
    }
    close(fm);
   return(flash_len);
}


int uei_set_url_reboot(char * url)
{
    char read_url[256];
    int n;

    apmib_init();

    apmib_get(MIB_UEI_FW_URL, (void *)read_url);

    printf("Current URL in flash=%s\n", read_url);

    if(url != NULL ) {
        apmib_set(MIB_UEI_FW_URL, (void *)url);
	apmib_update(CURRENT_SETTING);
        apmib_get(MIB_UEI_FW_URL, (void *)read_url);
        printf("Write to flash URL:%s, read bakc URL:%s\n", url, read_url);
	n = strcmp(url, read_url);
	if(n != 0){
	    printf("Update flash url not successful, will not download image and update flash n = %d\n" , n);
	    return(-1);
        }   
        printf("Crash kernel and reboot device\n", url, read_url);
        //system("/home/loadFlashUrl.sh");
        crash_kernel();
        system("reboot");
    }
    return(0);
}

int main(int argc, char** argv) {
    char url[256];

    if(argc<2){
     uei_set_url_reboot(NULL);
     printf("There is no input URL, using original URL\n");
     return(EXIT_SUCCESS);
     } 

    strcpy(url, (char *) argv[1]);
    printf("Input url=%s\n", url); 
    uei_set_url_reboot(url); 
    return (EXIT_SUCCESS);
}	

