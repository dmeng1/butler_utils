#include <stdio.h>   
#include <stdlib.h>   
#include <string.h> 
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h>
#include <string.h>
#include <zlib.h>

#include "asym_key.h"
#include "uei_online_enc.h"
#include "base64.h"
#include "apmib.h"

#define DO_PRINTF(...) \
if (!sg_print_buffer_only) \
{                          \
    printf(__VA_ARGS__);   \
}

static int sg_print_buffer_only = 0;

char key_table[256]= {244,109,99,245,106,179,252,163,250,70,255,177,24,149,91,77,187,66,44,24,113,127,230,220,159,191,196,179,230,31,101,81,101,179,106,121,198,5,158,86,100,233,18,167,98,115,28,238,70,177,181,133,187,8,53,174,71,68,189,8,236,253,251,102,66,246,111,48,223,219,57,23,147,120,201,197,127,54,199,13,109,114,178,94,91,100,206,21,191,113,30,30,7,122,11,63,242,158,247,183,33,159,15,157,199,180,27,136,255,147,123,145,122,13,42,181,179,17,77,182,183,117,170,124,242,132,71,17,122,125,210,123,47,166,44,83,22,242,60,150,122,186,178,166,178,86,194,146,140,141,14,52,202,90,176,253,164,187,204,9,62,92,85,180,2,54,138,172,132,228,62,97,255,182,115,241,16,149,103,214,138,169,87,123,226,93,222,105,5,189,132,29,2,26,244,70,252,14,176,174,75,8,190,242,35,59,116,105,126,152,252,162,93,130,1,125,225,70,249,167,164,148,225,11,110,194,78,116,121,164,99,213,191,173,100,214,244,195,37,34,13,59,123,141,147,60,91,112,59,225,56,201,79,77,145,246};//randomly generated key table 

#define GET_MAC_ADDR_FROM_FLASH

void show_str(const char * input_str, char *title)
{ 
    int i, len;
    len=strlen(input_str);
    DO_PRINTF("\t %s len=%d\n", title, len);
    for(i =0; i< len; i++){
        printf("%c",(char)(0x00ff & input_str[i]));
    }
    DO_PRINTF("\n");
}

void show_bit(const char *input_array, int len, char *title)
{ 
    int i;
    DO_PRINTF("\t %s  length =%d\n", title, len);

    for(i = 1; i<= len; i++){
        DO_PRINTF("%02x",(int)(0x00ff & input_array[i-1]));
        if((i % 40) == 0 && (i != 0)) {
	    DO_PRINTF("\n");	
        }
    }
    DO_PRINTF("\n");
}

int get_sub_str(char *long_ptr, char *private, char*cer)
{
    int len;

    if(long_ptr == NULL || private == NULL || cer == NULL) {
        DO_PRINTF(" There are null pointers, could not get data\n");
    return(-1);
    }

    strcpy(private,long_ptr);
    //show_str(private, "privare");
    len = strlen(private);
    strcpy(cer, (long_ptr + len +1));
    //show_str(cer, "Certifiacte");
    return(0);
}

int get_key_from_mac(char *out_buf)
{
    int i;
#ifdef GET_MAC_ADDR_FROM_FLASH
    unsigned char mac[ENC_BUF_LEN + 1] = { 0 };
    apmib_init();
    apmib_get(MIB_HW_NIC0_ADDR, (void*)mac);
#else
    int fd;
    struct ifreq ifr;
    char *iface = "br0";
    unsigned char *mac = NULL;

    memset(&ifr, 0, sizeof(ifr));
    fd = socket(AF_INET, SOCK_DGRAM, 0);

    ifr.ifr_addr.sa_family = AF_INET;
    strncpy(ifr.ifr_name , iface , IFNAMSIZ-1);

    if (0 == (i = ioctl(fd, SIOCGIFHWADDR, &ifr))) {
        mac = (unsigned char *)ifr.ifr_hwaddr.sa_data;
        /*show MAC*/
        //DO_PRINTF("Mac : %.2X:%.2X:%.2X:%.2X:%.2X:%.2X\n" , mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    } else {
	    DO_PRINTF("ioctl return value %d is not zero\n", i); 
	    close(fd);
	    return -1;
    }

    close(fd);
#endif

    for (i =0; i< 6;i++)
	    out_buf[i] = mac[i];

    for (i=0; i<6;i++)
	    out_buf[6+i]=key_table[(unsigned char) out_buf[i]];
    
    for (i=0; i<6;i++)
	    out_buf[12+i]=key_table[(unsigned char)out_buf[6+i]];

    for (i=0; i<6;i++)
	    out_buf[18+i]=key_table[(unsigned char)out_buf[12+i]];

    for (i=0; i<6;i++)
	    out_buf[24+i]=key_table[(unsigned char)out_buf[18+i]];

    for (i=0; i<2;i++)
	    out_buf[30+i]=key_table[(unsigned char)out_buf[24+i]];
#if 0
    DO_PRINTF ("\t Composed key array in key array \n");
    for(i=0; i<32; i++){
    	    DO_PRINTF("%02x",  0x00ff & out_buf[i]);
    }
    
    DO_PRINTF("\n");
#endif
    return 0;
}

/**********************************************************************************************************************************************************/
/* decrypt_key()
 * Inputs:
 *      a binary string pointer to the encrypted key array
 * Oputput:
 *       A pointer to a idecrrypted array, this pointer need to be freed by the caller after use the encryptest array
 * The funnction will generate encryption vector using device mac address, decrypt the uei_key and store the reult of decryption
 * *********************************************************************************************************************************************************/

char *decrypt_key(char *enced_key, int key_len)
{
    char enc_vector[ENC_VECTOR_LEN];
    char *decrypt_data_ptr = NULL;
    int len;

    //show_bit(enced_key, key_len, "ecrypted key");

    memset(enc_vector, 0, sizeof(enc_vector));

    get_key_from_mac(enc_vector);

    //show_bit(enc_vector, 32, "decrypt ENC vector");

    decrypt_data_ptr = (char *) malloc(ENC_BUF_LEN + 1);

    if(decrypt_data_ptr == NULL){
    DO_PRINTF("No enough memeory for decrypted data \n");
    return(NULL);
    }

    memset(decrypt_data_ptr, 0, ENC_BUF_LEN);

    len = ueic_encrypt_with_vector_large(0, enced_key, key_len, enc_vector, decrypt_data_ptr, ENC_BUF_LEN);
    //show_bit(decrypt_data_ptr, len, "Decrypt data");
    
    //DO_PRINTF("decrypt date length %d\n", len);
    return(decrypt_data_ptr);

}

/* Start read_secret specific code. */
#ifdef READ_INTER_KEY
/************************************************************************************
 * uncompreess a char buffer,
 * return the uncompressed buffer pointer 
 * Caller needs to free the retuned buffer pointer after using the buffer
 */

char *uncompress_buffer(char *in_str, int in_len, int out_len)
{
    z_stream instream;
    instream.zalloc = Z_NULL;
    instream.zfree = Z_NULL;
    instream.opaque = Z_NULL;
    char * uncompressed_str = NULL;

    uncompressed_str = (char*) malloc(out_len+1);

    if(uncompressed_str == NULL){
       DO_PRINTF(" There is no enough memoryi for compressed string\n");
       return(NULL);
    }

    instream.avail_in = (uInt)in_len;
    instream.next_in = (Bytef *)in_str; 
    instream.avail_out = (uInt)(out_len);
    instream.next_out = (Bytef *)uncompressed_str;

    inflateInit(&instream);
    inflate(&instream, Z_NO_FLUSH);
    inflateEnd(&instream);

    DO_PRINTF("Total inflate length =%d in_len=%d out_len = %d\n", instream.total_out, in_len, out_len);

    //show_bit(uncompressed_str, instream.total_out,  "Uncompressed data");

    return(uncompressed_str);
}
 
int retrieve_enc(char *out_buf, int out_len ){

    DO_PRINTF("Read from flash\n");
    if(apmib_init()) 
      apmib_get(MIB_UEI_INTER_KEY, (void *)out_buf);
    else
      DO_PRINTF("apmit_init error\n");

      
    return(0);
}

char *decrypt_inter_key(void) {
    char * tmp_ptr=NULL, *decrypt_ptr = NULL, *uncompressed_ptr = NULL;

    tmp_ptr= (char *) malloc(ENC_BUF_LEN + 1);

    if(tmp_ptr == NULL){
    DO_PRINTF("No enough memory to allocate drypt buffer buffer\n");
    return(NULL);
    }
    memset(tmp_ptr, 0, ENC_BUF_LEN); //Clean base64 buffer
    retrieve_enc(tmp_ptr,  ENC_BUF_LEN);

    //show_bit(tmp_ptr, ENC_BUF_LEN, "Read back Enc binary");

    decrypt_ptr = decrypt_key(tmp_ptr, ENC_BUF_LEN);

    if(decrypt_ptr == NULL){
     DO_PRINTF("No enough memory to allocate drypt buffer buffer\n");
     return(NULL);
    }

    uncompressed_ptr = uncompress_buffer(decrypt_ptr, ENC_BUF_LEN, MAX_IN_BUF_SZ);
    
    if(uncompressed_ptr == NULL){
    DO_PRINTF("uncompree failed\n");
    return(NULL);
    }
        
    free(decrypt_ptr); 

    free(tmp_ptr);
    
    //show_str(uncompressed_ptr, "Uncompressed String");

    return(uncompressed_ptr);
}

/***************************************************************************
 * get_inter_key() is an API function 
 * inputs
 * private is the pointer to private key
 * cer is the point to the certificate 
 * Caller need to private memory for these two pointers
 * The API function will read from flash and filled out these two data.
 * At function return, the caller can use these data as needed.
 * **************************************************************************/ 
void get_inter_key(char *private, char *cer)
{
    char *long_ptr = NULL;
    char mac_buf[32];


    get_key_from_mac(mac_buf);

    memset(private, 0, IOT_PRIVATE_LEN);
    memset(cer, 0, IOT_PUBLIC_LEN);

    long_ptr = decrypt_inter_key();

    get_sub_str(long_ptr, private, cer);

    
    free(long_ptr);

    return;    
}

/* Start store_inter_key specific code. */
#else
int read_file( char *file_name, char *out_buf, int buf_sz)
{
    FILE *fp;
    int file_len = 0, i = 0;

    if(out_buf == NULL) {
        DO_PRINTF(" The output buffer is NULL, culd not read any string\n");
    return(-1);
    }

    fp = fopen(file_name,"r");

    if(fp ==NULL) {
    DO_PRINTF("The file %s could not be openned \n", file_name);
        return(-1);
    }

    fseek(fp, 0L, SEEK_END);
    file_len = ftell(fp);

    if(file_len >= buf_sz){
        DO_PRINTF("The buffer size of %d is less than the file sizei of %d , could not read whole file to the buffer\n", buf_sz, file_len);
       return(-1);  
    }else{
        DO_PRINTF(" The file length is %d\n", file_len);
    }

    fseek(fp, 0L, SEEK_SET);

    while(!feof(fp) && i < buf_sz) {
        out_buf[i++]= (char)(0xff & fgetc(fp));
    }

    fclose(fp);

    return (file_len);
}

/************************************************************************************
 * compreess a char buffer,
 * return the buffer pointer 
 * Caller needs to free the retuned buffer pointer after using the buffer
 */

char *compress_buffer(char *in_str, int in_len, int *out_len)
{
    z_stream instream;
    instream.zalloc = Z_NULL;
    instream.zfree = Z_NULL;
    instream.opaque = Z_NULL;
    char * compressed_str = NULL;

    compressed_str = (char*) malloc(in_len+1);

    if(compressed_str == NULL){
       DO_PRINTF(" There is no enough memoryi for compressed string\n");
       return(NULL);
    }

    instream.avail_in = (uInt)in_len;
    instream.next_in = (Bytef *)in_str; 
    instream.avail_out = (uInt)(in_len);
    instream.next_out = (Bytef *)compressed_str;

    deflateInit(&instream, Z_BEST_COMPRESSION);
    deflate(&instream, Z_FINISH);
    deflateEnd(&instream);
    
    *out_len = instream.total_out;

    return(compressed_str);
}

/**************************************************************************************************************
 * compose_secret() is a function read iot connection string, private key and public certificate from 
 * corresponding files. Compose they as a sigle string.  Thi single string will be compressed.
 * the pointer of compressed bits of array will be output to the encryption engy and encrypted.
 * the retuen pointer should be freed by the caller
 * ***********************************************************************************************************/
char *compose_compressed_secret(int *compressed_len)
{
    char private[IOT_PRIVATE_LEN];
    char public_cer[IOT_PUBLIC_LEN];
    char pad_buf[PAD_LENGTH];
    char *final_str = NULL, *compressed_str=NULL;
    int private_len = 0, public_cer_len = 0;

    memset(private, 0, IOT_PRIVATE_LEN);
    memset(public_cer, 0, IOT_PUBLIC_LEN);
    memset(pad_buf, 0, PAD_LENGTH);

    private_len = read_file(IOT_PRIVATE_KEY, private, IOT_PRIVATE_LEN);
    if(private_len ==0){
        DO_PRINTF("The priveate key string is zero length\n");
    } else {
	   //show_str(private, "Private String"); 
    }

    public_cer_len = read_file(IOT_PUBLIC_CER, public_cer, IOT_PUBLIC_LEN);

    if(public_cer_len == 0){
        DO_PRINTF("The public strkey string is zero length\n");
    } else {
	   //show_str(public_cer, "Public certificate string"); 
    }
    
    final_str= (char *)malloc(private_len + public_cer_len + PAD_LENGTH + 2);

    if(final_str == NULL){
       DO_PRINTF(" There is no enough memory\n");
       return(NULL);
    }

    strcpy(final_str, private);
    final_str[private_len]='\0';
    strcpy((final_str + private_len + 1), public_cer);
    final_str[private_len + public_cer_len + 1]='\0';
    strcpy((final_str + private_len + public_cer_len + 2), pad_buf);

    //show_str(final_str, "Final STR_1");
    //show_str((final_str + private_len + 1), "Final STR_2");
    
    compressed_str = compress_buffer(final_str, (private_len + public_cer_len + PAD_LENGTH + 2), compressed_len);
    //DO_PRINTF("private_len=%d, public_len=%d, PAD_LENGTH=%d comprssed_len=%d\n", private_len, public_cer_len, PAD_LENGTH, *compressed_len);

    free(final_str);
    return(compressed_str);
}

int read_iot_key_from_file( char *iot_key_buf, int size)
{
    FILE *fp;

    fp = fopen(AZURE_IOT_KEY_NAME,"rb");
     if(fp ==NULL) {
	DO_PRINTF("The file could not be openned \n");
        return(1);
     }
    fread(iot_key_buf, size, 1, fp);
    
    fclose(fp);

    return (0);
}

/**********************************************************************************************************************************************************/
/* encrypt_key()
 * Inputs:
 * input:   
 *       a string pointer to a key   
 * Oputput:
 *       A pointer to a encrypted array, this pointer need to be freed by the caller after use the encryptest array
 * The funnction will generate encryption vector using device mac address, encrypt the uei_key and store the reult of 2048 bytes into flash paramter section
 * *********************************************************************************************************************************************************/

char *encrypt_key(char *key, int key_len)
{
    char enc_vector[ENC_VECTOR_LEN];
    char *enced_data_ptr = NULL;
    unsigned int len;

    if(key == NULL){
       DO_PRINTF("The input key is NULL\n");
       return(NULL);
    }

    memset(enc_vector, 0, sizeof(enc_vector));

    get_key_from_mac(enc_vector);

    enced_data_ptr = (char *) malloc(ENC_BUF_LEN + 1);

    if(enced_data_ptr == NULL){
	DO_PRINTF("No enough memeory for encrypted data \n");
	return(NULL);
    }

    memset(enced_data_ptr, 0, ENC_BUF_LEN);

    len = ueic_encrypt_with_vector_large(1, key, key_len, enc_vector, enced_data_ptr, ENC_BUF_LEN);

    //show_bit(enced_data_ptr, ENC_BUF_LEN, "Encrypted data");

    if(len != ENC_BUF_LEN){
	    DO_PRINTF("encrypte len=%d, expected len = %d\n", len, ENC_BUF_LEN);
    }

    return(enced_data_ptr);
}

int flash_enc(char *in_ptr, int in_len){
    if(in_ptr == NULL) 
    {
        DO_PRINTF("THe input pointer is NULL\n");
	return(-1);
    }

    if(apmib_init()) {
        apmib_set(MIB_UEI_INTER_KEY, (void *)in_ptr);
        apmib_update(CURRENT_SETTING);
     } else {
	     DO_PRINTF("apmib_init error\n");
     }
    return(0);
}

char *encrypt_inter_key(void)
{
    char *compressed_secret = NULL, *enced_ptr = NULL;
    int len = 0;
    char *decrypt_ptr = NULL;

    compressed_secret = compose_compressed_secret(&len);

    if(compressed_secret == NULL || len == 0){
        DO_PRINTF("secret compress failed\n");
	return(NULL);
    }

    //show_bit(compressed_secret, len, "Compressed scret data");
    enced_ptr = encrypt_key(compressed_secret, len);

    if(enced_ptr == NULL ){
        DO_PRINTF("Encryption failed\n");
	free(compressed_secret);
	return(NULL);
    }

    free(compressed_secret);
    
    /*following are test code to test decrypt function*/

    //show_bit(enced_ptr, len, "TEST: Encrypted data");
    decrypt_ptr = decrypt_key(enced_ptr, ENC_BUF_LEN);
    //show_bit(decrypt_ptr, ENC_BUF_LEN, "TEST: Decrypt data");
    free(decrypt_ptr);
    return(enced_ptr);
}

/***********************************************************
*Store secret is a API function.  The function will read    
* "int.nb.cer"
* "int.nb.key"
* files from current directory and encrypted them and store in the flash. 
* The caller need to prepare these three files before call this API function.
* **********************************************************/
void store_inter_key(void)
{ 
    char *enc_ptr=NULL;
    
    enc_ptr = encrypt_inter_key();

    if(enc_ptr == NULL){
        DO_PRINTF("Could not get encypted secret\n");
        return;
    }

    //show_bit(enc_ptr,ENC_BUF_LEN,"Ecrypted data");

    flash_enc(enc_ptr, ENC_BUF_LEN);

    
    free(enc_ptr); 
}
#endif

int main(int argc, char *argv[])
{
#ifdef READ_INTER_KEY
    char private[IOT_PRIVATE_LEN];
    char cer[IOT_PUBLIC_LEN];
    int print_cert = 1;
    int print_key = 1;

    if (argc > 1)
    {
        if (strcmp(argv[1], "cert") == 0)
        {
            sg_print_buffer_only = 1;

            print_cert = 1;
            print_key  = 0;
        }
        else if (strcmp(argv[1], "key")  == 0)
        {
            sg_print_buffer_only = 1;

            print_cert = 0;
            print_key  = 1;
        }
    }
   
    memset(private, 0, IOT_PRIVATE_LEN);
    memset(cer, 0, IOT_PUBLIC_LEN);
    get_inter_key(private, cer);

    if (print_key)
    {
        show_str(private, "private key");
    }
    if (print_cert)
    {
        show_str(cer, "Certificate");
    }
#else
    store_inter_key();
#endif
    
    return(0);   
}
