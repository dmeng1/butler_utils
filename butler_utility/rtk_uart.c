/**
 * @file        rtk_uart.c
 * @brief       hardware abstraction layer for realtek UART1 device
 *
 */

/*
 * COPYRIGHT 2015 UNIVERSAL ELECTRONICS INC (UEI).
 * These materials are provided under license by UEI.
 * UEI licenses this file to you under the SOFTWARE EVALUATION LICENSE AGREEMENT
 * accompanying the SDK delivered to you (the "License").
 * You may also obtain a copy of the License directly from UEI.
 * You may not use this file except in strict compliance with the License
 * and all restrictions set forth therein. Your use and access to the file
 * constitutes your agreement to be bound by the terms of the License.
 * Unless required by applicable law or agreed to in writing, all materials
 * distributed under the License is provided on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, either express or implied.
 */

/* control plus includes */
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/errno.h>
#include <fcntl.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include <error.h>
#include <string.h>


/*
 ********************************************************************************
 * Constants.
 ********************************************************************************
*/

/*
 ********************************************************************************                                                                
 * Globals.
 ********************************************************************************
*/
static int uart_handle = -1;

/*
 ********************************************************************************
 * Data Types.
 ********************************************************************************
 */

/*
 ********************************************************************************
 * Prototypes.
 ********************************************************************************
 */

/**
 * @brief        write message to IR bus 
 *
 * @param        message to be written
 *
 * @param        length of the message
 *
 * @param        reponse from the IR bus
 *
 * @ingroup      Auto-LookupAPI
 *
 */
int uart_write( const unsigned char *message, const int length)
{
    int             bytes = 0;
    if(uart_handle != -1)
    {
        bytes = write(uart_handle, message, length);
    } else {
        printf("Serial handle is invalide\n");
    }
    
    return bytes;
}

/**
 * @brief   open a device handle instance to IR device
 *
 * @ingroup Auto-LookupAPI
*/
int uart_open()
{
    struct termios options;

    uart_handle = open( "/dev/ttyS1", O_RDWR | O_NOCTTY);
    if(uart_handle == -1) 
    {
      	printf("Failed to Open UART1 port\n");
        return -1;
    }
    memset( &options, 0, sizeof(options));
    options.c_cflag = B38400 | CS8 | CREAD | CLOCAL;
    options.c_iflag = IGNPAR;
    options.c_oflag = 0;
    options.c_lflag = 0;
    options.c_cc[VMIN]=1;
    options.c_cc[VTIME]=0;
    tcflush(uart_handle, TCIFLUSH);
    tcsetattr(uart_handle,TCSANOW,&options);
    return 0;
}
 
/**
 * @brief   Close the device handler to the IR device
 *
 * @ingroup Auto-LookupAPI
*/
void uart_close()
{
    /*When closing reset the fd(termios) to old fd settings*/
    close(uart_handle);
}

int uart_read(char* buffer, int length)
{
    int i = 0, n;
    char cn;

    do {
	n = read(uart_handle, &cn, 1);
	sprintf(&buffer[i],"%c", cn);
	uart_write(&cn, 1);
	i++;
    } while (i < length && cn != '\r');

    buffer[i] = '\0';
    return(i);
}

/**
 * @brief   ms sleep funciton.
 *
 * @param   Time delay in Milliseconds.  
 *
 * @ingroup Utils
*/
void
uei_ms_sleep( unsigned int mS )
{
    usleep(mS*1000);
}

int main(void)
{
    int result; 
    unsigned char buffer[128];

    uart_open();
    if(uart_handle != -1){
	    while(1) {
        	uart_write("\n\nHello World!\r\n", 16);
	    }
	memset(buffer, 0, 128); 
	result = uart_read(buffer, 124);
	strcat(buffer, "\r\n");
	uart_write(buffer, (result+4));
	uart_close();
	return(1);
    }
    return(0);
}

