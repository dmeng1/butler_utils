#!/bin/bash

SUPPORTED_TARGETS="realtek"
SCRIPT_DIR=`dirname $BASH_SOURCE`

usage()
{
	printf "Usage: build.sh [--platform=TARGET] [--debug] [--help] [--clean-all] [Make Commands]\n"
	printf "where:\n"
	printf "\t--platform=TARGET\n"
	printf "\t\tThe target to set the environment up for. The default is realtek.\n"
	printf "\t\tSupported Targets:\n"
	for target in $SUPPORTED_TARGETS
	do
		printf "\t\t\t${target}\n"
	done
	printf "\t--debug\n"
	printf "\t\tBuild in debug mode.\n"
	printf "\t--clean-all\n"
	printf "\t\tCleans all of the supported targets.\n"
	printf "\t--help\n"
	printf "\t\tDisplay this message.\n"
	printf "\n\tMake Commands\n"
	printf "\t\tCommands that are to be relayed by the Makefile.\n"
}

TARGET=linux

# Check options.
for i in "$@"
do
	case $i in
		--platform=*)
			TARGET=${i#*=}

			# Check platforms
			for target in $SUPPORTED_TARGETS
			do
				if [ "$TARGET" == "$target" ]; then
					TARGET_SUPPORTED=true
					break
				fi
			done

			if [ "$TARGET_SUPPORTED" != true ]; then
				printf "Unsupported targets '${TARGET}'.\n"
				printf "Supported Targets:\n"
				for target in $SUPPORTED_TARGETS
				do
					printf "\t${target}\n"
				done
				exit 1
			fi
		;;
		--debug)
			export DEBUG=y
		;;
		--clean-all)
			for target in $SUPPORTED_TARGETS
			do
				export TARGET=$target
				make -C $SCRIPT_DIR clean
			done

			exit 0
		;;
		--help)
			usage
			exit 0
		;;
		*)
			MAKE_CMDS="${MAKE_CMDS} ${i}"
		;;
	esac
done

export TARGET

if [ "$TARGET" == "realtek" ]; then
	# Set this to the path to your toolchain.
	CROSS_COMPILE_PATH=/opt/toolchains/realtek/toolchain

	export PATH=${CROSS_COMPILE_PATH}/bin:$PATH
	export CROSS_COMPILE_PREFIX=mips-linux-uclibc-
	export CC=gcc
fi


make -C $SCRIPT_DIR/build $MAKE_CMDS

