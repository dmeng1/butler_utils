#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "apmib.h"

#define ROOTFS_BUFFER_OFFSET    16
#define BOOT "secure_boot.bin"
#define PARAMETER "parameter.bin"
#define FW_MAIN  "fw.bin"
#define JFFS2 "jffs2.bin"
#define FW_SMALL "fw_small.bin"
#define BLOCK_FILE "block_file.bin"
#define MB 0x100000

void usage(void)
{
	printf("This tool will use five input data files to make a 32 MB image data file---block_file.bin\n");
	printf("The output block_file.bin file can be used in factory procedure to flash an new NB device flash memory.\n");
        printf("The output block_file.bin can also be directly written to NB device flash using tool using flash_32mb_image.\n");
	printf("Input files 1. secure_boot.bin binary file is built directly from bootcode_rtl8197f.\n");
	printf("            2. fw.bin binary file is built from RealTek SDK using normal build procedure.\n");
        printf("            3. parameter.bin binary file is extracted from a bolck_file.bin using extract_parameter_jffs2 tool or from NB device using data_extract tool.\n");
        printf("            4. jffs2.bin binary file is extracted from a bolck_file.bin using extract_parameter_jffs2 tool or from NB device using data_extract tool.\n");
	printf("            5. fw_small.bin binary file is built from realTek SDK using normal small image built procedure.\n");
	printf("All input files must prepared and put into the same folder as this tool.\n");
	printf("This tool will only use the old (current) flash data structure. This tool can not be used for new flash memory data structure.\n");
}

int endian_adjust(int input)
{
	int out1, out2;

	out1 =(input >> 16);
	out1 = (out1 >> 8)|(out1 & 0xff) << 8;

	out2 = (input & 0xffff);
        out2 = (out2 >> 8) | (out2 & 0xff) << 8;
	return (out2 << 16| out1);
}

unsigned char *read_partial_file(FILE *fd,  int file_offset, int *out_len)
{
	IMG_HEADER_T *header;
	unsigned char *out_buf = NULL;
	unsigned char hbuf[20];
	int i, j;

	if(fd == NULL ){
		printf("The file ID(%ld) is invalide\n", (long int)fd);
		return NULL;
	}
        
	fseek(fd, file_offset, SEEK_SET);
	fread(hbuf, sizeof(IMG_HEADER_T), 1, fd);
	header = (IMG_HEADER_T *)hbuf;
        
	header->startAddr = endian_adjust(header->startAddr);
	header->burnAddr = endian_adjust(header->burnAddr);
	header->len = endian_adjust(header->len);
        
	out_buf = (unsigned char *)malloc(header->len + 16);

	if(out_buf == NULL)
	{
		printf("Out of system memory\n");
		return NULL;
	}

	fseek(fd, file_offset, SEEK_SET);
	fread(out_buf, header->len + 16, 1, fd);

	*out_len = header->len;
        
	return(out_buf);
}

int main(void)
{
    FILE *fblock, *fm;
    unsigned char  *buf = NULL;
    int i, len, file_offset = 0;

    usage();

    fblock = fopen(BLOCK_FILE, "wb");
    if(fblock == NULL ){
        printf("The file %s could not be created.\n", BLOCK_FILE);
        return(-1);
    }

    buf = (unsigned char *) malloc(MB);
    if(buf == NULL){
        printf("buffer allocation failed\n");
        fclose(fblock);
        return(-1);
    }	
    memset(buf, 0xff, MB);

    for(i = 0; i< 32; i++)
	    fwrite(buf, MB, 1, fblock);

    printf("Done initialize the file buffer\n");

    /* Write Bootloader part of the flash memory*/ 			     
    fm = fopen(BOOT, "rb");
    if(fm == NULL ){
        printf("The file %s could not be openned.\n", BOOT);
        return(-1);
    }

    fseek(fblock, file_offset, SEEK_SET);
    len = 0x50000;
    buf = (unsigned char *) realloc(buf, len);
    if(buf == NULL) {
        printf("buffer reallocation failed\n");
        fclose(fm);
        fclose(fblock);
        return(-1);
    }	
    fread(buf, len, 1, fm);
    fwrite((buf + 16), len, 1, fblock); // The first 16 bytes are not write to block file
    fclose(fm);

    printf("Done write secure_boot\n");

    file_offset += len;


    /*Write parameter part of the flash memory*/ 			     
    fm = fopen(PARAMETER, "rb");
    if(fm == NULL ){
        printf("The file %s could not be openned.\n", PARAMETER);
	fclose(fblock);
        return(-1);
    }
    fseek(fblock, file_offset, SEEK_SET);
    len = 0x30000;
    buf = (unsigned char *) realloc(buf, len);
    if(buf == NULL) {
        printf("buffer reallocation failed\n");
        fclose(fm);
        fclose(fblock);
        return(-1);
    }	
    fread(buf, len, 1, fm);
    fwrite(buf, len, 1, fblock); 
    fclose(fm);
    printf("Done write parameter\n");

    file_offset += len;

    /*Write fw.bin Linux part of the flash memory*/
    free(buf);
    buf = NULL;
    fm = fopen(FW_MAIN,"rb");
    if(fm == NULL) {
        printf("Could not open file %s\n", FW_MAIN);
	fclose(fblock);
        return(-1);
    }

    buf = (unsigned char *)read_partial_file(fm, 0, &len); //Read linux part
    if(buf == NULL){
        printf("Read linux part of the %s failed\n", FW_MAIN);
	fclose(fm);
	fclose(fblock);
	return(-1);
    }

    if((len +16) > 2*MB){
        printf("The length of the Linux kernel is too big 0x%x\n", len);
	free(buf);
	fclose(fm);
	fclose(fblock);
	return(-1);
    }
    fseek(fblock, file_offset, SEEK_SET);
    fwrite(buf, (len + 16), 1, fblock);
    printf("Done write fw.bin---linux\n");

    file_offset += (2 * MB);
    free(buf);


    /* Write rootfs part of the flash memory*/
    buf = read_partial_file(fm, (len+16), &len); //Read rootfs part

    if( buf == NULL ){
        printf("Linux part of file %s could not be read.\n", FW_MAIN);
	fclose(fm);
	fclose(fblock);
        return(-1);
    }

    if(len > 16*MB){
        printf("The length of the rootfs is too big 0x%x\n", len);
	free(buf);
	fclose(fm);
	fclose(fblock);
	return(-1);
    }

    fseek(fblock, file_offset, SEEK_SET);
    fwrite((buf + ROOTFS_BUFFER_OFFSET), len, 1, fblock); 
    printf("Done write fw.bin----rootfs\n");

    file_offset += (16 * MB); 
    fclose(fm);
    free(buf);

    /*Write jffs2  part to block_file.bin*/ 			     
    fm = fopen(JFFS2, "rb");
    if(fm == NULL ){
        printf("The file %s could not be craeted.\n", JFFS2);
	fclose(fblock);
        return(-1);
    }

    fseek(fblock, file_offset, SEEK_SET);
    len = 9 * MB;
    buf = (unsigned char *) malloc(len);
    if(buf == NULL) {
        printf("There is no memory for buffer \n");
        fclose(fm);
        fclose(fblock);
        return(-1);
    }	
    fread(buf, len, 1, fm);
    fwrite(buf, len, 1, fblock); 
    fclose(fm);
    printf("Done write jffs2\n");

    file_offset += len;

    /* Write Linux kernel of fw_small.bin to the  block_file.bin file*/
    free(buf);
    buf = NULL;
    fm = fopen(FW_SMALL,"rb");
    if(fm == NULL) {
        printf("Could not open file(%s)\n", FW_MAIN);
        return(-1);
    }

    buf = (unsigned char *)read_partial_file(fm, 0, &len); //Read linux part
    if(buf == NULL){
        printf("Read linux part of the %s failed\n", FW_MAIN);
	fclose(fm);
	fclose(fblock);
	return(-1);
    }

    if((len +16) > 2*MB){
        printf("The length of the Linux kernel is too big 0x%x\n", len);
	free(buf);
	fclose(fm);
	fclose(fblock);
	return(-1);
    }
    fseek(fblock, file_offset, SEEK_SET);
    fwrite(buf, (len + 16), 1, fblock);
    printf("Done write small.bin---linux\n");

    file_offset += (2 * MB);
    free(buf);


    /* Write rootfs part of the block_file.bin*/
    buf = read_partial_file(fm, (len+16), &len); //Read rootfs part
    if(buf == NULL){
        printf("Read root part of the fw.bin failed\n");
	fclose(fm);
	free(fblock);
	return(-1);
    }

    if(len > 2.5 * MB){
        printf("The length of the rootfs of small image is too big 0x%x\n", len);
	free(buf);
	fclose(fm);
	fclose(fblock);
	return(-1);
    }

    fseek(fblock, file_offset, SEEK_SET);
    fwrite((buf + ROOTFS_BUFFER_OFFSET), len, 1, fblock); 
    printf("Done write small.bin---rootfs\n");
    fclose(fm);
    fclose(fblock);
    free(buf);
    printf("32MB image created successfully!!!\n");
    return(0);
}

