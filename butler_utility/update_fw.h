#ifndef __UEI_IMAGE_H__
#define __UEI_IMAGE_H__

#define FW_SIGNATURE			((char *)"cs6c")	// fw signature
#define FW_SIGNATURE_WITH_ROOT		((char *)"cr6c")	// fw signature with root fs
#define ROOT_SIGNATURE          	((char *)"r6cr")

#define SIG_LEN		4
#define HEADER_LEN	16 
enum file_type {
    NONE,	
    ROOT_TYPE,
    LINUX_TYPE,
    FW_TYPE
};

#define linux_dev 		"/dev/mtdblock0"
#define rootfs_dev		"/dev/mtdblock1"
#define jffs_dev		"/dev/medblock2"
#define linux_backup 		"/dev/mtdblock3"
#define rootfs_backup		"/dev/mtdblock4"
#define linux_dev_write 	"/dev/mtd0"
#define rootfs_dev_write	"/dev/mtd1"
#define jffs_dev_write		"/dev/med2"
#define linux_backup_write	"/dev/mtd3"
#define rootfs_backup_write	"/dev/mtd4"
#define LINUX_FLASH_OFFSET	0x60000
#define LINUX_BACKUP_OFFSET	0x0
#define ROOTFS_OFFSET		0
#define ROOTFS_BUFFER_OFFSET	16
#define LINUX_BIN		"linux.bin"
#define ROOT_BIN		"root.bin"
#define FW_BIN			"fw.bin" //This will be general file name downloaded from cloude server 
#endif
