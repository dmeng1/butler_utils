#include <stdint.h>
#include "base64.h"

#define BASE64_IN_BLOCK_SIZE        3
#define BASE64_OUT_BLOCK_SIZE       4
#define BASE64_BYTE_BIT             6
#define BASE64_PADDING_CH           '='

static const char g_encode_index[] = {
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
    'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
    'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
    'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'
};

static const int8_t g_decode_index[] = {
      -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
      -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
      -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1, 0x3e,   -1,   -1,   -1, 0x3f,
    0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b, 0x3c, 0x3d,   -1,   -1,   -1,   -1,   -1,   -1,
      -1, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e,
    0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19,   -1,   -1,   -1,   -1,   -1,
      -1, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28,
    0x29, 0x2a, 0x2b, 0x2c, 0x2d, 0x2e, 0x2f, 0x30, 0x31, 0x32, 0x33,   -1,   -1,   -1,   -1,   -1
};

int base64_encode(char * out_str, size_t out_str_len, const void * in_buffer, size_t in_len) {
    static const struct {
        uint8_t     shift;
        uint8_t     mask;
        uint8_t     next_shift;
        uint8_t     next_mask;
    } byte_info[BASE64_IN_BLOCK_SIZE] = {
        {2, 0x3f, 4, 0x03},
        {4, 0x0f, 2, 0x0f},
        {6, 0x03, 0, 0x3f}
    };
    int retval = 0;
    const uint8_t * ptr = in_buffer;

    while (in_len > 0) {
        int i;
        uint8_t rem;

        for (i = 0, rem = 0; i < BASE64_IN_BLOCK_SIZE && in_len > 0; i++, in_len--, ptr++) {
            if (out_str_len > 1) {
                *(out_str++) = g_encode_index[rem | ((*ptr >> byte_info[i].shift) & byte_info[i].mask)];
                out_str_len--;
            }
            rem = (*ptr & byte_info[i].next_mask) << byte_info[i].next_shift;
            retval++;
        }
        if (out_str_len > 1) {
            *(out_str++) = g_encode_index[rem];
            out_str_len--;
        }
        retval++;
    }
    for (; (retval & 0x3) != 0; retval++) {
        if (out_str_len > 1) {
            *(out_str++) = BASE64_PADDING_CH;
            out_str_len--;
        }
    }
    if (out_str_len > 0)
        *out_str = '\0';

    return retval;
}

int base64_decode(void * out_buffer, size_t out_buffer_len, const char * in_str) {
    static const struct {
        uint8_t     shift;
        uint8_t     next_shift;
        uint8_t     next_mask;
    } byte_info[BASE64_OUT_BLOCK_SIZE] = {
        {BASE64_BYTE_BIT, 2, 0x3f},
        {4,               4, 0x0f},
        {2,               6, 0x03},
        {0,               0, 0x00}
    };
    int retval = 0;
    int i;
    int bi;
    int8_t in_val;
    uint8_t rem;
    uint8_t * ptr;

    for (i = 0, bi = BASE64_OUT_BLOCK_SIZE, rem = 0, ptr = out_buffer; (*in_str & 0x80) == 0 && (in_val = g_decode_index[(int) *in_str]) >= 0; i++, in_str++) {
        bi = i & 0x3;
        if (byte_info[bi].shift < BASE64_BYTE_BIT) {
            retval++;
            if (out_buffer_len > 0) {
                *(ptr++) = rem | (in_val >> byte_info[bi].shift);
                out_buffer_len--;
            }
        }
        rem = (in_val & byte_info[bi].next_mask) << byte_info[bi].next_shift;
    }
    if (bi >= 1) {
        const char * cptr;

        for (cptr = in_str; *cptr != '\0' && *cptr != BASE64_PADDING_CH && bi < BASE64_OUT_BLOCK_SIZE; cptr++, bi++, i++) {}
        if (*cptr != '\0' && *cptr != BASE64_PADDING_CH)
            retval = -i;
    }
    else
        retval = -i;

    return retval;
}
