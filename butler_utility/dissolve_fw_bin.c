/* This utility tool is to extract the boot loadersection, parameter section, fw.bin, jffs2 and small image  sections from current 32MB image data file.  The icurrent 32MB data file is using old partition*/
/* The resul:  fw_boot.bin is the bootloader section data
 *             fw_parameter.bin is the parameter sction data file
 *             fw_fw.bin is the firware section of the image file
 *             fw_jffs2.bin is the jffs2 sction data file 
 *             fw_small.bin is the small image sction of the data file*/

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <mtd/mtd-user.h>
#include <unistd.h>

#define KB 0x1000
#define MB 0x100000
#define FW_BOOT		"fw_boot.bin"
#define PARAMETER_FILE	"fw_parameter.bin"
#define FW_FW		"fw_fw.bin"
#define JFFS2_FILE 	"fw_jffs2.bin"
#define FW_SMALL 	"fw_small.bin"
#define FILE_32MB 	"block_file.bin"

int read_file_2_buffer(unsigned char *buffer)
{
	FILE *fp;
	int len;
	char FILE_NAME[1024];
	
	printf("Please input block file\n");
	scanf("%s", FILE_NAME);
	printf("Input file name %s\n", FILE_NAME);
	fp=fopen(FILE_NAME, "rb");
	if(fp==NULL){
		printf("Input file does not exist and use default block file block_file.bin\n");
		fp = fopen(FILE_32MB,"rb");
		if(fp == NULL){
			printf("There is no default files in the folder, exit\n");
			return(1);
		}
	}
        len = fread(buffer, 1, 32*MB, fp);
	fclose(fp);
	if (len < 32*MB){
		printf("The data file len (0x%x) is less than 32 MB and exit\n", len);
	        return(1);
	}
	return(0);
}

void main() //This will generate two files fw_parameter.bin and fw_jffs2.bin 
{
    unsigned char *buffer = NULL, *file_buffer=NULL;
    FILE *fd;
    int len;

    file_buffer = (unsigned char *) malloc(32*MB + 1);
    if(file_buffer == NULL){
	   printf("No enough memory and exit\n");
	   exit(1);
    }

    if(read_file_2_buffer(file_buffer) !=0){
	    printf("read file failed, free data buffer\n");
	    free(file_buffer);
	    exit(1);
    }

    fd = fopen(FW_BOOT, "wb");
    if(fd ==NULL ){
        printf("The file %s is not openned\n", FW_BOOT);
	free(file_buffer);
	exit(1);
    }

    printf("Read bootloder section\n");
    len = 320 * KB; //Flash boot loader section length
    buffer = (unsigned char *) (file_buffer + 0x0); /* 0x50000 is the start of bootloader secsion*/
    fwrite(buffer, len, 1, fd);
    fclose(fd);

    fd = fopen(PARAMETER_FILE, "wb");
    if(fd ==NULL ){
        printf("The file %s is not available\n", PARAMETER_FILE);
	free(file_buffer);
	exit(1);
    }

    printf("Read parameter section\n");
    len = 192 * KB; //Flash parameter section length
    buffer = (unsigned char *) (file_buffer + 0x50000); /* 0x50000 is the start of parameter secsion*/
    fwrite(buffer, len, 1, fd);
    fclose(fd);


    fd = fopen(FW_FW, "wb");
    if(fd ==NULL ){
        printf("The file %s could not openned\n", FW_FW);
	free(file_buffer);
	exit(1);
    }

    printf("Read firmware section\n");
    len = 18 * MB; //Flash firmware section length
    buffer = (unsigned char *) (file_buffer + 0x80000); /* 0x80000 is the start of firmware secsion*/
    fwrite(buffer, len, 1, fd);
    fclose(fd);

    printf("Read jffs2 section\n");
    fd = fopen(JFFS2_FILE, "wb");
    if(fd == NULL ){
        printf("The file %s is not available\n", JFFS2_FILE);
	free(file_buffer);
	exit(1);
    }
    len = 9 * MB;
    buffer = (unsigned char *)(file_buffer + 0x01280000);
    fwrite(buffer, 1, len, fd);
    fclose(fd);

    fd = fopen(FW_SMALL, "wb");
    if(fd ==NULL ){
        printf("The file %s could not openned\n", FW_FW);
	free(file_buffer);
	exit(1);
    }

    printf("Read small image section\n");
    len = 4.5 * MB; //Flash firmware section length
    buffer = (unsigned char *) (file_buffer + 0x01B80000); /* 0x01B80000 is the start of firmware secsion*/
    fwrite(buffer, len, 1, fd);

    fclose(fd);
    free(file_buffer);
    exit(0);
}
