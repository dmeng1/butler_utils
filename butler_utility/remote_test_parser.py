# This is remote test result parser python script
# usage remote_test_parser input_log_file_name outputfile_name(.csv)
# This is 
# Tested with qse_tv_logs.txt
#

COMMAND_KEY = "cmd_body"
RESULT_KEY = "FormatVersion"

ERROR = "error"
import linecache
import sys
import re
import json
import base64


title = [ "Command NAME,"
          "Execution,"
          "Success,"
          "Failed,"
          "Line of error"          
]

command = []
command_idx = []
num_cmd_idx = 0
totalLines = 0
count =[]
fail = []
succ =[]
error_line = []
count_index = 0

def parser_delay(infile, outfile):
    with open(outfile, "w")as f:
        for item in title:
            f.write(str(item))
        f.write("\n")
        
        getAllCmdLinesFromFile(infile)
        for i in range(0, num_cmd_idx):
            status, e_line = getResultFromFile(command_idx[i], command_idx[i+1], infile)
            print("status=", status)
            print("e_line=", e_line)
            getCmdFromFile(command_idx[i], infile, status, e_line)
        print(count)
        print (command_idx)
        writeToResultFile(f)
    f.close()
    print("..........................Parsing is done.................")

def writeToResultFile(fp):
    global count_index
    
    for i in range (count_index):
        a_list = []
        a_list.append(command[i])
        a_list.append(count[i])
        a_list.append(succ[i])
        a_list.append(fail[i])
        a_list.append(error_line[i])
        for item in a_list:
            fp.write(str(item)+",")
        fp.write("\n")
    
def parser_main():
    in_file = ""
    arg_num = len(sys.argv)
         
    if (arg_num == 3):
        in_file = sys.argv[1]
        out_file = sys.argv[2]
        print("input file:" + in_file)
        print("output file:" + out_file)
        print(".....................Start parsing......................")
    else:
        print(".............Usage: streaming_parser.py input_file_name output_file_name.csv................")
        exit(0)

    parser_delay(in_file, out_file)
    exit(0)
    
def isCmdInList(cmd, status, e_line):
    global count_index
    try:
        x = int(command.index(cmd))
        #print("Index", x)
        count[x] = int(count[x]) + 1
        if status == 0:
            succ[x] = int(succ[x]) + 1
        else:
            fail[x] = int(fail[x]) + 1
            
    except(ValueError):
        #print("Command",cmd, "is not in command list, add to the comand list")
        command.append(cmd)
        count_index = count_index +1
        count.append(1)
        if status == 0:
            succ.append(1)
            fail.append(0)
            error_line.append(0)
        else:
            fail.append(1)
            error_line.append(e_line)
            succ.append(0)
        

def getAllCmdLinesFromFile(file):
    global num_cmd_idx
    
    command_str = ""
    i=0;   
    for line in open(file, "r"):
        i = i+1
        if(re.search(COMMAND_KEY,line)):
            #print(line)
            command_idx.append(i);
            num_cmd_idx = num_cmd_idx + 1
    command_idx.append(i+1)
    
def getCmdFromFile(index, infile, status, e_line):
    #print("index", index)
    line_str= linecache.getline(infile, index)
    base64_str = line_str.split()[2]
    #print("encoded_cmd", base64_str)
    mystr_bytes = base64_str.encode("ascii")
    base64_string_bytes = base64.b64decode(mystr_bytes)
    decode_cmd = base64_string_bytes.decode('ascii')
    #print("decode_cmd", decode_cmd)
    cmd_json_obj = json.loads(decode_cmd)
    if "function" in cmd_json_obj:
        cmd = cmd_json_obj["function"]
        print(" cmd_str", cmd)
        isCmdInList(cmd, status, e_line)

#get result from the command execution
#if error is in result, return 1 and command line number
#if no error in the result, return 0, 0
        
def getResultFromFile(start_idx, end_idx, infile):
    #print("index", index)
    for i in range(start_idx, end_idx):
        line_str= linecache.getline(infile, i)
        if RESULT_KEY in line_str:
            result_json_obj = json.loads(line_str)
            #print(result_json_obj)
            if "Actions" in result_json_obj:
                actions_array = result_json_obj["Actions"]
                #print("actions_array", actions_array)
                for item in actions_array:
                    if "ActionResult" in item:
                        #print(item)
                        base64_str = item["ActionResult"]
                        print("encoded_result", base64_str)
                        mystr_bytes = base64_str.encode("ascii")
                        base64_string_bytes = base64.b64decode(mystr_bytes)
                        decode_result = base64_string_bytes.decode('ascii')
                        print("decode_result", decode_result)
                        if "error" in decode_result:
                            err = decode_result["error"]
                            if rrr["code"] != 0:
                                return 1, start_idx
        return 0, 0
        
if __name__ == '__main__':
    parser_main()


