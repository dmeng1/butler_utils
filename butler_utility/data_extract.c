#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <mtd/mtd-user.h>
#include <unistd.h>

#include "uei_image.h"

#define MB 0x100000
#define PARAMETER_FILE "parameter.bin"
#define JFFS2_FILE "jffs2.bin"

void read_flash(char * dev_name, int flash_offset, unsigned char *buffer, int length)
{
	int fm, i, j, flash_len;

        fm = open(dev_name, O_RDWR);
	if(fm <= 0){
	   printf("open %s fail\n", dev_name);
	   return; 
	}
	
	lseek(fm,flash_offset, SEEK_SET);

	if(buffer == NULL){
	    printf("No memory buffer\n");
	    close(fm);
	    return; 
	}

        flash_len = read(fm, buffer, length);
        if(flash_len != length){
	    printf("There are less than 0x%x bytes readable from flash. Actual read bytes is 0x%x\n", length, flash_len);
	}
        close(fm);
}

int main() //This will generate two files parameter.bin and jffs2.bin 
{
    unsigned char *buffer = NULL;
    FILE *fd;
    int len;

    fd = fopen(PARAMETER_FILE, "wb");
    if(fd ==NULL ){
        printf("The file %s is not available\n", PARAMETER_FILE);
	return(0);
    }

    printf("Read Linux section\n");
    len = 0x30000; //Flash parameter section start length
    buffer = (unsigned char *) malloc(len);
    read_flash(linux_dev, 0x50000, buffer, len); //0x50000 is the parametre start address in flash
    fwrite(buffer, len, 1, fd);
    fclose(fd);


    printf("Read jffs2 section\n");

    fd = fopen(JFFS2_FILE, "wb");
    if(fd == NULL ){
        printf("The file %s is not available\n", JFFS2_FILE);
	return(0);
    }
    len = 9 * MB;
    buffer = (unsigned char *) realloc(buffer, len);
    read_flash(jffs_dev, 0, buffer, len);
    fwrite(buffer, len, 1, fd);

    free(buffer);
    fclose(fd);

    return 1;
}
