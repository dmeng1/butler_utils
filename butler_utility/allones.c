#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv){
    unsigned int mb, i,j;
    char f_name[256];
    FILE *fp = NULL;
    unsigned char buffer_1k[1024];

   if(argc < 2) {
	  printf("usage: allones xx (MB)\n");
	  exit(0); 
   }

   mb = atoi( argv[1]);
   sprintf(f_name, "all_ones_%dmb.dat",mb);
   printf("This program will generate %d MB all ones file %s\n", mb, f_name);
  
   fp=fopen(f_name, "wb");

   if(fp == NULL){
	   printf("Could not open file %s to write\n");
	   exit(0);
   }

   memset(buffer_1k, 0xFF, 1024);

   for(i =0; i< mb; i++){
	   for (j=0; j<1024; j++)
	   fwrite(buffer_1k, 1024, 1, fp);
   }

   fclose(fp);
   return 0;
}
