#ifndef __ASYM_KEY_H__
#define __ASYM_KEY_H__

#define AZURE_IOT_KEY_NAME      "azure_iot_key.txt"
#define IOT_PRIVATE_KEY         "private_clear.key"
#define IOT_PUBLIC_CER          "public.cert"
#define IOT_HOST_NAME           "host_name.txt"

#define IOT_PRIVATE_LEN         2048
#define IOT_PUBLIC_LEN          2048
#define IOT_HOST_NAME_LEN       50
#define CONNECTION_LEN          150

#define MAX_IN_BUF_SZ       4146 
#define FLASH_LEN           640 
#define ENC_BUF_LEN         4096 
#define BASE64_LEN          5462
#define ENC_VECTOR_LEN      32
#define PAD_LENGTH          32
#define HOST_TAG        "HostName="
#define HOST_TAG_LEN    9
#define DEVICE_TAG      "DeviceId=NB-"
#define DEVICE_TAG_LEN  12
#define X509_TAG        "x509=true"
#define X509_TAG_LEN    9

extern void show_str(const char * input_str, char *title);
extern void show_bit(const char *input_array, int len, char *title);
extern void show_connection_str(char * connection_str);
extern void store_secret(void);
extern void get_secret(char *private, char *cer, char *host_name);
#endif
