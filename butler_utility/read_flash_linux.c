#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <mtd/mtd-user.h>
#include <unistd.h>


#include "uei_image.h"

#define __DEBUG__ 	1 

int endian_adjust(int input)
{
	int out1, out2;

	out1 =(input >> 16);
	out1 = (out1 >> 8)|(out1 & 0xff) << 8;

	out2 = (input & 0xffff);
        out2 = (out2 >> 8) | (out2 & 0xff) << 8;
	return (out2 << 16| out1);
}

void read_flash(char * dev_name, int flash_offset, unsigned char *buffer, int length)
{
	int fm, i, j, flash_len;

        fm = open(dev_name, O_RDWR);
	if(fm <= 0){
	   printf("open %s fail\n", linux_dev);
	   return; 
	}
	
	lseek(fm,flash_offset, SEEK_SET);

	if(buffer == NULL){
	    printf("No memory buffer\n");
	    close(fm);
	    return; 
	}

        flash_len = read(fm, buffer, length);
        if(flash_len != length){
	    printf("There are less than 0x%x bytes readable from flash\n", length);
	}
#ifdef __DEBUG__
	j = 0;
	for(i = 0; i <  256; i++){	
	      printf("%2x ", buffer[i]);
	      if(j == 15){
		      printf("\n");
		      j=0;
	      }
	      else 
		      j++;
	}
        printf("\n\n");
#endif 
        free(buffer);
        close(fm);
}

int write_flash(char *dev_name, int flash_offset, unsigned char *buffer, int buffer_offset, int length)
{
	int fm, flash_len;
        mtd_info_t mtd_info;
        erase_info_t ei;
	unsigned char *tmp;

        fm = open(dev_name, O_RDWR);
	if(fm <= 0){
	   printf("open %s fail\n", dev_name);
	   return(-1); 
	}
	
	ioctl(fm, MEMGETINFO, &mtd_info);
	printf("MTD Type: %x\n MTD total size: %x bytes\n MTD erase size:%x bytes\n", mtd_info.type, mtd_info.size, mtd_info.erasesize);

        if(flash_offset % mtd_info.erasesize){
	    printf("The flash_offset(0x%x) is NOT multiply of erase_size(0x%x)\n", flash_offset, mtd_info.erasesize);
	}

	ei.length = mtd_info.erasesize;

	for(ei.start = flash_offset; ei.start < mtd_info.size; ei.start += ei.length)
	{
		ioctl(fm, MEMUNLOCK, &ei);
		ioctl(fm, MEMERASE, &ei);
		printf(".");
	}
        printf("\n\n");

	lseek(fm, flash_offset, SEEK_SET);

	if(buffer == NULL){
	    printf("No memory buffer\n");
	    close(fm);
	    return; 
	}

	tmp = (unsigned char *) (buffer + buffer_offset);

        flash_len = write(fm, tmp, length);
        if(flash_len != length){
	    printf("There are less than 0x%x bytes writeable to flash wirte length:0x%x\n", length, flash_len);
	}
        close(fm);
	return(flash_len);
}


unsigned char *read_file(char * file_name,  int in_offset, int *out_len)
{
	FILE *fd;
	int file_len, i, j;
	unsigned char hbuf[16];
	IMG_HEADER_T *header;
	unsigned char *out_buf = NULL;

	fd = fopen(file_name, "rb");

	if(fd ==NULL ){
		printf("The file %s is not available\n", file_name);
		return NULL;
	}
        
	fseek(fd, 0L, SEEK_SET);
	fread(hbuf, sizeof(IMG_HEADER_T), 1, fd);
	header = (IMG_HEADER_T *)hbuf;
        
	header->startAddr = endian_adjust(header->startAddr);
	header->burnAddr = endian_adjust(header->burnAddr);
	header->len = endian_adjust(header->len);
        
	out_buf = (unsigned char *)malloc(header->len + 16);

	if(out_buf == NULL)
	{
		printf("Out of system memory\n");
		return NULL;
	}

#ifdef __DEBUG__
	printf("The file %s signature =%4s, start_addr =0x%x, burn_address=0x%x length is 0x%x\n", file_name, header->signature, header->startAddr, header->burnAddr, header->len);
#endif

	fseek(fd, in_offset, SEEK_SET);
	fread(out_buf, header->len + 16, 1, fd);

#ifdef __DEBUG__
	printf("The first 256 bytes of the file: %s\n", file_name);
	j = 0;
	for(i=0; i< 256; i++) {
	      printf(" 0x%2x ", out_buf[i]);
	      if(j == 15){
		      printf("\n");
		      j=0;
	      }
	      else 
		      j++;
        }
	printf("\n\n");
	
	printf("The last 256 bytes of the file %s\n", file_name);
	j = 0;
	for(i=(header->len + 16 - 256) ; i< (header->len + 16); i++){
		printf(" 0x%2x ", out_buf[i]);
	      if(j == 15){
		      printf("\n");
		      j=0;
	      }
	      else 
		      j++;
	}
	printf("\n\n");
#endif
	fclose(fd);

	*out_len = header->len;
        
	return(out_buf);
}

int upgrade_rootfs_from_mem(unsigned char * buffer, int len)
{
    if(buffer == NULL ){
        printf(" rootfs buffer is NULL\n");
        return(-1);
    }

    write_flash(rootfs_dev_write, 0, buffer, ROOTFS_BUFFER_OFFSET, len);
}

int  upgrade_rootfs_from_file(void)
{
    unsigned char *buffer;
    int len;

    buffer = read_file(ROOT_BIN, 0, &len);

    if(buffer == NULL ){
        printf("Outof memory\n");
        return(-1);
    }

    upgrade_rootfs_from_mem(buffer, len);

    free(buffer);
}

int main()
{
	unsigned char *buffer;
	int i, len;
#if 0
	buffer = read_file(LINUX_BIN, 16, &len);
	if(buffer != NULL ){
	   printf("File %s has length:0x%x\n", LINUX_BIN, len);
	   free(buffer);	
	}
#endif

#if 0
        write_flash(LINUX_, 0, buffer, 256);
        
	buffer = (unsigned char *) malloc(256);

        read_flash(linux_dev, LINUX_FLASH_OFFSET, buffer, 256);

        read_flash(rootfs_dev, ROOTFS_OFFSET, buffer, 256);

	buffer = (unsigned char *) malloc(0x20000);

        if(buffer == NULL){
		printf("Out off memory\n");
	       return;
	}
        for (i =0; i < 0x20000; i++)
	     buffer[i] = i;

        //write_flash(rootfs_dev, (ROOTFS_OFFSET + 0xB6070), buffer, 256);
        write_flash("/dev/mtd2", 0x50000, buffer, 0,  0x20000);
#endif
	buffer = (unsigned char *) malloc(258);

        for (i =0; i < 258; i++)
	     buffer[i] = 0;

	printf("%s first 256 bytes\n", linux_dev);

        read_flash(linux_dev, LINUX_FLASH_OFFSET, buffer, 256);

	printf("%s last 256 bytes\n", linux_dev);

        read_flash(linux_dev, (0x50000 + 0x15e816 - 258), buffer, 258);

	free(buffer);

	return 1;
}

