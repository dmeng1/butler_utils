#include <sys/ioctl.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#define Addr_GND  0x64//7 bit format is 0x64
#define Addr_VCC  0xCE//7 bit format is 0x67
#define Addr_SCL  0xCA//7 bit format is 0x65
#define Addr_SDA  0xCC//7 bit format is 0x66
int file=-1;
int IS_IIC_WriteByte(int file, unsigned char i2caddr, unsigned char addr, unsigned char data);
unsigned char PWM_Gamma64[64]=
{
  0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
  0x08,0x09,0x0b,0x0d,0x0f,0x11,0x13,0x16,
  0x1a,0x1c,0x1d,0x1f,0x22,0x25,0x28,0x2e,
  0x34,0x38,0x3c,0x40,0x44,0x48,0x4b,0x4f,
  0x55,0x5a,0x5f,0x64,0x69,0x6d,0x72,0x77,
  0x7d,0x80,0x88,0x8d,0x94,0x9a,0xa0,0xa7,
  0xac,0xb0,0xb9,0xbf,0xc6,0xcb,0xcf,0xd6,
  0xe1,0xe9,0xed,0xf1,0xf6,0xfa,0xfe,0xff
};



void usage() {
	printf("    Usage: cur is LED current level 0=>20 mA; 1=>15 mA; 2=>10 mA; 3=>5 mA\n");
	printf("\t led_control ------- Show this usage \n" );
	printf("\t led_control 0 cur---- Turn off all LEDs\n");
        printf("\t led_control 1 cur---- Bright all\n");
        printf("\t led_control 2 cur ---- Dim red\n");
        printf("\t led_control 3 cur ---- Bright orange\n");    
        printf("\t led_control 4 cur ---- Bright white round\n");    
        printf("\t led_control 7 cur ---- Bright orange blink\n");
        printf("\t led_control 8 cur ---- Dim blue\n");
        printf("\t led_control 9 cur ---- Dim blue blink\n");    
        printf("\t led_control 10 cur-----Dim blue round \n");
        printf("\t led_control 11 cur ----Orange_bounce_back\n");
        printf("\t led_control 20 cur ----Blue tooth pairing and advertisement mode\n");
        printf("\t led_control 21 cur ----BLE connected and paired\n");
        printf("\t led_control 22 cur ----Show Connection\n");
        printf("\t led_control 23 cur ----BLE Disconnected\n");
        printf("\t led_control 24 cur ----Configuring WIFI\n");
        printf("\t led_control 25 cur ----WIFI connected\n");
        printf("\t led_control 26 cur ----WIFI Failed\n");
        printf("\t led_control 27 cur ----Ready to set up\n");
        printf("\t led_control 28 cur ----Connecting WIFI\n");
        printf("\t led_control 29 cur ----Downloading/Installing Updates\n");
        printf("\t led_control 30 cur ----heard_wake_word\n");
        printf("\t led_control 31 cur ----heard_ok_google\n");
        printf("\t led_control 32 cur ----busy_processing\n");
        printf("\t led_control 33 cur ----busy_processing_offline_request\n");
        printf("\t led_control 34 cur ----busy_processing_google_request\n");
        printf("\t led_control 35 cur ----busy_processing_google_online_request\n");
        printf("\t led_control 40 cur ----boot_up\n");
        printf("\t led_control 41 cur ----normal_reminder_notification\n");
        printf("\t led_control 42 cur ----ad_reminder_notification\n");
        printf("\t led_control 43 cur ----urgent_reminder_notification\n");
        printf("\t led_control 44 cur ----mute\n");
        printf("\t led_control 45 cur ----adj_butlter_vol\n");
        printf("\t led_control 46 cur ----idle\n");
	printf("\t led_control 100 cur ---Bright Red\n");
	printf("\t led_control 101 cur ---Bright blue\n");
	printf("\t led_control 102 cur --- Bright green\n");
	printf("\t led_control 103 cur --- Blue blink\n");
	printf("\t led_control 104 cur --- round pattern\n");
	printf("\t led_control 999 cur --- error\n");

}

int open_i2c_dev(int i2cbus)
{
	char filename[64];
	int file;
	
	sprintf(filename, "/dev/i2c-%d", i2cbus);
	file = open(filename, O_RDWR);
	if (file < 0) {
		fprintf(stderr, "Error: Could not open file `%s': %s\n", filename, strerror(ENOENT));
	}
	
	return file;
}

static int check_funcs(int file)
{
	unsigned long funcs;
	
	/* check adapter functionality */
	if (ioctl(file, I2C_FUNCS, &funcs) < 0) {
		fprintf(stderr, "Error: Could not get the adapter functionality matrix: %s\n", strerror(errno));
		return -1;
	}
	
	if (!(funcs & I2C_FUNC_I2C)) {
		fprintf(stderr, "Error: Adapter does not have %s capability\n", "I2C transfers");
		return -1;
	}
	
	return 0;
}

int i2c_read(int file, int i2caddr, int addr) {
	__u8 write_buf[2];
	__u8 read_buf[2];
	struct i2c_msg xfer[2];
	struct i2c_rdwr_ioctl_data rdwr;
	int ret, data;
	
	memset(write_buf, 0, sizeof(write_buf));
	memset(read_buf, 0, sizeof(read_buf));
	
	write_buf[0] = (addr & 0xff00) >> 8;
	write_buf[1] = (addr & 0x00ff) >> 0;
	
	xfer[0].addr = i2caddr;
	xfer[0].flags = 0;
	xfer[0].len = 2;
	xfer[0].buf = write_buf;
	
	xfer[1].addr = i2caddr;
	xfer[1].flags = I2C_M_RD;
	xfer[1].len = 2;
	xfer[1].buf = read_buf;
	
	rdwr.msgs = xfer;
	rdwr.nmsgs = 2;
	ret = ioctl(file, I2C_RDWR, &rdwr);
	if (ret != 2) {
		fprintf(stderr, "Error: Sending messages failed: %s\n", strerror(errno));
		return -1;
	}
	
	data = read_buf[1] | read_buf[0] << 8;
	
	return data;
}

int i2c_write(int file, int i2caddr, int addr, int data) {
	__u8 write_buf[4];
	struct i2c_msg xfer[1];
	struct i2c_rdwr_ioctl_data rdwr;
	int ret;
	
	memset(write_buf, 0, sizeof(write_buf));
	
	write_buf[0] = (addr & 0xff00) >> 8;
	write_buf[1] = (addr & 0x00ff) >> 0;
	write_buf[2] = (data & 0xff00) >> 8;
	write_buf[3] = (data & 0x00ff) >> 0;
	
	xfer[0].addr = i2caddr;
	xfer[0].flags = 0;
	xfer[0].len = 4;
	xfer[0].buf = write_buf;
	
	rdwr.msgs = xfer;
	rdwr.nmsgs = 1;
	ret = ioctl(file, I2C_RDWR, &rdwr);
	if (ret != 1) {
		fprintf(stderr, "Error: Sending messages failed: %s\n", strerror(errno));
		return -1;
	}
	
	return 0;
}
void Init_FL3199(void)
{
	int i;
  IS_IIC_WriteByte(file, Addr_GND,0x00,0x01);//normal operation 
  IS_IIC_WriteByte(file, Addr_GND,0x01,0x77);//on/off
  IS_IIC_WriteByte(file, Addr_GND,0x02,0x07);//on/off
  IS_IIC_WriteByte(file, Addr_GND,0x03,0x00); 
  //IS_IIC_WriteByte(file, Addr_GND,0x04,0x00); 
  for(i=0x07;i<0x0F;i++){
	IS_IIC_WriteByte(file, Addr_GND, i, 0x00);
	}//PWM data
  IS_IIC_WriteByte(file, Addr_GND, 0x10, 0x00);//updata
  
}

void set_current_level_FL3199(long level)
{
	switch(level) {
	   case 0:
		IS_IIC_WriteByte(file,Addr_GND,0x04,0x00);
		break;
	   case 1:
		IS_IIC_WriteByte(file,Addr_GND,0x04,0x10);
		break;
	   case 2:
		IS_IIC_WriteByte(file,Addr_GND,0x04,0x20);
		break;
	   case 3:
		IS_IIC_WriteByte(file,Addr_GND,0x04,0x30);
		break;
	   case 4:
		IS_IIC_WriteByte(file,Addr_GND,0x04,0x40);
		break;
	   case 5:
		IS_IIC_WriteByte(file,Addr_GND,0x04,0x50);
		break;
	   case 6:
		IS_IIC_WriteByte(file,Addr_GND,0x04,0x60);
		break;
	   case 7:
		IS_IIC_WriteByte(file,Addr_GND,0x04,0x70);
		break;
	   default:
		IS_IIC_WriteByte(file,Addr_GND,0x04,0x00);
		break;
	}
}

void IS31FL3199_mode2(void)//One shot mode, only in setup
{ 

  IS_IIC_WriteByte(file,Addr_GND,0x00,0x01);//normal operation 
  IS_IIC_WriteByte(file,Addr_GND,0x01,0x77);//on/off
  IS_IIC_WriteByte(file,Addr_GND,0x02,0x07);//on/off
  IS_IIC_WriteByte(file,Addr_GND,0x03,0x70);//One shot mode
  //IS_IIC_WriteByte(file,Addr_GND,0x04,0x00); 
  usleep(500000);
  
  IS_IIC_WriteByte(file,Addr_GND,0x07,0xff);//PWM
  IS_IIC_WriteByte(file,Addr_GND,0x08,0xff);
  IS_IIC_WriteByte(file,Addr_GND,0x09,0xff);
  IS_IIC_WriteByte(file,Addr_GND,0x0a,0xff);
  IS_IIC_WriteByte(file,Addr_GND,0x0b,0xff);
  IS_IIC_WriteByte(file,Addr_GND,0x0c,0xff);
  IS_IIC_WriteByte(file,Addr_GND,0x0d,0xff);
  IS_IIC_WriteByte(file,Addr_GND,0x0e,0xff);
  IS_IIC_WriteByte(file,Addr_GND,0x0f,0xff);
  IS_IIC_WriteByte(file,Addr_GND,0x10,0x00);//update PWM registers

  IS_IIC_WriteByte(file,Addr_GND,0x11,0x00);//T0
  IS_IIC_WriteByte(file,Addr_GND,0x12,0x01);
  IS_IIC_WriteByte(file,Addr_GND,0x13,0x02);
  IS_IIC_WriteByte(file,Addr_GND,0x14,0x05);
  IS_IIC_WriteByte(file,Addr_GND,0x15,0x04);
  IS_IIC_WriteByte(file,Addr_GND,0x16,0x03);
  IS_IIC_WriteByte(file,Addr_GND,0x17,0x06);
  IS_IIC_WriteByte(file,Addr_GND,0x18,0x07);
  IS_IIC_WriteByte(file,Addr_GND,0x19,0x09);

  IS_IIC_WriteByte(file,Addr_GND,0x1a,0xa0);//T1~T3
  IS_IIC_WriteByte(file,Addr_GND,0x1b,0xa0);
  IS_IIC_WriteByte(file,Addr_GND,0x1c,0xa0);

  IS_IIC_WriteByte(file,Addr_GND,0x1d,0x07);//T4
  IS_IIC_WriteByte(file,Addr_GND,0x1e,0x07);
  IS_IIC_WriteByte(file,Addr_GND,0x1f,0x07);
  IS_IIC_WriteByte(file,Addr_GND,0x20,0x07);
  IS_IIC_WriteByte(file,Addr_GND,0x21,0x07);
  IS_IIC_WriteByte(file,Addr_GND,0x22,0x07);
  IS_IIC_WriteByte(file,Addr_GND,0x23,0x07);
  IS_IIC_WriteByte(file,Addr_GND,0x24,0x07);
  IS_IIC_WriteByte(file,Addr_GND,0x25,0x07);
 
  IS_IIC_WriteByte(file,Addr_GND,0x26,0x00);
  IS_IIC_WriteByte(file,Addr_GND,0x03,0x70);//One shot mode
}


void Rgb_Pwm_Control_FL3199(unsigned char datx,unsigned char datR,unsigned char datG,unsigned char datB)//datx RGBx
{
    switch(datx) 
      {
          case 1:IS_IIC_WriteByte(file, Addr_GND, 0x07, datR);//set out1 pwm  R 
                 IS_IIC_WriteByte(file, Addr_GND, 0x08, datG);//set out2 pwm  G
                 IS_IIC_WriteByte(file, Addr_GND, 0x09, datB);//set out3 pwm  B
                 break;
          case 2:IS_IIC_WriteByte(file, Addr_GND, 0x0A, datR);//set out4 pwm  R
                 IS_IIC_WriteByte(file, Addr_GND, 0x0B, datG);//set out5 pwm  G
                 IS_IIC_WriteByte(file, Addr_GND, 0x0C, datB);//set out6 pwm  B
                 break;
          case 3:IS_IIC_WriteByte(file, Addr_GND, 0x0D, datR);//set out7 pwm  R
                 IS_IIC_WriteByte(file, Addr_GND, 0x0E, datG);//set out8 pwm  G
                 IS_IIC_WriteByte(file, Addr_GND, 0x0F, datB);//set out9 pwm  B
                 break;
          default:break;
      }
    IS_IIC_WriteByte(file, Addr_GND, 0x10, 0x00);//update PWM registers
}


void IS31FL3199_mode1(void)//need to run in loop
{
	int j;
  Init_FL3199();
  usleep(500000);
  usleep(500000);
  for (j=0;j<64;j++)//all LED ramping up
  {
    Rgb_Pwm_Control_FL3199(1, PWM_Gamma64[j],PWM_Gamma64[j],PWM_Gamma64[j]);
    Rgb_Pwm_Control_FL3199(2, PWM_Gamma64[j],PWM_Gamma64[j],PWM_Gamma64[j]);
    Rgb_Pwm_Control_FL3199(3, PWM_Gamma64[j],PWM_Gamma64[j],PWM_Gamma64[j]);
    usleep(10000/4);//10/4ms
  }
  sleep(1); //keep on 1s
  for (j=63;j>=0;j--)//all LED ramping down
  {
    Rgb_Pwm_Control_FL3199(1, PWM_Gamma64[j],PWM_Gamma64[j],PWM_Gamma64[j]);
    Rgb_Pwm_Control_FL3199(2, PWM_Gamma64[j],PWM_Gamma64[j],PWM_Gamma64[j]);
    Rgb_Pwm_Control_FL3199(3, PWM_Gamma64[j],PWM_Gamma64[j],PWM_Gamma64[j]);
    usleep(10000/2);//10/2ms
  } 
  sleep(1); //keep off 1s
}



int IS_IIC_WriteByte(int file, unsigned char i2caddr, unsigned char addr, unsigned char data) {
	__u8 write_buf[4];
	struct i2c_msg xfer[1];
	struct i2c_rdwr_ioctl_data rdwr;
	int ret;
	
	memset(write_buf, 0, sizeof(write_buf));
	
	write_buf[0] = addr;
	write_buf[1] = data;
	
	xfer[0].addr = i2caddr;
	xfer[0].flags = 0;
	xfer[0].len = 2;
	xfer[0].buf = write_buf;
	
	rdwr.msgs = xfer;
	rdwr.nmsgs = 1;
	ret = ioctl(file, I2C_RDWR, &rdwr);
	if (ret != 1) {
		fprintf(stderr, "Error: Sending messages failed: %s\n", strerror(errno));
		return -1;
	}
	
	return 0;
}

void delight_FL3199( void ) {
       Rgb_Pwm_Control_FL3199(1, 0, 0, 0);
       Rgb_Pwm_Control_FL3199(2, 0, 0, 0);
       Rgb_Pwm_Control_FL3199(3, 0, 0, 0);
       Init_FL3199();
}
void light_all_FL3199( void ) {
       Rgb_Pwm_Control_FL3199(1, 255, 255,255);
       Rgb_Pwm_Control_FL3199(2, 255, 255,255);
       Rgb_Pwm_Control_FL3199(3, 255, 255,255);
}

void set_all_color(int red, int green, int blue) {
       Rgb_Pwm_Control_FL3199(1, red, green, blue);
       Rgb_Pwm_Control_FL3199(2, red, green, blue);
       Rgb_Pwm_Control_FL3199(3, red, green, blue);
}

void dim_red_FL3199( void ) {
       Rgb_Pwm_Control_FL3199(1, 60, 0, 0);
       Rgb_Pwm_Control_FL3199(2, 60, 0, 0);
       Rgb_Pwm_Control_FL3199(3, 60, 0, 0);
}

void dim_blue_FL3199( void ) {
       Rgb_Pwm_Control_FL3199(1, 0, 0, 40);
       Rgb_Pwm_Control_FL3199(2, 0, 0, 40);
       Rgb_Pwm_Control_FL3199(3, 0, 0, 40);
}

void bright_red_FL3199( void ) {
       Rgb_Pwm_Control_FL3199(1, 255, 0, 0);
       Rgb_Pwm_Control_FL3199(2, 255, 0, 0);
       Rgb_Pwm_Control_FL3199(3, 255, 0, 0);
}

void bright_green_FL3199( void ) {
       Rgb_Pwm_Control_FL3199(1, 0, 255, 0);
       Rgb_Pwm_Control_FL3199(2, 0, 255, 0);
       Rgb_Pwm_Control_FL3199(3, 0, 255, 0);
}
void bright_blue_FL3199( void ) {
       Rgb_Pwm_Control_FL3199(1, 0, 0, 255);
       Rgb_Pwm_Control_FL3199(2, 0, 0, 255);
       Rgb_Pwm_Control_FL3199(3, 0, 0, 255);
}

void bright_cyan_FL3199( void ) {
       Rgb_Pwm_Control_FL3199(1, 0, 255, 255);
       Rgb_Pwm_Control_FL3199(2, 0, 255, 255);
       Rgb_Pwm_Control_FL3199(3, 0, 255, 255);
}

void bright_orange_FL3199( void ) {
       Rgb_Pwm_Control_FL3199(1, 237, 109, 0);
       Rgb_Pwm_Control_FL3199(2, 237, 109, 0);
       Rgb_Pwm_Control_FL3199(3, 237, 109, 0);
}

void bright_orange_quick_on_off_FL3199( void ) {
       IS_IIC_WriteByte(file,Addr_GND,0x00,0x01);//normal operation 
       IS_IIC_WriteByte(file,Addr_GND,0x01,0x77);//on/off
       IS_IIC_WriteByte(file,Addr_GND,0x02,0x07);//on/off
       IS_IIC_WriteByte(file,Addr_GND,0x03,0x70);//One shot mode
       //IS_IIC_WriteByte(file,Addr_GND,0x04,0x00); 
       usleep(500000);

       Rgb_Pwm_Control_FL3199(1, 237, 109, 0);
       Rgb_Pwm_Control_FL3199(2, 237, 109, 0);
       Rgb_Pwm_Control_FL3199(3, 237, 109, 0);

       IS_IIC_WriteByte(file,Addr_GND,0x11,0x00);//T0
       IS_IIC_WriteByte(file,Addr_GND,0x12,0x00);
       IS_IIC_WriteByte(file,Addr_GND,0x13,0x00);
       IS_IIC_WriteByte(file,Addr_GND,0x14,0x00);
       IS_IIC_WriteByte(file,Addr_GND,0x15,0x00);
       IS_IIC_WriteByte(file,Addr_GND,0x16,0x00);
       IS_IIC_WriteByte(file,Addr_GND,0x17,0x00);
       IS_IIC_WriteByte(file,Addr_GND,0x18,0x00);
       IS_IIC_WriteByte(file,Addr_GND,0x19,0x00);

       IS_IIC_WriteByte(file,Addr_GND,0x1a,0xa0);//T1~T3
       IS_IIC_WriteByte(file,Addr_GND,0x1b,0xa0);
       IS_IIC_WriteByte(file,Addr_GND,0x1c,0xa0);

       IS_IIC_WriteByte(file,Addr_GND,0x1d,0x07);//T4
       IS_IIC_WriteByte(file,Addr_GND,0x1e,0x07);
       IS_IIC_WriteByte(file,Addr_GND,0x1f,0x07);
       IS_IIC_WriteByte(file,Addr_GND,0x20,0x07);
       IS_IIC_WriteByte(file,Addr_GND,0x21,0x07);
       IS_IIC_WriteByte(file,Addr_GND,0x22,0x07);
       IS_IIC_WriteByte(file,Addr_GND,0x23,0x07);
       IS_IIC_WriteByte(file,Addr_GND,0x24,0x07);
       IS_IIC_WriteByte(file,Addr_GND,0x25,0x07);

       IS_IIC_WriteByte(file,Addr_GND,0x26,0x00);
       IS_IIC_WriteByte(file,Addr_GND,0x03,0x70);//One shot mode
}

void bright_1s_blink_FL3199( int option) {

       IS_IIC_WriteByte(file,Addr_GND,0x00,0x01);//normal operation 
       IS_IIC_WriteByte(file,Addr_GND,0x01,0x77);//on/off
       IS_IIC_WriteByte(file,Addr_GND,0x02,0x07);//on/off
       IS_IIC_WriteByte(file,Addr_GND,0x03,0x70);//One shot mode
       //IS_IIC_WriteByte(file,Addr_GND,0x04,0x00); 
       usleep(500000);
       if(option == 0) {
           dim_blue_FL3199();
       } else {
           bright_blue_FL3199();
       }
       IS_IIC_WriteByte(file,Addr_GND,0x11,0x00);//T0 = 0
       IS_IIC_WriteByte(file,Addr_GND,0x12,0x00);
       IS_IIC_WriteByte(file,Addr_GND,0x13,0x00);
       IS_IIC_WriteByte(file,Addr_GND,0x14,0x00);
       IS_IIC_WriteByte(file,Addr_GND,0x15,0x00);
       IS_IIC_WriteByte(file,Addr_GND,0x16,0x00);
       IS_IIC_WriteByte(file,Addr_GND,0x17,0x00);
       IS_IIC_WriteByte(file,Addr_GND,0x18,0x00);
       IS_IIC_WriteByte(file,Addr_GND,0x19,0x00);

       IS_IIC_WriteByte(file,Addr_GND,0x1a,0x17);//T1,T3=0; T2=256ms
       IS_IIC_WriteByte(file,Addr_GND,0x1b,0x17);
       IS_IIC_WriteByte(file,Addr_GND,0x1c,0x17);

       IS_IIC_WriteByte(file,Addr_GND,0x1d,0x01);//T4 256ms
       IS_IIC_WriteByte(file,Addr_GND,0x1e,0x01);
       IS_IIC_WriteByte(file,Addr_GND,0x1f,0x01);
       IS_IIC_WriteByte(file,Addr_GND,0x20,0x01);
       IS_IIC_WriteByte(file,Addr_GND,0x21,0x01);
       IS_IIC_WriteByte(file,Addr_GND,0x22,0x01);
       IS_IIC_WriteByte(file,Addr_GND,0x23,0x01);
       IS_IIC_WriteByte(file,Addr_GND,0x24,0x01);
       IS_IIC_WriteByte(file,Addr_GND,0x25,0x01);

       IS_IIC_WriteByte(file,Addr_GND,0x26,0x00);
       IS_IIC_WriteByte(file,Addr_GND,0x03,0x70);//One shot mode
}

void dim_blue_round_FL3199(void)//One shot mode, only in setup
{ 

  IS_IIC_WriteByte(file,Addr_GND,0x00,0x01);//normal operation 
  IS_IIC_WriteByte(file,Addr_GND,0x01,0x77);//on/off
  IS_IIC_WriteByte(file,Addr_GND,0x02,0x07);//on/off
  IS_IIC_WriteByte(file,Addr_GND,0x03,0x70);//One shot mode
  //IS_IIC_WriteByte(file,Addr_GND,0x04,0x00); 
  usleep(500000);
  
  dim_blue_FL3199();

  IS_IIC_WriteByte(file,Addr_GND,0x11,0x00);//T0
  IS_IIC_WriteByte(file,Addr_GND,0x12,0x00);
  IS_IIC_WriteByte(file,Addr_GND,0x13,0x00);
  IS_IIC_WriteByte(file,Addr_GND,0x14,0x00);
  IS_IIC_WriteByte(file,Addr_GND,0x15,0x00);
  IS_IIC_WriteByte(file,Addr_GND,0x16,0x01);
  IS_IIC_WriteByte(file,Addr_GND,0x17,0x00);
  IS_IIC_WriteByte(file,Addr_GND,0x18,0x00);
  IS_IIC_WriteByte(file,Addr_GND,0x19,0x02);

  IS_IIC_WriteByte(file,Addr_GND,0x1a,0x17);//T1~T3
  IS_IIC_WriteByte(file,Addr_GND,0x1b,0x17);
  IS_IIC_WriteByte(file,Addr_GND,0x1c,0x17);

  IS_IIC_WriteByte(file,Addr_GND,0x1d,0x02);//T4
  IS_IIC_WriteByte(file,Addr_GND,0x1e,0x02);
  IS_IIC_WriteByte(file,Addr_GND,0x1f,0x02);
  IS_IIC_WriteByte(file,Addr_GND,0x20,0x02);
  IS_IIC_WriteByte(file,Addr_GND,0x21,0x02);
  IS_IIC_WriteByte(file,Addr_GND,0x22,0x02);
  IS_IIC_WriteByte(file,Addr_GND,0x23,0x02);
  IS_IIC_WriteByte(file,Addr_GND,0x24,0x02);
  IS_IIC_WriteByte(file,Addr_GND,0x25,0x02);
 
  IS_IIC_WriteByte(file,Addr_GND,0x26,0x00);
}

void white_round_FL3199(void)//One shot mode, only in setup Bounce back
{ 

  IS_IIC_WriteByte(file,Addr_GND,0x00,0x01);//normal operation 
  IS_IIC_WriteByte(file,Addr_GND,0x01,0x77);//on/off
  IS_IIC_WriteByte(file,Addr_GND,0x02,0x07);//on/off
  IS_IIC_WriteByte(file,Addr_GND,0x03,0x70);//One shot mode
  //IS_IIC_WriteByte(file,Addr_GND,0x04,0x00); 
  usleep(500000);
  
  light_all_FL3199();

  IS_IIC_WriteByte(file,Addr_GND,0x11,0x00);//T0
  IS_IIC_WriteByte(file,Addr_GND,0x12,0x00);
  IS_IIC_WriteByte(file,Addr_GND,0x13,0x00);

  IS_IIC_WriteByte(file,Addr_GND,0x14,0x03);
  IS_IIC_WriteByte(file,Addr_GND,0x15,0x03);
  IS_IIC_WriteByte(file,Addr_GND,0x16,0x03);

  IS_IIC_WriteByte(file,Addr_GND,0x17,0x06);
  IS_IIC_WriteByte(file,Addr_GND,0x18,0x06);
  IS_IIC_WriteByte(file,Addr_GND,0x19,0x06);

  IS_IIC_WriteByte(file,Addr_GND,0x1a,0x10);//T1~T3
  IS_IIC_WriteByte(file,Addr_GND,0x1b,0x10);
  IS_IIC_WriteByte(file,Addr_GND,0x1c,0x10);

  IS_IIC_WriteByte(file,Addr_GND,0x1d,0x09);//T4
  IS_IIC_WriteByte(file,Addr_GND,0x1e,0x09);
  IS_IIC_WriteByte(file,Addr_GND,0x1f,0x09);

  IS_IIC_WriteByte(file,Addr_GND,0x20,0x03);
  IS_IIC_WriteByte(file,Addr_GND,0x21,0x03);
  IS_IIC_WriteByte(file,Addr_GND,0x22,0x03);

  IS_IIC_WriteByte(file,Addr_GND,0x23,0x09);
  IS_IIC_WriteByte(file,Addr_GND,0x24,0x09);
  IS_IIC_WriteByte(file,Addr_GND,0x25,0x09);
 
  IS_IIC_WriteByte(file,Addr_GND,0x26,0x00);
}

/*************************************************************
 * color  will be as RGB in pulse_2s
 * **********************************************************/

void pulse_2s(int red, int green, int blue )//One shot mode, only in setup
{
  IS_IIC_WriteByte(file, Addr_GND,0x00,0x01);//normal operation 
  IS_IIC_WriteByte(file, Addr_GND,0x01,0x77);//on/off
  IS_IIC_WriteByte(file, Addr_GND,0x02,0x07);//on/off
  IS_IIC_WriteByte(file, Addr_GND,0x03,0x70);//One shot mode
  //IS_IIC_WriteByte(file, Addr_GND,0x04,0x00);
  usleep(500000);

  set_all_color(red, green, blue);
  
  IS_IIC_WriteByte(file, Addr_GND,0x11,0x00);//T0
  IS_IIC_WriteByte(file, Addr_GND,0x12,0x00);
  IS_IIC_WriteByte(file, Addr_GND,0x13,0x00);

  IS_IIC_WriteByte(file, Addr_GND,0x14,0x00);
  IS_IIC_WriteByte(file, Addr_GND,0x15,0x00);
  IS_IIC_WriteByte(file, Addr_GND,0x16,0x00);

  IS_IIC_WriteByte(file, Addr_GND,0x17,0x00);
  IS_IIC_WriteByte(file, Addr_GND,0x18,0x00);
  IS_IIC_WriteByte(file, Addr_GND,0x19,0x00);

  IS_IIC_WriteByte(file, Addr_GND,0x1a,0x21);//T1=T3=T2 = 520 ms
  IS_IIC_WriteByte(file, Addr_GND,0x1b,0x21);
  IS_IIC_WriteByte(file, Addr_GND,0x1c,0x21);

  IS_IIC_WriteByte(file, Addr_GND,0x1d,0x11);//T4=i520 ms
  IS_IIC_WriteByte(file, Addr_GND,0x1e,0x11);
  IS_IIC_WriteByte(file, Addr_GND,0x1f,0x11);

  IS_IIC_WriteByte(file, Addr_GND,0x20,0x11);
  IS_IIC_WriteByte(file, Addr_GND,0x21,0x11);
  IS_IIC_WriteByte(file, Addr_GND,0x22,0x11);

  IS_IIC_WriteByte(file, Addr_GND,0x23,0x11);
  IS_IIC_WriteByte(file, Addr_GND,0x24,0x11);
  IS_IIC_WriteByte(file, Addr_GND,0x25,0x11);

  IS_IIC_WriteByte(file, Addr_GND,0x26,0x00);
}

void pulse_1s(int red, int green, int blue )//One shot mode, only in setup
{
  IS_IIC_WriteByte(file, Addr_GND,0x00,0x01);//normal operation 
  IS_IIC_WriteByte(file, Addr_GND,0x01,0x77);//on/off
  IS_IIC_WriteByte(file, Addr_GND,0x02,0x07);//on/off
  IS_IIC_WriteByte(file, Addr_GND,0x03,0x70);//One shot mode
  //IS_IIC_WriteByte(file, Addr_GND,0x04,0x00);
  usleep(500000);

  set_all_color(red, green, blue);
  
  IS_IIC_WriteByte(file, Addr_GND,0x11,0x00);//T0
  IS_IIC_WriteByte(file, Addr_GND,0x12,0x00);
  IS_IIC_WriteByte(file, Addr_GND,0x13,0x00);

  IS_IIC_WriteByte(file, Addr_GND,0x14,0x00);
  IS_IIC_WriteByte(file, Addr_GND,0x15,0x00);
  IS_IIC_WriteByte(file, Addr_GND,0x16,0x00);

  IS_IIC_WriteByte(file, Addr_GND,0x17,0x00);
  IS_IIC_WriteByte(file, Addr_GND,0x18,0x00);
  IS_IIC_WriteByte(file, Addr_GND,0x19,0x00);

  IS_IIC_WriteByte(file, Addr_GND,0x1a,0x10);//T1=T3=T2 = 260 ms
  IS_IIC_WriteByte(file, Addr_GND,0x1b,0x10);
  IS_IIC_WriteByte(file, Addr_GND,0x1c,0x10);

  IS_IIC_WriteByte(file, Addr_GND,0x1d,0x01);//T4=260 ms
  IS_IIC_WriteByte(file, Addr_GND,0x1e,0x01);
  IS_IIC_WriteByte(file, Addr_GND,0x1f,0x01);

  IS_IIC_WriteByte(file, Addr_GND,0x20,0x01);
  IS_IIC_WriteByte(file, Addr_GND,0x21,0x01);
  IS_IIC_WriteByte(file, Addr_GND,0x22,0x01);

  IS_IIC_WriteByte(file, Addr_GND,0x23,0x01);
  IS_IIC_WriteByte(file, Addr_GND,0x24,0x01);
  IS_IIC_WriteByte(file, Addr_GND,0x25,0x01);

  IS_IIC_WriteByte(file, Addr_GND,0x26,0x00);
}

void on_off_1s(int red, int green, int blue )//One shot mode, only in setup
{
  IS_IIC_WriteByte(file, Addr_GND,0x00,0x01);//normal operation 
  IS_IIC_WriteByte(file, Addr_GND,0x01,0x77);//on/off
  IS_IIC_WriteByte(file, Addr_GND,0x02,0x07);//on/off
  IS_IIC_WriteByte(file, Addr_GND,0x03,0x70);//One shot mode
  //IS_IIC_WriteByte(file, Addr_GND,0x04,0x00);
  usleep(500000);

  set_all_color(red, green, blue);
  
  IS_IIC_WriteByte(file, Addr_GND,0x11,0x00);//T0
  IS_IIC_WriteByte(file, Addr_GND,0x12,0x00);
  IS_IIC_WriteByte(file, Addr_GND,0x13,0x00);

  IS_IIC_WriteByte(file, Addr_GND,0x14,0x00);
  IS_IIC_WriteByte(file, Addr_GND,0x15,0x00);
  IS_IIC_WriteByte(file, Addr_GND,0x16,0x00);

  IS_IIC_WriteByte(file, Addr_GND,0x17,0x00);
  IS_IIC_WriteByte(file, Addr_GND,0x18,0x00);
  IS_IIC_WriteByte(file, Addr_GND,0x19,0x00);

  IS_IIC_WriteByte(file, Addr_GND,0x1a,0x27);//T1=T3= 0 ms; T2=520 ms
  IS_IIC_WriteByte(file, Addr_GND,0x1b,0x27);
  IS_IIC_WriteByte(file, Addr_GND,0x1c,0x27);

  IS_IIC_WriteByte(file, Addr_GND,0x1d,0x02);//T4=520 ms
  IS_IIC_WriteByte(file, Addr_GND,0x1e,0x02);
  IS_IIC_WriteByte(file, Addr_GND,0x1f,0x02);

  IS_IIC_WriteByte(file, Addr_GND,0x20,0x02);
  IS_IIC_WriteByte(file, Addr_GND,0x21,0x02);
  IS_IIC_WriteByte(file, Addr_GND,0x22,0x02);

  IS_IIC_WriteByte(file, Addr_GND,0x23,0x02);
  IS_IIC_WriteByte(file, Addr_GND,0x24,0x02);
  IS_IIC_WriteByte(file, Addr_GND,0x25,0x02);

  IS_IIC_WriteByte(file, Addr_GND,0x26,0x00);
}

void boot_up(void)
{
   white_round_FL3199();
}

void normal_reminder_notification(void)
{
    unsigned char delta = 13, pwm;
    int i;
   
    while(1) {
	pwm =0;
        for(i = 0; i < 20; i++)
        {
            Rgb_Pwm_Control_FL3199(1, pwm, pwm, pwm);
            Rgb_Pwm_Control_FL3199(2, pwm, pwm, pwm);
            Rgb_Pwm_Control_FL3199(3, pwm, pwm, pwm);
	    if(pwm <= 242)
               pwm += delta;
	    else 
	        pwm = 255;
  	    usleep(50000);
        }
        printf("pwm = %d\n", pwm);
        for(i = 0; i < 20; i++)
        {
           Rgb_Pwm_Control_FL3199(1, pwm, pwm, pwm);
           Rgb_Pwm_Control_FL3199(2, pwm, pwm, pwm);
           Rgb_Pwm_Control_FL3199(3, pwm, pwm, pwm);
	   if(pwm > delta)
              pwm -= delta;
	   else
	      pwm = 0;
           usleep(50000);
       }
   }
}

void ad_reminder_notification()
{
    pulse_2s(0, 0, 255);
}
 
void urgent_reminder_notification(void)
{
    on_off_1s(255, 0, 0);
}

void mute(void)
{
   set_all_color(255,0,0);
}

void adj_butlter_vol()
{
   set_all_color(0,0,0);
}
 
void idle()
{
   set_all_color(0,0,0);
}

void ble_paring_ad_mode( void ) {
    Rgb_Pwm_Control_FL3199(1, 0, 0,255);
    Rgb_Pwm_Control_FL3199(2, 0, 0, 10);
    Rgb_Pwm_Control_FL3199(3, 0, 0, 10);
    usleep(500000);
    Rgb_Pwm_Control_FL3199(1, 0, 0, 0);
    Rgb_Pwm_Control_FL3199(2, 0, 0, 255);
    Rgb_Pwm_Control_FL3199(3, 0, 0, 0);
    usleep(500000);
    Rgb_Pwm_Control_FL3199(1, 0, 0, 10);
    Rgb_Pwm_Control_FL3199(2, 0, 0, 10);
    Rgb_Pwm_Control_FL3199(3, 0, 0, 255);
    usleep(500000);
    Rgb_Pwm_Control_FL3199(1, 0, 0, 0);
    Rgb_Pwm_Control_FL3199(2, 0, 0, 255);
    Rgb_Pwm_Control_FL3199(3, 0, 0, 0);
    usleep(500000);
}


void ble_connected_paired(void )
{
    bright_blue_FL3199();
}

void show_connection(void)
{
    int i;
    for( i = 0; i < 5; i++)
    {
        bright_blue_FL3199();
        usleep(500000);
        delight_FL3199();
    }
}

void ble_disconnected(void)
{
    int i;
    for(i = 0 ; i < 5; i++)
    {
        bright_red_FL3199();
        usleep(500000);
        delight_FL3199();
    }
}

void config_wifi(void)
{
    Rgb_Pwm_Control_FL3199(1, 0, 255, 0);
    Rgb_Pwm_Control_FL3199(2, 0, 10, 0);
    Rgb_Pwm_Control_FL3199(3, 0, 10, 0);
    usleep(500000);
    Rgb_Pwm_Control_FL3199(1, 0, 0, 0);
    Rgb_Pwm_Control_FL3199(2, 0, 255, 0);
    Rgb_Pwm_Control_FL3199(3, 0, 0, 0);
    usleep(500000);
    Rgb_Pwm_Control_FL3199(1, 0, 10, 0);
    Rgb_Pwm_Control_FL3199(2, 0, 10, 0);
    Rgb_Pwm_Control_FL3199(3, 0, 255,0);
    usleep(500000);
    Rgb_Pwm_Control_FL3199(1, 0, 0, 0);
    Rgb_Pwm_Control_FL3199(2, 0, 255,0);
    Rgb_Pwm_Control_FL3199(3, 0, 0, 0);
    usleep(500000);
}

void wifi_connected(void)
{
    bright_green_FL3199();
    usleep(5000000);
}

void wifi_failed(void)
{
    int i;
    for(i = 0; i < 5; i++) {    
        bright_red_FL3199();
        usleep(500000);
        delight_FL3199();
        usleep(500000);
    }
}

void ready_to_setup(void)
{
    Rgb_Pwm_Control_FL3199(1, 255, 255,255);
    Rgb_Pwm_Control_FL3199(2, 0, 0, 0);
    Rgb_Pwm_Control_FL3199(3, 0, 0, 0);
    usleep(1000000);
    Rgb_Pwm_Control_FL3199(1, 0, 0, 0);
    Rgb_Pwm_Control_FL3199(2, 0, 0, 0);
    Rgb_Pwm_Control_FL3199(3, 255, 255, 255);
    usleep(1000000);

}
void connecting_wifi(void) 
{
    Rgb_Pwm_Control_FL3199(1, 255, 255,255);
    Rgb_Pwm_Control_FL3199(2, 0, 0, 0);
    Rgb_Pwm_Control_FL3199(3, 0, 0, 0);
    usleep(500000);
    Rgb_Pwm_Control_FL3199(1, 0, 0, 0);
    Rgb_Pwm_Control_FL3199(2, 255, 255, 255);
    Rgb_Pwm_Control_FL3199(3, 0, 0, 0);
    usleep(500000);
    Rgb_Pwm_Control_FL3199(1, 0, 0, 0);
    Rgb_Pwm_Control_FL3199(2, 0, 0, 0);
    Rgb_Pwm_Control_FL3199(3, 255, 255, 255);
    usleep(500000);
    Rgb_Pwm_Control_FL3199(1, 0, 0, 0);
    Rgb_Pwm_Control_FL3199(2, 255, 255, 255);
    Rgb_Pwm_Control_FL3199(3, 0, 0, 0);
    usleep(500000);
}

void download_updates(void)
{
    Rgb_Pwm_Control_FL3199(1, 255, 255,255);
    Rgb_Pwm_Control_FL3199(2, 0, 0, 0);
    Rgb_Pwm_Control_FL3199(3, 0, 0, 0);
    usleep(250000);
    Rgb_Pwm_Control_FL3199(1, 0, 0, 0);
    Rgb_Pwm_Control_FL3199(2, 255, 255, 255);
    Rgb_Pwm_Control_FL3199(3, 0, 0, 0);
    usleep(250000);
    Rgb_Pwm_Control_FL3199(1, 0, 0, 0);
    Rgb_Pwm_Control_FL3199(2, 0, 0, 0);
    Rgb_Pwm_Control_FL3199(3, 255, 255, 255);
    usleep(250000);
}

void heard_wake_word()
{
}

void heard_ok_google()
{ 
}

void busy_processing_online_request()
{
}

void busy_processing_offline_request(void)
{
}

void busy_processing_google_request(void)
{
}
 
void busy_processing_google_online_request(void)
{
}

void error(void)
{
    Rgb_Pwm_Control_FL3199(1, 255, 85, 0);
    Rgb_Pwm_Control_FL3199(2, 255, 85, 0);
    Rgb_Pwm_Control_FL3199(3, 255, 85, 0);
}

int main(int argc, char* argv[]) {
	
	long int pattern;
	long level;

	file = open_i2c_dev(2);
	if (file < 0 || check_funcs(file)) {
		printf("The LED control is not enabled\n");
		exit(1);

	}
	//printf("\n\nOPEN I2C file %d\n\n", file);
	
	if(argc < 2) {
                usage();
	        exit(1); //no inputr parameter shows usage
        }

	if(argc < 3){
		set_current_level_FL3199(7);
	} else {
		level = strtol(argv[2],NULL,10);
		set_current_level_FL3199(level);
	}

	pattern = strtol(argv[1], NULL, 10);

	printf("Input paramter is pattern of (%d)\n", pattern);

        delight_FL3199();
        switch(pattern){
		case 0:
		    break;
                case 1:
		    light_all_FL3199();
		    break;
		case 2:
		    dim_red_FL3199();
		    break;
		case 3:
                    bright_orange_FL3199();    
		    break;
		case 4:
                    white_round_FL3199();    
		    break;
		case 7:
		    bright_orange_quick_on_off_FL3199();
		    break;
		case 8:
		    dim_blue_FL3199();
		    break;
		case 9:
		    Init_FL3199();
		    bright_1s_blink_FL3199(0);
		    break;
		case 10:
		    dim_blue_round_FL3199();
		    break;
		case 20:
		    ble_paring_ad_mode();
		    break;
		case 21:
		    ble_connected_paired();
		    break;
		case 22:
		    show_connection();
		    break;
		case 23:
		    ble_disconnected();
		    break;
		case 24:
		    config_wifi();
		    break;
		case 25:
		    wifi_connected();
		    break;
		case 26:
		    wifi_failed();
		    break;
		case 27:
		    ready_to_setup();
		    break;
		case 28:
		    connecting_wifi();
		    break;
		case 29:
		    download_updates();
		    break;
		case 30:
                    heard_wake_word();
                    break;
		case 31:
                    heard_ok_google();
		    break;
		case 32:
                    busy_processing_online_request();
		    break;
		case 33:
                    busy_processing_offline_request();
                    break;
		case 34:
                    busy_processing_google_request();
                    break;
 	        case 35:
                    busy_processing_google_online_request();
		    break;
                case 40:
		    boot_up();
		    break;
		case 41:
		    normal_reminder_notification();
		    break;
		case 42:
		    ad_reminder_notification();
		    break;
		case 43:
		    urgent_reminder_notification();
		    break;
		case 44:
		    mute();
		    break;
		case 45:
		    adj_butlter_vol();
		    break;
		case 100:
		    bright_red_FL3199();
		    break;
		case 101:
		    bright_blue_FL3199();
		    break;
		case 102:
		    bright_green_FL3199();
		    break;
		case 103:
		    Init_FL3199();
		    bright_blue_FL3199();
		    bright_1s_blink_FL3199(1);
		    break;
		case 104:
		    Init_FL3199();
		    IS31FL3199_mode2();
		   break; 
		case 999:
		    error();
		   break; 
		default:
		    break;
	}
	close(file);
	return 0;
}
