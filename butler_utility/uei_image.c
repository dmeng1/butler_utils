#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <linux/fs.h>
#include <mtd/mtd-user.h>
#include <unistd.h>
#include <openssl/md5.h> 
#include <string.h>

#include "uei_image.h"
#include "uei_image_err.h"

#define KERNEL_SIZE		0x200000 /*2   MB*/
#define NORMAL_FS_SIZE		0x1000000 /*16 MB*/
#define SMALL_FS_SIZE		0x280000 /*2.5 MB*/



static void md5_sum(unsigned char *md5, char *buffer, int len);
static void kill_processes(void);
static int fwChecksumOk(char *data, int len);
static int endian_adjust(int input);
static int read_flash(char * dev_name, int flash_offset, unsigned char *buffer, int length);
static int write_flash(char *dev_name, int flash_offset, unsigned char *buffer, int buffer_offset, int length);
static unsigned char *read_partial_file(FILE *fd,  int file_offset, int *out_len);
static int upgrade_rootfs_from_mem(unsigned char * buffer, int len);
static int upgrade_linux_from_mem(unsigned char * buffer, int len);
static int  upgrade_fw(void);
static int detect_file_type(void);
char *read_fw_file_to_buffer(int *sz);

static void md5_sum(unsigned char *md5, char *buffer, int len) {
    MD5_CTX ctx;

    MD5_Init(&ctx);
    MD5_Update(&ctx, buffer, len);
    MD5_Final(md5, &ctx);
    return;    
}

static void kill_processes(void)
{
    printf("upgrade: killing tasks...\n");
    kill(1, SIGTSTP);               /* Stop init from reforking tasks */
    kill(1, SIGSTOP);
    kill(2, SIGSTOP);
    kill(3, SIGSTOP);
    kill(4, SIGSTOP);
    kill(5, SIGSTOP);
    kill(6, SIGSTOP);
    kill(7, SIGSTOP);
    signal(SIGTERM,SIG_IGN);        /* Don't kill ourselves... */
    setpgrp();                      /* Don't let our parent kill us */
    sleep(1);
    signal(SIGHUP, SIG_IGN);        /* Don't die if our parent dies due to
                                     * a closed controlling terminal */
}

static int fwChecksumOk(char *data, int len)
{
    unsigned short sum=0;
    int i;

    for (i=0; i<len; i+=2) {
        sum += WORD_SWAP( *((unsigned short *)&data[i]) );
									        }
    return(sum);
}


static int endian_adjust(int input)
{
	int out1, out2;

	out1 =(input >> 16);
	out1 = (out1 >> 8)|(out1 & 0xff) << 8;

	out2 = (input & 0xffff);
        out2 = (out2 >> 8) | (out2 & 0xff) << 8;
	return (out2 << 16| out1);
}

static int read_flash(char * dev_name, int flash_offset, unsigned char *buffer, int length)
{
	int fm, flash_len;
	int ret = SUCCESS;

        fm = open(dev_name, O_RDWR);
	if(fm <= 0){
	   printf("open %s fail\n", linux_dev);
	   return(FLASH_OPEN_ERROR); 
	}
	
	lseek(fm,flash_offset, SEEK_SET);

        flash_len = read(fm, buffer, length);
        if(flash_len != length){
	    printf("There are less than 0x%x bytes readable from flash\n", length);
	    ret = FLASH_READ_LENGTH_ERROR;
	}

        close(fm);

	return(ret);

}

static int write_flash(char *dev_name, int flash_offset, unsigned char *buffer, int buffer_offset, int length)
{
	int fm, flash_len;
	unsigned char *tmp;
	int ret = SUCCESS;

        fm = open(dev_name, O_RDWR);

	if(fm <= 0){
	   printf("open %s fail\n", dev_name);
	   return(FLASH_OPEN_ERROR); 
	}

	lseek(fm, flash_offset, SEEK_SET);

	tmp = (unsigned char *) (buffer + buffer_offset);

        flash_len = write(fm, tmp, length);

        if(flash_len != length){
	    printf("There are less than 0x%x bytes writeable to flash wirte length:0x%x\n", length, flash_len);
	    close(fm);
	    return(FLASH_WRITE_LNEGTH_ERROR);
	}

        sync();
        
	if(ioctl(fm, BLKFLSBUF, NULL) < 0){
            printf("flush mtd system cache error\n");
	    ret = FLASH_WRITE_ERROR;
        }

        close(fm);

	return(ret);
}


static unsigned char *read_partial_file(FILE *fd,  int file_offset, int *out_len)
{
	IMG_HEADER_T *header;
	unsigned char *out_buf = NULL;
	unsigned char hbuf[20];

	fseek(fd, file_offset, SEEK_SET);
	fread(hbuf, sizeof(IMG_HEADER_T), 1, fd);
	header = (IMG_HEADER_T *)hbuf;
        
	header->startAddr = endian_adjust(header->startAddr);
	header->burnAddr = endian_adjust(header->burnAddr);
	header->len = endian_adjust(header->len);
        
	out_buf = (unsigned char *)malloc(header->len + 16);

	if(out_buf == NULL)
	{
		printf("Out of system memory\n");
		return(NULL);
	}

	fseek(fd, file_offset, SEEK_SET);
	fread(out_buf, header->len + 16, 1, fd);

	*out_len = header->len;
        
	return(out_buf);
}

char *read_fw_file_to_buffer(int *size)
{
    FILE *fp;
    int leng = 0;
    char *buf;


    fp = fopen("/tmp/fw.bin", "rb");
    if(fp == NULL)
    {
        printf("The /tmp/fw.bin is not availablei\n");
	return;
    }

    fseek(fp, 0, SEEK_END);
    leng = ftell(fp);
    rewind(fp);
    
    buf =(char *) malloc(leng + 1);
    
    if(buf == NULL)
    {
      printf("Allocate buffer(%d) failed\n", leng);
    }else
    {
      printf("Allocate buffer(%d) success\n", leng);
    }

    *size = fread(buf, leng, 1, fp);

    printf("Read back data length=%d\n", *size);

    fclose(fp);
    
    return(buf); 

}



static void inValidateFsFlash(void)
{
    unsigned char buf[16] = {0};

    write_flash(rootfs_dev, 0, buf, 0, 16);
}

static int upgrade_rootfs_from_mem(unsigned char * buffer, int len)
{
    int checksum;
    int ret = SUCCESS;
    char tmp[1024] ="";
    int i;

    //unsigned char orig_md5[MD5_DIGEST_LENGTH], new_md5[MD5_DIGEST_LENGTH];

    printf("Update roofs: Write_device %s rootfs_offset=0x%x buffer length=0x%x \n", rootfs_dev, ROOTFS_BUFFER_OFFSET , len);

    checksum = fwChecksumOk((char *) &buffer[ROOTFS_BUFFER_OFFSET], len);
    
    if (checksum != 0) {
        printf("rootfs checksum = 0x%X\n", checksum);
	return(FS_CHECKSUM_ERROR);
    }

    //md5_sum(orig_md5, (char *)(buffer + ROOTFS_BUFFER_OFFSET), len);
    
   // memcpy(tmp, (char*)(buffer + ROOTFS_BUFFER_OFFSET), 1024); /*save 1K datia from beginiing to veryfy*/

    ret = write_flash(rootfs_dev, 0, buffer, ROOTFS_BUFFER_OFFSET, len);

    if(ret != SUCCESS) {
	 printf("Write flash failed\n");
	 //free(buffer);
         return(ret);
    }

    printf("Write fs to flash complete.\n");

#if 0 // Do not read back fs becasue the SQUESH ERROR
    memset(buffer, 0, 1025);
    ret = read_flash(rootfs_dev, 0, buffer, 1024);

    if(memcmp(tmp, buffer, 512) != 0)
    {
	printf("Readback data is diff from write data to flash, write errror\n The readback value:\n");    
	for (i=0; i< 1024; i++)
	    printf(" 0x%02x", buffer[i]);
	printf("\n");

	printf("The original values :\n");    
	for (i=0; i< 1024; i++)
	    printf(" 0x%02x", tmp[i]);
	printf("\n");
        ret = FLASH_WRITE_ERROR;  
    }

    if(ret != SUCCESS) {
        return(ret);
    }

    md5_sum(new_md5, (char *)buffer, len);

    if(memcmp(orig_md5, new_md5,MD5_DIGEST_LENGTH) != 0)
        ret = FS_VALIDATION_ERROR; 	    
    else
        printf("verify fs read from flash complete.\n");
#endif
     
    return(ret);
}

static int upgrade_linux_from_mem(unsigned char * buffer, int len)
{
    int checksum;
    int ret = SUCCESS;
    unsigned char orig_md5[MD5_DIGEST_LENGTH], new_md5[MD5_DIGEST_LENGTH];
    
    printf("Update Linux: Write_device %s flash_offset=0x%x buffer length=0x%x \n", linux_dev, LINUX_FLASH_OFFSET, (len+16));

    checksum = fwChecksumOk((char *) &buffer[16], len);

    if (checksum != 0) {
	     printf("Linux kernel checksum = 0x%X\n", fwChecksumOk((char *)&buffer[16], len));
	     return(KERNELCHECKSUM_ERROR);
    }

    md5_sum(orig_md5, (char *)buffer, (len + 16));

    ret = write_flash(linux_dev, LINUX_FLASH_OFFSET, buffer, 0, (len + 16));

    if(ret != SUCCESS) {
        return(ret);
    }
    printf("Write linux kernel to flash complete.\n");

    memset(buffer, 0, len);
    ret = read_flash(linux_dev, LINUX_FLASH_OFFSET, buffer, (len + 16));

    if(ret != SUCCESS) {
        return(ret);
    }

    md5_sum(new_md5, (char *) buffer, (len + 16));
    
    if(memcmp(orig_md5, new_md5, MD5_DIGEST_LENGTH) != 0)
        ret = KERNEL_VALIDATION_ERROR; 	    
    else
        printf("Verify linux kernel read from flash complete.\n");

    return(ret);
}

static int  upgrade_fw(void)
{
    FILE *fd;
    int len = 0, fs_len = 0;
    unsigned char *buf = NULL, *fs_buf = NULL;
    int ret = SUCCESS;

    fd = fopen(FW_BIN,"rb");
    if(fd == NULL) {
        printf("Could not open file(%s)\n", FW_BIN);
        return(FW_FILE_OPEN_ERROR);
    }

    buf = (unsigned char *)read_partial_file(fd, 0, &len); //Read linux part
    if(buf == NULL){
        printf("Read linux part of the fw.bin failed\n");
        fclose(fd);
	return(MEM_ALLOCATION_ERROR);
    }

    if(len > 0 && len < KERNEL_SIZE) { 
        fs_buf = read_partial_file(fd, (len+16), &fs_len); //Read root part
	printf("fs_len = 0x%x\n", fs_len);
        if(fs_buf == NULL){
            printf("Read root part of the fw.bin failed\n");
            fclose(fd);
            return(MEM_ALLOCATION_ERROR);
        }
        if(fs_len > 0 && fs_len < NORMAL_FS_SIZE) {
	    inValidateFsFlash();      // This will invailidate file system flash to avoid ubsync between Kernel and FS
            ret = upgrade_linux_from_mem(buf, len);
	    if(ret == SUCCESS)
                ret = upgrade_rootfs_from_mem(fs_buf, fs_len);
	}
	else
            ret = FS_OVER_SIZE_ERROR;
    }
    else 
	ret = KERNEL_OVER_SIZE_ERROR;

    if(buf != NULL)
       free(buf);

    if(fs_buf != NULL)
       free(fs_buf);

    fclose(fd);
    return(ret);
}

static int get_kernel_header_from_buffer(char*fw_buf, int fw_sz, IMG_HEADER_T *out_header )
{
    IMG_HEADER_T *header;
    unsigned char hbuf[20];

    if((fw_buf == NULL) || (fw_sz == 0)) 
    {
        printf("iEither buffer is empty or buffer size is zeroi, Coudl not update.\n");
        return(IMAGE_FILE_ERROR);
    }

    memcpy(hbuf, fw_buf, sizeof(IMG_HEADER_T));
    header = (IMG_HEADER_T *)hbuf;
    out_header->startAddr = endian_adjust(header->startAddr);
    out_header->burnAddr = endian_adjust(header->burnAddr);
    out_header->len = endian_adjust(header->len);

    return(0);
}


static int get_rootfs_header_from_buffer(char*fw_buf, int fw_sz, int offset, IMG_HEADER_T *out_header )
{
    IMG_HEADER_T *header;
    unsigned char hbuf[20];

    if((fw_buf == NULL) || (fw_sz == 0)) 
    {
        printf("iEither buffer is empty or buffer size is zeroi, Coudl not update.\n");
        return(IMAGE_FILE_ERROR);
    }

    memcpy(hbuf, (fw_buf + offset), sizeof(IMG_HEADER_T));
    header = (IMG_HEADER_T *)hbuf;
    out_header->startAddr = endian_adjust(header->startAddr);
    out_header->burnAddr = endian_adjust(header->burnAddr);
    out_header->len = endian_adjust(header->len);

    return(0);
}

static int  upgrade_fw_buffer(char *buf, int buf_sz)
{
    IMG_HEADER_T kernel_header, fs_header;
    int ret = SUCCESS;

    if((buf == NULL) || (buf_sz == 0)) 
    {
        printf("Either buffer is empty or buffer size is zeroi, Could not update firmware.\n");
        return(IMAGE_FILE_ERROR);
    }

    ret = get_kernel_header_from_buffer(buf, buf_sz, &kernel_header);

    if(ret != SUCCESS) 
    {
        printf("read linux kernel header is failed\n"); 
        return(IMAGE_FILE_ERROR);
    }

    if((kernel_header.len < 0) || (kernel_header.len > KERNEL_SIZE)) 
    { 
        printf("linux kernel size is wrong\n"); 
	return(KERNEL_OVER_SIZE_ERROR);
    }

    ret = get_rootfs_header_from_buffer(buf, buf_sz, (kernel_header.len + 16), &fs_header);

    if((fs_header.len < 0) || (fs_header.len > NORMAL_FS_SIZE)) 
    { 
        printf("Rootfs size is wrong\n"); 
	return(FS_OVER_SIZE_ERROR);
    }

    inValidateFsFlash();      // This will invailidate file system flash to avoid unsync between Kernel and FS
    ret = upgrade_linux_from_mem(buf, kernel_header.len);

    if(ret == SUCCESS)
    {
        ret = upgrade_rootfs_from_mem((buf + kernel_header.len + 16), fs_header.len);
    }else
    {
        printf("Kernel update from buffer is failed\n");
    }


    if(ret == SUCCESS)
    {
        printf("FW image update from buffer is successful\n");
    }else 
    {
        printf("FS update from buffer is failed\n");
    }

    return(ret);
}

/*******************************************************************
*  detect_file_type() return image file type			   *
*  return values:                                                  *
*      ROOTFS_TYPE if the file is rootfs                           *
*      LINUX_TYPE  if the file is linux image			   *
*      FW_TYPE	   if the file contains linux and rootfs	   *
*******************************************************************/      

static int detect_file_type(void)
{
	FILE *fd;
	int file_len;
	unsigned char hbuf[16];
	IMG_HEADER_T *header;

	fd = fopen(FW_BIN, "rb");

	if(fd ==NULL ){
		printf("The file %s is not available\n", FW_BIN);
		return NONE;
	}
        
	fseek(fd, 0L, SEEK_SET);
	fread(hbuf, sizeof(IMG_HEADER_T), 1, fd);
	header = (IMG_HEADER_T *)hbuf;
	header->len = endian_adjust(header->len);
        
	fseek(fd, 0L, SEEK_END);
	file_len = ftell(fd);

	fclose(fd);
        
	if(file_len > (header->len+16)) {
	    printf("This is fw.bin file\n");
            return(FW_TYPE);
	} else {
	   if(!memcmp(header->signature, ROOT_SIGNATURE, SIG_LEN)){
	       printf("This is root.bin only file\n");
	       return(ROOT_TYPE);
	   } else {
	       printf("This is Linux.bin file\n");  
	       return(LINUX_TYPE);
	   }
        }		
	return NONE;
}


/*******************************************************************
*  detect_file_type_buffer() return image file type                *
*  return values:                                                  *
*      ROOTFS_TYPE if the buffer data is rootfs                    *
*      LINUX_TYPE  if the buffer data is linux image		   *
*      FW_TYPE	   if the buffer data includes linux and rootfs	   *
*******************************************************************/      

static int detect_file_type_buffer(char * buf, int buf_sz)
{
	unsigned char hbuf[16];
	IMG_HEADER_T *header;

	if((buf == NULL) || (buf_sz == 0) ){
		printf("The buffer has no valid data \n");
		return NONE;
	}
        
	memcpy(hbuf, buf, sizeof(IMG_HEADER_T));
	header = (IMG_HEADER_T *)hbuf;
	header->len = endian_adjust(header->len);
        
        
	if(buf_sz > (header->len+16)) {
	    printf("This is fw.bin file\n");
            return(FW_TYPE);
	} else {
	   if(!memcmp(header->signature, ROOT_SIGNATURE, SIG_LEN)){
	       printf("This is root.bin only file\n");
	       return(ROOT_TYPE);
	   } else {
	       printf("This is Linux.bin file\n");  
	       return(LINUX_TYPE);
	   }
        }		
	return NONE;
}

int flash_image(void)
{
    int file_type;
    int ret = SUCCESS;

    file_type = detect_file_type();
    if(file_type == FW_TYPE) {
        ret = upgrade_fw();                   //upgrade fw.bin
	if(ret == SUCCESS)
            printf("Firmware image Update is done\n");
	else 
            printf("Firmware image Update failed error = %d \n", ret);
    }
    else {
        printf("Unreconized file type \n");
	ret = IMAGE_FILE_ERROR;
    }

    return(ret);
}

/* Add this function to process the firmware data buffer instea of file 9/23/2109 */
int flash_image_buffer(char * buf, int buf_sz)
{
    int firmware_type;
    int ret = SUCCESS;

    if((buf == NULL) || (buf_sz == 0))
    {
        printf("Ether buffer is rmpty or buf size is zero\n");
	return(IMAGE_FILE_ERROR);
    }

    firmware_type = detect_file_type_buffer(buf, buf_sz);

    if(firmware_type == FW_TYPE) {
        ret = upgrade_fw_buffer(buf, buf_sz); //upgrade firmware 
	if(ret == SUCCESS)
            printf("Firmware Update is done\n");
	else 
            printf("Firmware  Update failed error = %d \n", ret);
    }
    else {
        printf("Unreconized file type \n");
	ret = IMAGE_FILE_ERROR;
    }

    return(ret);
}

void showMd5(unsigned char *md5_buf)
{
    int i;
    printf("\n MD5 string:");
    for(i=0; i< MD5_DIGEST_LENGTH*2; i++)
    {    
      printf("%c", md5_buf[i]);
    }
    printf("\n");
}

int checkFwMd5(unsigned char *orig_md5)
{
    int file_len;
    FILE *fd = NULL;
    char *buf = NULL;
    unsigned char new_md5_hex[MD5_DIGEST_LENGTH];
    unsigned char new_md5[MD5_DIGEST_LENGTH * 2 + 1];
    int i, sz;
    int ret = SUCCESS;

    if(orig_md5 == NULL)
    {
        printf("The orignal MD5 is empty\n");
	return(FW_MD5_ERROR);
    } 
    printf("File name:%s", FW_BIN);

    fd = fopen(FW_BIN, "rb");

    if(fd ==NULL ){
        printf("The file %s is not available\n", FW_BIN);
	return IMAGE_FILE_ERROR;
    }

    fseek(fd, 0L, SEEK_END);
    file_len = ftell(fd);

    rewind(fd);

    buf = (unsigned char *)malloc(file_len);

    if(buf == NULL)
    {
        printf("Out of system memory\n");
        return(MEM_ALLOCATION_ERROR);
    }
        
    sz =fread(buf, file_len, 1, fd);
    printf("MD5CHECKSUM readback data length=%d\n", sz);

    
    md5_sum(new_md5_hex, buf, file_len);

    for (i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        sprintf(&new_md5[i * 2], "%02x", new_md5_hex[i]);
    }

    printf("\n new MD5:[%s]\n", new_md5);
    printf("orignal MD5:[%s]\n", orig_md5);

    if(strcmp(new_md5, orig_md5) != 0)
    {
	printf("MD5 faile derror=%d\n", FW_MD5_ERROR);
        ret = FW_MD5_ERROR;
    } 
    else
    {
         printf("MD5 compare OK\n");
    }
    
    fclose(fd);

    free(buf);

    return(ret);
}

int checkFwMd5Buffer(unsigned char *orig_md5, char *buf, int  buffer_sz)
{
    unsigned char new_md5_hex[MD5_DIGEST_LENGTH];
    unsigned char new_md5[MD5_DIGEST_LENGTH * 2 + 1];
    int i;
    int ret = SUCCESS;

    printf("buffer size %d\n", buffer_sz);

    if( (orig_md5 == NULL) || ( buf == NULL) || (buffer_sz == 0))
    {
        printf("MD5[%s] is empty, or buffer is empty or buffer_sz:[%d] is zero\n", orig_md5, buf, buffer_sz);
	return(FW_MD5_ERROR);
    } 
    
    md5_sum(new_md5_hex, buf, buffer_sz);

    for (i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        sprintf(&new_md5[i * 2], "%02x", new_md5_hex[i]);
    }

    printf("\n new MD5:[%s]\n", new_md5);
    printf(" orignal MD5:[%s]\n", orig_md5);

    if(strcmp(new_md5, orig_md5) != 0)
    {
	printf("MD5 fail derror=%d\n", FW_MD5_ERROR);
        ret = FW_MD5_ERROR;
    } 
    else
    {
         printf("MD5 compare OK\n");
    }
    
    return(ret);
}
#ifndef __BUILD_LIB__
int main(void)
{
	char md5_sum[(MD5_DIGEST_LENGTH * 2) +1] = "edee874402a3a0525c824f3b2460eade";
	int ret  = -1;
	char *buf= NULL;
	int buf_sz;
        FILE *fp;
        int leng = 0;

        fp = fopen("/tmp/fw.bin", "rb");
        if(fp == NULL)
        {
            printf("The /tmp/fw.bin is not availablei\n");
            return;
        }

        fseek(fp, 0, SEEK_END);
        leng = ftell(fp);
        rewind(fp);

        buf =(char *) malloc(leng + 1);

        if(buf == NULL)
        {
             printf("Allocate buffer(%d) failed\n", leng);
	     return(-1);
        }else
        {
             printf("Allocate buffer(%d) success\n", leng);
        }

        buf_sz = fread(buf, leng, 1, fp);

        printf("Read back data length=%d\n", buf_sz);

        fclose(fp);

	//return flash_image();
#if 0 
	if((ret = checkFwMd5(md5_sum)) == SUCCESS)
	{
	    printf("The MD5 check is OK %d\n", ret);
	} else
	{
	    printf("The MD5 check is fail:%d \n", ret);
	}
#endif


       ret = checkFwMd5Buffer(md5_sum, buf, leng);

       if(ret == SUCCESS)
       {
           printf("MD5 sum check are succesful\n");
           ret = flash_image_buffer(buf, leng);
	   printf("Update image result=%d \n");
       }
       else
       {
           printf("MD5 sum check are Failed\n");
       }

       free(buf);

       return(ret);

}
#endif

