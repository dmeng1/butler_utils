#include <openssl/sha.h>
#include <stdio.h>

#define BUF_SZ 256

int main(int argc, char **argv)
{
    unsigned char buf[BUF_SZ];
    FILE *fp = NULL;
    SHA256_CTX ctx;  
    size_t len;

    if (argc < 2) {
        fprintf(stderr, "usage: %s <file>\n", argv[0]);
	return 1;
    }

    fp = fopen(argv[1], "r");
    if (!fp) {
	printf("%s is not valide file\n", argv[1]);
        return 1;
    }

    SHA256_Init(&ctx);
    do {
         len = fread(buf, 1, BUF_SZ, fp);
	 SHA256_Update(&ctx, buf, len);
    } while (len == BUF_SZ);

    SHA256_Final(buf, &ctx);

    fclose(fp);

    for (len = 0; len < SHA256_DIGEST_LENGTH; ++len)
    printf("%02x", buf[len]);
    printf("\n \t total length of sha256 %d\n\n", SHA256_DIGEST_LENGTH);
    return 0;
}
