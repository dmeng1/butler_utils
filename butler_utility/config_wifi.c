#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "apmib.h"

static int uei_set_flash_reboot(char * ssid, char *password)
{
    char cmd[128];     

    printf("WIFI SSID=%s Password=%s\n", ssid, password);

    if(ssid == NULL || password == NULL ) {
        printf("There is not ssid or password");
	return(0); 
    }

    /*Follwoing code set CPE mode*/
    system("flash set WLAN0_MODE 1");
    system("flash set WLAN0_ENCRYPT 6");
    system("flash set WLAN0_WPA_CIPHER_SUITE 3");
    system("flash set WLAN0_WPA2_CIPHER_SUITE 3");
    sprintf(cmd, "flash set %s %s", "WLAN0_SSID", ssid); 
    system(cmd);
    sprintf(cmd,"flash set %s %s", "WLAN0_WPA_PSK", password);
    system(cmd);
    system("flash set DHCP 1");
    system("reboot");
    return(0);
}

int main(int argc, char** argv) {
    char ssid[256], password[256];

    if(argc<2){
     printf("usage: config_wifi ssid password\n");
     return(EXIT_SUCCESS);
     } 

    strcpy(ssid, (char *) argv[1]);
    strcpy(password, argv[2]);
    uei_set_flash_reboot(ssid, password); 
    return (EXIT_SUCCESS);
}	

