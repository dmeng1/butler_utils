#ifndef _BASE64_H_
#define _BASE64_H_

#include <stdio.h>

#ifndef BASE64_API
#define BASE64_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

BASE64_API int base64_encode(char * out_str, size_t out_str_len, const void * in_buffer, size_t in_len);
BASE64_API int base64_decode(void * out_buffer, size_t out_buffer_len, const char * in_str);

#ifdef __cplusplus
}
#endif

#endif
