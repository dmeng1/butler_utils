#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <mtd/mtd-user.h>
#include <linux/fs.h>
#include <errno.h>
#include "apmib.h"

int uei_write_verify_flash(char * test_str)
{
    char read_str[128];
    int n;

    memset(read_str, 0, 128);
    apmib_init();
    apmib_set(MIB_UEI_FW_URL, (void *)test_str);
    apmib_update(CURRENT_SETTING);
    sleep(1);
    apmib_get(MIB_UEI_FW_URL, (void *)read_str);
    n = strcmp(test_str, read_str);
    if(n != 0){
        printf("The write string is not verified, error= %d\n" , n);
        return(-1);
    }   
    return(0);
}

int main(void) {
    char str[129] = "01234567890qwertyuiopasdfghjklzxcvbnm<>!@#$%^&*()_+=-qwkldjkalkdjfjjjksad;f;;jskdjfjoiurqweo127849834709988283792837471878839399";
    int i, ret, count = 0;
    char tmp;

     while(1) {  
         tmp = str[0];
         for(i = 0; i< 127; i++) 
             str[i] = str[i+1];
         str[127] = tmp;
         ret = uei_write_verify_flash(str);
         if (ret == -1) {
             printf("Write flash could not verified at COUNT = %d\n", count);
	     return(-1);
	 }
         count++;
         printf("This is the %d round test\n", count);	 
     } 

    return (0);
}	
