/* 
 * Copyright 2017 Universal Electronics Inc. All Rights
 * Reserved.
 * 
 * NOTICE:  You are permitted to use this file only in
 * accordance with the terms of the Universal Electronic Inc.
 * license agreement under which it was obtained.
 * Redistribution, publication, or distribution in any form is
 * not permitted without a separate license.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER(S) "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE COPYRIGHT OWNER(S) BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
 * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 */
/** 
 * @mainpage Introduction
 * <p>The UEI online encryption tools provides functions to facilitate 
 * certain online requirements for accessing Quickset Cloud</p>
 */
/**
 * @file        uei_online_enc.h
 * @brief       Provides the interface functions for the QuickSet online encrytion untilities.
 * 
 */

#ifndef _UEI_ONLINE_ENC_H_
#define _UEI_ONLINE_ENC_H_

#ifdef __cplusplus
extern "C" {
#endif

#define ENC_VECTOR_LEN      32
#define kI_data_max         1000


/**
 * @defgroup online_utilties Quickset Cloud Untilies 
 * @brief This module identifies the functions that are used to obtain version information from QuickSet. 
 * @{
 */
/**
 * @brief        <a name="ueic_encrypt_with_vector"></a> Encrypt 
 *              a user id with provided encryption vector, and
 *              output a result string
 * @param       user_id a null terminated ASCII string with 
 *                      valid length from 1 to 124.
 * @param       enc_vector a null terminated ASCII string max 
 *                         lenght of 16 bytes
 * @param       outbuf the encypted result text sting. It is b64
 *                     encoded and NULL terminated.
 * @param       out_dz the size of the outbuf, must be greater 
 *                     than 343.
 * @since       1.0
 * @return      the length of the b64 codeded encypted user id 
 *              string
 *              -1 : invalid user_id
 *              -2 : invalid enc_vector
 *              -3 : outbuf is null and/or out_sz < 342.
 *              -5 : out of memory
 *              -6 : encryption error
 */
int ueic_encrypt_with_vector(const char * user_id, int len, const char *enc_vector, char * outbuf, const int out_sz);
int ueic_encrypt_with_vector_large( int is_enc, const char * in_buf, int in_len, const char *enc_vecter, char * outbuf, const int out_sz );


/** @} */ /* end group */
#ifdef __cplusplus
}
#endif

#endif
