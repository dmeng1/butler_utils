/* This utility tool is to extract the parameter section data from 32MB data file.  The 32MB data file is using old partition*/
/* The resul:  fw_parameter.bin is the parameter sction data file
 *             fw_jffs2.bin is the jffs2 sction data file */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <mtd/mtd-user.h>
#include <unistd.h>

//#include "uei_image.h"

#define MB 0x100000
#define PARAMETER_FILE "fw_parameter.bin"
#define JFFS2_FILE "fw_jffs2.bin"
#define FILE_32MB "block_file.bin"

int read_file_2_buffer(unsigned char *buffer)
{
	FILE *fp;
	int len;
	char FILE_NAME[1024];
	
	printf("Please input block file\n");
	scanf("%s", FILE_NAME);
	printf("Input file name %s\n", FILE_NAME);
	fp=fopen(FILE_NAME, "rb");
	if(fp==NULL){
		printf("Input file does not exist and use default block file block_file.bin\n");
		fp = fopen(FILE_32MB,"rb");
		if(fp == NULL){
			printf("There is no default files in the folder, exit\n");
			return(1);
		}
	}
        len = fread(buffer, 1, 32*MB, fp);
	fclose(fp);
	if (len < 32*MB){
		printf("The data file len (0x%x) is less than 32 MB and exit\n", len);
	        return(1);
	}
	return(0);
}

void main() //This will generate two files fw_parameter.bin and fw_jffs2.bin 
{
    unsigned char *buffer = NULL, *file_buffer=NULL;
    FILE *fd;
    int len;

    file_buffer = (unsigned char *) malloc(32*MB + 1);
    if(file_buffer == NULL){
	   printf("No enough memory and exit\n");
	   exit(1);
    }

    if(read_file_2_buffer(file_buffer) !=0){
	    printf("read file failed, free data buffer\n");
	    free(file_buffer);
	    exit(1);
    }

    fd = fopen(PARAMETER_FILE, "wb");
    if(fd ==NULL ){
        printf("The file %s is not available\n", PARAMETER_FILE);
	free(file_buffer);
	exit(1);
    }

    printf("point to parameter section\n");
    len = 0x30000; //Flash parameter section length
    buffer = (unsigned char *) (file_buffer + 0x50000); /* 0x50000 is the start of parameter secsion*/
    fwrite(buffer, len, 1, fd);
    fclose(fd);


    printf("Read jffs2 section\n");

    fd = fopen(JFFS2_FILE, "wb");
    if(fd == NULL ){
        printf("The file %s is not available\n", JFFS2_FILE);
	free(file_buffer);
	exit(1);
    }
    len = 9 * MB;
    buffer = (unsigned char *)(file_buffer + 0x01280000);
    fwrite(buffer, 1, len, fd);
    fclose(fd);
    free(file_buffer);
    exit(0);
}
