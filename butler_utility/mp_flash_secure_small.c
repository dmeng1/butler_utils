#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <linux/fs.h>
#include <mtd/mtd-user.h>
#include <unistd.h>


#include "uei_image.h"




int fwChecksumOk(char *data, int len)
{
    unsigned short sum=0;
    int i;

    for (i=0; i<len; i+=2) {
        sum += WORD_SWAP( *((unsigned short *)&data[i]) );
									        }
    return(sum);
}


int endian_adjust(int input)
{
	int out1, out2;

	out1 =(input >> 16);
	out1 = (out1 >> 8)|(out1 & 0xff) << 8;

	out2 = (input & 0xffff);
        out2 = (out2 >> 8) | (out2 & 0xff) << 8;
	return (out2 << 16| out1);
}

void read_flash(char * dev_name, int flash_offset, unsigned char *buffer, int length)
{
	int fm, i, j, flash_len;

        fm = open(dev_name, O_RDWR);
	if(fm <= 0){
	   printf("open %s fail\n", linux_dev);
	   return; 
	}
	
	lseek(fm,flash_offset, SEEK_SET);

	if(buffer == NULL){
	    printf("No memory buffer\n");
	    close(fm);
	    return; 
	}

        flash_len = read(fm, buffer, length);
        if(flash_len != length){
	    printf("There are less than 0x%x bytes readable from flash\n", length);
	}
#ifdef __DEBUG__
	j = 0;
	for(i = 0; i <  256; i++){	
	      printf("%2x ", buffer[i]);
	      if(j == 15){
		      printf("\n");
		      j=0;
	      }
	      else 
		      j++;
	}
        printf("\n\n");
#endif 
        free(buffer);
        close(fm);
}

int write_flash(char *dev_name, int flash_offset, unsigned char *buffer, int buffer_offset, int length)
{
	int fm, flash_len;
        mtd_info_t mtd_info;
        erase_info_t ei;
	unsigned char *tmp;

        fm = open(dev_name, O_RDWR);
	if(fm <= 0){
	   printf("open %s fail\n", dev_name);
	   return(-1); 
	}

	lseek(fm, flash_offset, SEEK_SET);

	if(buffer == NULL){
	    printf("No memory buffer\n");
	    close(fm);
	    return; 
	}

	tmp = (unsigned char *) (buffer + buffer_offset);

        flash_len = write(fm, tmp, length);
        if(flash_len != length){
	    printf("There are less than 0x%x bytes writeable to flash wirte length:0x%x\n", length, flash_len);
	}
        sync();
        if(ioctl(fm, BLKFLSBUF, NULL) < 0){
            printf("flush mtd system cache error\n");
        }
        close(fm);
	return(flash_len);
}


unsigned char *read_file(char * file_name,  int in_offset, int *out_len)
{
	FILE *fd;
	int file_len, i, j;
	unsigned char hbuf[16];
	IMG_HEADER_T *header;
	unsigned char *out_buf = NULL;

	fd = fopen(file_name, "rb");

	if(fd ==NULL ){
		printf("The file %s is not available\n", file_name);
		return NULL;
	}
        
	fseek(fd, 0L, SEEK_SET);
	fread(hbuf, sizeof(IMG_HEADER_T), 1, fd);
	header = (IMG_HEADER_T *)hbuf;
        
	header->startAddr = endian_adjust(header->startAddr);
	header->burnAddr = endian_adjust(header->burnAddr);
	header->len = endian_adjust(header->len);
        
	out_buf = (unsigned char *)malloc(header->len + 16);

	if(out_buf == NULL)
	{
		printf("Out of system memory\n");
		return NULL;
	}

#ifdef __DEBUG__
	printf("The file %s signature =%4s, start_addr =0x%x, burn_address=0x%x length is 0x%x\n", file_name, header->signature, header->startAddr, header->burnAddr, header->len);
#endif

	fseek(fd, in_offset, SEEK_SET);
	fread(out_buf, header->len + 16, 1, fd);

#ifdef __DEBUG__
	printf("The first 256 bytes of the file: %s\n", file_name);
	j = 0;
	for(i=0; i< 256; i++) {
	      printf(" 0x%2x ", out_buf[i]);
	      if(j == 15){
		      printf("\n");
		      j=0;
	      }
	      else 
		      j++;
        }
	printf("\n\n");
	
	printf("The last 256 bytes of the file %s\n", file_name);
	j = 0;
	for(i=(header->len + 16 - 256) ; i< (header->len + 16); i++){
		printf(" 0x%2x ", out_buf[i]);
	      if(j == 15){
		      printf("\n");
		      j=0;
	      }
	      else 
		      j++;
	}
	printf("\n\n");
#endif
	fclose(fd);

	*out_len = header->len;
        
	return(out_buf);
}


unsigned char *read_partial_file(FILE *fd,  int file_offset, int *out_len)
{
	IMG_HEADER_T *header;
	unsigned char *out_buf = NULL;
	unsigned char hbuf[20];
	int i, j;

	if(fd == NULL ){
		printf("The file ID(%d) is invalide\n", fd);
		return NULL;
	}
        
	fseek(fd, file_offset, SEEK_SET);
	fread(hbuf, sizeof(IMG_HEADER_T), 1, fd);
	header = (IMG_HEADER_T *)hbuf;
        
	header->startAddr = endian_adjust(header->startAddr);
	header->burnAddr = endian_adjust(header->burnAddr);
	header->len = endian_adjust(header->len);
        
	out_buf = (unsigned char *)malloc(header->len + 16);

	if(out_buf == NULL)
	{
		printf("Out of system memory\n");
		return NULL;
	}

#ifdef __DEBUG__
	printf("The signature =%4s, start_addr =0x%x, burn_address=0x%x length is 0x%x\n", header->signature, header->startAddr, header->burnAddr, header->len);
#endif

	fseek(fd, file_offset, SEEK_SET);
	fread(out_buf, header->len + 16, 1, fd);

#ifdef __DEBUG__
	printf("The first 256 bytes of the file\n");
	j = 0;
	for(i=0; i< 256; i++) {
	      printf(" 0x%2x ", out_buf[i]);
	      if(j == 15){
		      printf("\n");
		      j=0;
	      }
	      else 
		      j++;
        }
	printf("\n\n");
	
	printf("The last 256 bytes of the file\n");
	j = 0;
	for(i=(header->len + 16 - 256) ; i< (header->len + 16); i++){
		printf(" 0x%2x ", out_buf[i]);
	      if(j == 15){
		      printf("\n");
		      j=0;
	      }
	      else 
		      j++;
	}
	printf("\n\n");
#endif

	*out_len = header->len;
        
	return(out_buf);
}

int upgrade_rootfs_from_mem(unsigned char * buffer, int len)
{
    if(buffer == NULL ){
        printf(" rootfs buffer is NULL\n");
        return(-1);
    }

    printf("rootfs checksum = 0x%X\n", fwChecksumOk(&buffer[ROOTFS_BUFFER_OFFSET], len));
    printf("Update roofs: Write_device %s rootfs_offset=0x%x buffer lengh=0x%x \n", linux_dev, ROOTFS_BUFFER_OFFSET , len);
    write_flash(linux_dev, (0x200000 + MP_LINUX_BACKUP_OFFSET), buffer, ROOTFS_BUFFER_OFFSET, len);
    return(0);
}

int upgrade_linux_from_mem(unsigned char * buffer, int len)
{
    if(buffer == NULL ){
        printf(" rootfs buffer is NULL\n");
        return(-1);
    }
    printf("Update Linux: Write_device %s flash_offset=0x%x buffer lengh=0x%x \n", linux_dev, MP_LINUX_BACKUP_OFFSET, (len+16));
    write_flash(linux_dev, MP_LINUX_BACKUP_OFFSET, buffer, 0, (len + 16));
    return(0);
}


void burn_small_fw(void)
{
    FILE *fd;
    int len;
    unsigned char *buf;


    fd = fopen(FW_SECURE_SMALL,"rb");
    if(fd == NULL) {
	    printf("Could not open file(%s)\n", FW_BIN);
	    return;
    }

    buf = (unsigned char *)read_partial_file(fd, 0, &len); //Read linux part
    if(buf == NULL){
        printf("Read linux part of the fw.bin failed\n");
	return;
    }

    upgrade_linux_from_mem(buf, len);

    free(buf);

    buf = read_partial_file(fd, (len+16), &len); //Read root part
    
    if(buf == NULL){
        printf("Read root part of the fw.bin failed\n");
	return;
    }

    upgrade_rootfs_from_mem(buf, len);
   
    free(buf);

    fclose(fd);
}

/*******************************************************************
*  detect_file_type() return image file type			   *
*  return values:                                                  *
*      ROOTFS_TYPE if the file is rootfs                           *
*      LINUX_TYPE  if the file is linux image			   *
*      FW_TYPE	   if the file contains linux and rootfs	   *
*******************************************************************/      

int detect_file_type(void)
{
	FILE *fd;
	int file_len;
	unsigned char hbuf[16];
	IMG_HEADER_T *header;

	fd = fopen(FW_SECURE_SMALL, "rb");

	if(fd ==NULL ){
		printf("The file %s is not available\n", FW_BIN);
		return NONE;
	}
        
	fseek(fd, 0L, SEEK_SET);
	fread(hbuf, sizeof(IMG_HEADER_T), 1, fd);
	header = (IMG_HEADER_T *)hbuf;
	header->len = endian_adjust(header->len);
        
	fseek(fd, 0L, SEEK_END);
	file_len = ftell(fd);

#ifdef DEBUG
        printf("The file length is 0x%x\n, data_length =0x%x", file_len, header->len);
#endif        
	fclose(fd);
        
	if(file_len > (header->len+16)) {
	    printf("This is fw.bin file\n");
            return(FW_TYPE);
	} else {
            printf("This is not FW type file, could not update small image\n"); 	
        }		
	return NONE;
}

int main(void)
{
    int file_type;

    file_type = detect_file_type();
    if(file_type == FW_TYPE) {
	printf("Start update secure small image\n");
        burn_small_fw();             //upgrade fw_secure_small.bin
    }
    else{
        printf("Unsupported file type, could not flash secure small image \n");
	return(-1);
    }
    printf("Firmware Image Update is done\n");
    return(0);
}

