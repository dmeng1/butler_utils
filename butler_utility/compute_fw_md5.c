#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <linux/fs.h>
#include <string.h>

#include "openssl/rsa.h"
#include "openssl/pem.h"
#include "openssl/err.h"
#include "openssl/sha.h"
#include <openssl/md5.h>

#include  "apmib.h"

#define KB	1024
#define MB	(KB * KB)
#define NEVO_BUTLER_FW_DL_TIMEOUT        600

#define LINUX_MAX_LENGTH 	(2 * MB)
#define ROOTFS_MAX_LENGTH 	(2.5 * MB)
#define MD5_LENGTH		65	
#define LINUX_OFFSET_OF_LEN	3
#define OFFSET_OF_LEN		2
#define SIZE_OF_SQFS_SUPER_BLOCK 640
#define SIZE_OF_CHECKSUM	2

static int endian_adjust(int input);
static void md5_sum(unsigned char *md5, unsigned char *buffer, int len);
static int get_linux_kernel_length(unsigned char *linux_buffer);
int get_linux_kernel_md5 (unsigned char *linux_buf);
static int get_rootfs_length(unsigned char *rootfs_buffer);
int get_rootfs_md5 (unsigned char *rootfs_buf);
static int computeMd5(unsigned char *buf, int buffer_sz, char *title);
static int get_kernel_header_from_buffer(char*fw_buf, int fw_sz, IMG_HEADER_T *out_header);
static int get_rootfs_header_from_buffer(char*fw_buf, int fw_sz, int offset, IMG_HEADER_T *out_header);

static int endian_adjust(int input)
{
        int out1, out2;

        out1 =(input >> 16);
        out1 = (out1 >> 8)|(out1 & 0xff) << 8;

        out2 = (input & 0xffff);
        out2 = (out2 >> 8) | (out2 & 0xff) << 8;
        return (out2 << 16| out1);
}

static void md5_sum(unsigned char *md5, unsigned char *buffer, int len) {
    MD5_CTX ctx;

    MD5_Init(&ctx);
    MD5_Update(&ctx, buffer, len);
    MD5_Final(md5, &ctx);
    return;
}

static int get_linux_kernel_length(unsigned char *linux_buffer)
{
    int len;

    len = 256*(256*(256 * linux_buffer[12] + linux_buffer[13]) + linux_buffer[14]) + linux_buffer[15];
    return(len);

}

int get_linux_kernel_md5 (unsigned char *linux_buf)
{
    int buf_len;
    unsigned char md5_hex[MD5_DIGEST_LENGTH];
    unsigned char print_md5[MD5_DIGEST_LENGTH * 2 + 1];
    int i;

    /*
    for (i = 0; i < 32; i++)
    {
        printf("%02x", linux_buf[i]);
    }*/

    if(linux_buf == NULL){
        printf("The linuyx buf NULL\n");
	return(1);
    }

    buf_len = get_linux_kernel_length(linux_buf);
    printf("Get linux buffer length:[0x%x]\n", buf_len);

    md5_sum(md5_hex, linux_buf, buf_len);

    for (i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        sprintf(&print_md5[i * 2], "%02x", md5_hex[i]);
    }

    printf("\nLinux kernel MD5:[%s]\n", print_md5);

    return(0);
}

static int get_rootfs_length(unsigned char *rootfs_buffer)
{
    int len;
    
    len = 256*(256*(256 * rootfs_buffer[8] + rootfs_buffer[9]) + rootfs_buffer[10]) + rootfs_buffer[11] + SIZE_OF_SQFS_SUPER_BLOCK + SIZE_OF_CHECKSUM;
   
    return(len);

}

int get_rootfs_md5 (unsigned char *rootfs_buf)
{
    int buf_len;
    unsigned char md5_hex[MD5_DIGEST_LENGTH];
    unsigned char print_md5[MD5_DIGEST_LENGTH * 2 + 1];
    int i;

    if(rootfs_buf == NULL){
        printf("The md5 buffer or rootfs buf NULL\n");
	return(1);
    }

#if 0 
    for (i = 0; i < 32; i++)
    {
        printf("%02x", rootfs_buf[i]);
    }
#endif


    buf_len = get_rootfs_length(rootfs_buf);

#if 0
    printf("Get rootfs buffer length:[%d]\n", buf_len);

    for (i = 0; i < 32; i++)
    {
        printf("%02x", rootfs_buf[buf_len-32 + i]);
    }
#endif

    md5_sum(md5_hex, rootfs_buf, buf_len);

    for (i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        sprintf(&print_md5[i * 2], "%02x", md5_hex[i]);
    }

    printf("\nRootfs MD5:[%s]\n", print_md5);


    return(0);
}


static int compute_fw_md5_buffer(unsigned char *buf, int buffer_sz)
{
    unsigned char new_md5_hex[MD5_DIGEST_LENGTH];
    unsigned char new_md5[MD5_DIGEST_LENGTH * 2 + 1];
    int i;
    int ret = 0;

    //printf("buffer size %d\n", buffer_sz);

    if(( buf == NULL) || (buffer_sz == 0))
    {
        printf("buffer is empty or buffer_sz:[%d] is zero\n", buffer_sz);
        return(-1);
    }

    md5_sum(new_md5_hex, buf, buffer_sz);

    for (i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        sprintf(&new_md5[i * 2], "%02x", new_md5_hex[i]);
    }

    printf("\nnew MD5:[%s]\n", new_md5);

    return(ret);
}

static int computeMd5(unsigned char *buf, int buffer_sz, char * title)
{
    unsigned char new_md5_hex[MD5_DIGEST_LENGTH];
    unsigned char new_md5[MD5_DIGEST_LENGTH * 2 + 1];
    int i;
    int ret = 0;

    //printf("buffer size %d\n", buffer_sz);

    if(( buf == NULL) || (buffer_sz == 0))
    {
        printf("buffer is empty or buffer_sz:[%d] is zero\n", buffer_sz);
        return(-1);
    }

    md5_sum(new_md5_hex, buf, buffer_sz);

    for (i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        sprintf(&new_md5[i * 2], "%02x", new_md5_hex[i]);
    }

    printf("\n%s  MD5:[%s]\n", title, new_md5);

    return(ret);
}

static int get_kernel_header_from_buffer(char*fw_buf, int fw_sz, IMG_HEADER_T *out_header )
{
    IMG_HEADER_T *header;
    unsigned char hbuf[20];

    if((fw_buf == NULL) || (fw_sz == 0))
    {
        printf("Either buffer is empty or buffer size is zeroi, Coudl not update.\n");
        return(-1);
    }

    memcpy(hbuf, fw_buf, sizeof(IMG_HEADER_T));
    header = (IMG_HEADER_T *)hbuf;
    out_header->startAddr = endian_adjust(header->startAddr);
    out_header->burnAddr = endian_adjust(header->burnAddr);
    out_header->len = endian_adjust(header->len);

    return(0);
}

static int get_rootfs_header_from_buffer(char*fw_buf, int fw_sz, int offset, IMG_HEADER_T *out_header )
{
    IMG_HEADER_T *header;
    unsigned char hbuf[20];

    if((fw_buf == NULL) || (fw_sz == 0))
    {
        printf("Either buffer is empty or buffer size is zero, Coudl not update.\n");
        return(-1);
    }

    memcpy(hbuf, (fw_buf + offset), sizeof(IMG_HEADER_T));
    header = (IMG_HEADER_T *)hbuf;
    out_header->startAddr = endian_adjust(header->startAddr);
    out_header->burnAddr = endian_adjust(header->burnAddr);
    out_header->len = endian_adjust(header->len);

    return(0);
}

int main(int argc, char **argv )
{
    FILE *fp; 
    unsigned int file_len, buf_sz;
    char *buf;    

    if(argc < 2 ){
        printf("Usage %s firmware_file_name\n", argv[0]);
	return(-1);
    } else {
	printf("The firmware file name is %s\n", argv[1]);
    }

    fp = fopen(argv[1], "rb");
    fseek(fp, 0, SEEK_END);
    file_len = ftell(fp);
    rewind(fp);

    buf =(char *) malloc(file_len + 1);

    if(buf == NULL)
    {
         printf("Allocate buffer(%d) failed\n", file_len);
         return(-1);
    }else
    {
         //printf("Allocate buffer(%d) success\n", file_len);
    }

    buf_sz = fread(buf, file_len, 1, fp);
    fclose(fp);

    computeMd5(buf, file_len, argv[1]);

    get_linux_kernel_md5(buf);

    buf_sz = get_linux_kernel_length(buf);
    
    //printf("Linux size:[0x%x]\n", buf_sz);

    get_rootfs_md5((unsigned char *)(buf+ buf_sz + 32));

    free(buf);

    return(0);
}


