#ifndef __UEI_IMAGE_ERROR_H__
#define __UEI_IMAGE_ERROR_H__
enum error_num {
	SUCCESS,
	KERNELCHECKSUM_ERROR, 
	FS_CHECKSUM_ERROR, 
	KERNEL_OVER_SIZE_ERROR, 
       	FS_OVER_SIZE_ERROR, 
	KERNEL_VALIDATION_ERROR,
	FS_VALIDATION_ERROR,
	IMAGE_FILE_ERROR,
	MEM_ALLOCATION_ERROR,
	FW_FILE_OPEN_ERROR,
	FLASH_OPEN_ERROR,
	FLASH_READ_ERROR,
	FLASH_READ_LENGTH_ERROR,
	FLASH_WRITE_ERROR,
	FLASH_WRITE_LNEGTH_ERROR,
	UNABLE_UPDATE_ERROR,
        URL_UPDATE_ERROR,
	VERSION_NOT_MATCH_ERROR,
	URI_NOT_MATCH_ERROR,
	DOWNLOAD_ERROR,
	IOTHUB_HANDLE_ERROR,
	FW_MD5_ERROR,
	FLASH_REWRITE_ERROR,
	UNKNOW_ERROR
}; 

char error_string[24][40] ={
	"SUCCESS",
	"KERNELCHECKSUM_ERROR", 
	"FS_CHECKSUM_ERROR", 
	"KERNEL_OVER_SIZE_ERROR", 
       	"FS_OVER_SIZE_ERROR", 
	"KERNEL_VALIDATION_ERROR",
	"FS_VALIDATION_ERROR",
	"IMAGE_FILE_TYPE_ERROR",
	"MEM_ALLOCATION_ERROR",
	"FW_FILE_OPEN_ERROR",
	"FLASH_OPEN_ERROR",
	"FLASH_READ_ERROR",
	"FLASH_READ_LENGTH_ERROR",
	"FLASH_WRITE_ERROR",
	"FLASH_WRITE_LNEGTH_ERROR",
	"UNABLE_UPDATE_ERROR",
        "URL_UPDATE_ERROR",
	"VERSION_NOT_MATCH_ERROR",
	"URI_NOT_MATCH_ERROR",
	"DOWNLOAD_ERROR",
	"IOTHUB_HANDLE_ERROR",
	"FW_MD5_ERROR",
	"FLASH_REWRITE_ERROR",
	"NKNOW_ERROR"
};
#endif
