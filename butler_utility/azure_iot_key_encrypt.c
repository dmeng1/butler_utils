#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "uei_online_enc.h"

#define ENC_VECTOR_LEN      32
#define MAX_USER_ID         2048
#define ENC_USER_ID_LEN     MAX_USER_ID         /* Actual maximum is 256 * 4 / 3 + 1 */

int main(int argc, char ** argv) {
    const char * enc_vector;
    char user_id[MAX_USER_ID + 1];
    int skip_line = 0;
    int len;

    if (argc != 2) {
        fprintf(stderr, "Usage: %s VECTOR\n", argv[0]);
        return 1;
    }
    enc_vector = argv[1];
    if (strlen(enc_vector) != ENC_VECTOR_LEN) {
        fprintf(stderr, "Encryption vector have to be %d bytes\n", ENC_VECTOR_LEN);
        return 1;
    }
    fprintf(stderr, "Please enter user ID to encrypt, or blank line to quit.\n");
    fprintf(stderr, "Maximum length of user ID is %d bytes\n", MAX_USER_ID);

    memset( user_id, 0, sizeof(user_id));
    len = sizeof(user_id);

    while (fgets(user_id, sizeof(user_id), stdin)) {
        char * end_line = strchr(user_id, '\n');
        char enc_user_id[ENC_USER_ID_LEN];
        char enc_user_id2[ENC_USER_ID_LEN];
        int enc_user_id_len;
        int i;
        
        memset( enc_user_id, 0, sizeof(enc_user_id));
        memset( enc_user_id2, 0, sizeof(enc_user_id2));
        /* Truncate line longer than MAX_USER_ID characters */
        if (skip_line) {
            skip_line = (end_line == NULL);
            continue;
        }
        if (end_line)
            *end_line = '\0';
        if (strlen(user_id) == 0)
            break;
        enc_user_id_len = ueic_encrypt_with_vector_large(1, user_id, MAX_USER_ID, enc_vector, enc_user_id, sizeof(enc_user_id));
        fprintf(stderr, "\nueic_encrypt_with_vector: %d\n", enc_user_id_len);
        for ( i = 0; i < enc_user_id_len; i++ )
        {
            fprintf(stderr,"%02x ", 0x00ff & enc_user_id[i]);
        }
        sleep(1);
        enc_user_id_len = ueic_encrypt_with_vector_large(0, enc_user_id, enc_user_id_len, enc_vector, enc_user_id2, sizeof(enc_user_id2));
        fprintf(stderr, "\nueic_encrypt_with_vector2: %d\n", enc_user_id_len);
        for ( i = 0; i < enc_user_id_len; i++ )
        {
            fprintf(stderr,"%02x ", 0x00ff & enc_user_id2[i]);
        }
        
	enc_user_id2[len+1]='\0';
        printf("\n\n\t oriignal userId=%s\n", enc_user_id2); 
    }

    return 0;
}
