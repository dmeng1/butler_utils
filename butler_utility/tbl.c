#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "apmib.h"
#include "mibtbl.h"

int main(int argc, char** argv) 
{   mib_table_entry_T *mib;
    int i=0;

    if (!apmib_init()) {
        printf("\nInitFail...\n");
        return -1;
    }
    mib = mib_get_table(CURRENT_SETTING);
    mib = mib->next_mib_table;
    while (mib[i].id) {
	   printf("id=%d, name=%s, type=%d, size=%d\n", mib[i].id, mib[i].name, mib[i].type, mib[i].size);
       i++;
    }
    printf("\nTotal MIBs=%d\n", i);
	
    return 0;
}
