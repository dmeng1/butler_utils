#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <errno.h>

#ifndef CLIENT
#include "apmib.h"
#endif


#define DEFAULT_AP_IP_ADDRESS 	0xFE01A8C0//192.168.1.254 => 154.1.168.192
#define CONFIG_PORT				65525

#ifndef CLIENT
int is_connected (void)
{
    int timeout = 0;
    while(!system("ping -c1 www.google.com &>/dev/null")){
	timeout++;
	printf(".");
        if(timeout == 10)
	    return 0;	
    }
    return 1;
}

int set_test_wifi(char*ssid, char *passwd){
    char command_ssid[50] = "iwpriv wlan0-vxd set_mib ssid=";
    char command_passwd[50] = "iwpriv wlan0-vxd set_mib passphrase=";
    char command[128];

    system("ifconfig wlan0 down");
    system("ifconfig br0 down");
    system("ifconfig wlan0-vxd down");

    strcpy(command, command_ssid);
    strcat(command, ssid);
    printf("New command string for SSID = %s\n", command);
    system(command);

    system("iwpriv wlan0-vxd set_mib opmode=8");
    system("iwpriv wlan0-vxd set_mib authtype=2");
    system("iwpriv wlan0-vxd set_mib encmode=2");
    system("iwpriv wlan0-vxd set_mib psk_enable=2");
    system("iwpriv wlan0-vxd set_mib wpa_cipher=0");
    system("iwpriv wlan0-vxd set_mib wpa2_cipher=8");
    system("iwpriv wlan0-vxd set_mib passphrase=11223344");

    strcpy(command, command_passwd);
    strcat(command, passwd);
    printf("New command string = %s\n", command);
    system(command);

    system("ifconfig wlan0 up");
    system("ifconfig wlan0-vxd up");
    system("brctl addif br0 wlan0-vxd");
    system("ifconfig br0 up");
    system("udhcpc -i br0 -p /etc/udhcpc/wlan0.deconfig -s /usr/share/udhcpc/wlan0-vxd.sh &");
    system("sleep 20");
    
    return(1);

}

void set_flash(char *ssid, char * passwd){

    int enable=1;
    char command_wlan_disable[56] = "flash set WLAN0_VAP4_WLAN_DISABLED 0";
    char command_wlan_vap4_ssid[36] = "flash set WLAN0_VAP4_SSID ";
    char command_wlan_encrypt[33] = "flash set WLAN0_VAP4_ENCRYPT 6";
    char command_wpa_psk[30] = "flash set WLAN0_VAP4_WPA_PSK ";
    char command_wsc_configured[39] = "flash set WLAN0_VAP4_WSC_CONFIGURED 1";
    char command_wsc_ssid[31] = "flash set WLAN0_VAP4_WSC_SSID ";
    char command[128];

    apmib_set(MIB_REPEATER_SSID1, (void *)ssid);

    strcpy(command, command_wlan_vap4_ssid);
    strcat(command, ssid);
    printf("New command string for SSID = %s\n", command);
    system(command);

    bzero(command, 128);
    strcpy(command, command_wsc_ssid);
    strcat(command, ssid);
    printf("New command string wsc_ssid = %s\n", command);
    system(command);
    bzero(command, 128);

    strcpy(command, command_wpa_psk);
    strcat(command, passwd);
    printf("New command string = %s\n", command);
    system(command);

    system(command_wlan_disable);
    system(command_wlan_encrypt);
    system(command_wsc_configured);
    apmib_set(MIB_REPEATER_ENABLED1, (void *) &enable);
    apmib_set(MIB_DHCP, (void *) &enable);
    apmib_update(CURRENT_SETTING);

    return;
}
#endif

#ifdef CLIENT
//client code
int main(int argc, char** argv) {
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in addr;
	int n;
    char buffer[256], in_buffer[256];
    char header[] = "This is request from client";

    if(sockfd < 0) {
        printf(" open socket fail\n");
	return -1;
    }    

    addr.sin_family = AF_INET;
    addr.sin_port = htons(CONFIG_PORT);
    addr.sin_addr.s_addr = DEFAULT_AP_IP_ADDRESS;
    if (connect(sockfd, (struct sockaddr *) &addr, sizeof (addr)) < 0) {
	    printf("Conect to AP server is failed\n");
        return -1;
    }

    printf("\nInput WIFI SSID:");
    bzero(buffer, 255);
    bzero(in_buffer, 255);
    scanf("%s",buffer); 
    n = send(sockfd, buffer, strlen(buffer), 0);
    n = read(sockfd, in_buffer, 63);
    printf("Client received from server: %s \n", in_buffer);
    printf("\nInput WIFI PASSWORD:");
    bzero(buffer, 255);
    bzero(in_buffer, 255);
    scanf("%s",buffer); 
    n = send(sockfd, buffer, strlen(buffer),0);
    n = read(sockfd, in_buffer, 63);
    printf("Client received from server %s\n", in_buffer);

    close(sockfd);
    
    return (EXIT_SUCCESS);
}

#else //server code

int main(int argc, char** argv) {
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    int cfd, client_addr_len;
    struct sockaddr_in addr, client_addr;
    char ssid_buffer[256], passwd_buffer[256];
    char out_buffer[2][64]= { "Receive SSID", "receive PASSWORD"};
    //char confirm[30] = "Connect OK";
    int n;

    apmib_init();

    if(sockfd < 0) {
        printf(" open socket fail\n");
	return -1;
    }

    addr.sin_family = AF_INET;
    addr.sin_port = htons(CONFIG_PORT);
    addr.sin_addr.s_addr = INADDR_ANY;
    n = 1;

    if(setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &n, sizeof(int)) < 0){
        perror("setsocketopt");
	exit(1);
    }

    if( (n = bind(sockfd, (const struct sockaddr *)&addr, sizeof(addr))) < 0){
        printf("bind to socket failed return value=%d errno=%s\n", n, strerror(errno));
        return -1;
    }

    if(listen(sockfd, 5) < 0) {
        printf("listen socket failed\n");
        return -1;
    }

    client_addr_len = sizeof(client_addr);
    cfd = accept(sockfd, (struct sockaddr *)&client_addr, (socklen_t *)&client_addr_len);
    printf("Acquire cfd = %d\n", cfd);
    if(cfd < 0) {
        printf("Accpet return error\n");
        return -1;
    }		
    
    bzero(ssid_buffer, 255);
    n = read(cfd, ssid_buffer, 255);
    if(n == 0) {
        printf("Read client input buffer error\n");
        return -1;		
    }
    printf("Received from config client: %s length= %d \n", ssid_buffer, n);
    send(cfd, out_buffer[0], 63, 0);

    bzero(passwd_buffer, 255);
    n = read(cfd, passwd_buffer, 255);
    if(n == 0) {
        printf("Read client input buffer error\n");
        return -1;		
    }
    printf("Received from config client: %s len=%d \n", passwd_buffer, n);
    send(cfd, out_buffer[1], 63,0);

    set_test_wifi(ssid_buffer, passwd_buffer);

    if(is_connected()){
	printf("\n\nConnection is ok\n");
        set_flash(ssid_buffer, passwd_buffer);
    }
    else
	printf("\n\nConnection time out\n");
    
    close(sockfd);
    close(cfd);

    return (EXIT_SUCCESS);
}	
#endif

