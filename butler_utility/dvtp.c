#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>
#include <signal.h>
#include "apmib.h"

pthread_t stress_thread = 0;
pthread_t downloading_thread = 0;
pthread_t blaster_thread = 0;
pthread_t audio_thread = 0;

int stress_flag=0;
int download_flag =0;
int blaster_flag =0;
int audio_flag = 0;

char *menu[] = {
        "0. Stop test",
        "1. wifi max throughput",
        "2. wifi off",
        "3. audio on",
        "4. audio off",
        "5. Play audio",
        "6. Start CPU stress",
        "7. Stop CUP stress",
        "8. Reset UE878",
        "9. UE878 continous blasting",
        "10. show \"top\"",
        "11. LED ON",
        "12. LED OFF ",
};

int show_menu(void){
    int i;
    for (i = 0; i<13; i++) {
	if(i == 6 && stress_flag ==1){
            printf("    %s*\n", menu[i]);
	} else {
            printf("    %s\n", menu[i]);
	}
    }
    scanf("%d", &i);
    return(i);
}

void *calculation(void *data) {
    int i;
    double  x, y;
      for(;;){
	      x= 550.0;
	      y =555.;
	      x= pow(x,y);
	      y = log(x);
	      y = sqrt(x);
	      if(stress_flag == 0)
	          break;
      }
    return;
}

void wifi_up(void)
{
	system("ifconfig eth0 up");
	system("ifconfig wlan0 up");
	system("ifconfig wlan0-vxd up");
	system("ifconfig br0 up");
}

void wifi_down(void)
{
    download_flag = 0;
    system("ifconfig eth0 down");
    system("ifconfig wlan0 down");
    system("ifconfig wlan0-vxd down");
    system("ifconfig br0 down");
}

void *start_download(void *data){
    for(;;){
	if( download_flag == 1)
            system("/home/scripts/download_loop.sh");
	else
            break;
    }
    return;
}

void wifi_max_throughput(void){
    char read_url[256];
    int n;
    apmib_init();
    apmib_get(MIB_UEI_FW_URL, (void *)read_url);
    printf("\n\nCurrent URL in flash:%s\n", read_url);
    printf("If this is correct URL for testing? 1--yes 0--No\n");
    scanf("%d",&n);
    if(n != 1){
         printf("\nPlease type new URL for downloding file:\n");
         scanf("%s",read_url);
	 apmib_set(MIB_UEI_FW_URL, (void *)read_url);
         apmib_update(CURRENT_SETTING);
    } 
    download_flag = 1;  
    pthread_create(&downloading_thread, NULL, &start_download, NULL);
    return;
}
void wifi_max_throughput_stop(void){
	download_flag = 0;
}

void stress_start(void){
    stress_flag = 1;
    pthread_create(&stress_thread, NULL, &calculation, NULL);
    printf("Thread ID = 0x%x\n", stress_thread);
    return;
}

void stress_stop(void){
    if(stress_thread != 0){
        pthread_cancel(stress_thread);
	stress_thread = 0;
    } else
        printf("No stress test is runing\n");
    stress_flag = 0; 
    return;
}

void reset_ue878(void){
    system("echo 8 > /sys/class/gpio/export");
    system("echo out > /sys/class/gpio/gpio8/direction");
    system("echo 1 > /sys/class/gpio/gpio8/value");
    system("echo 0 > /sys/class/gpio/gpio8/value");
    system("echo 1 > /sys/class/gpio/gpio8/value");
}

void *blastering(void *p){
    int ms = 500;
    int tmp = 0;

    system("/home/scripts/blaster_ir.sh");
}

void *audio_script(void * para){
    if( audio_flag != 0)
       system("/home/scripts/audio_test.sh");
    else
        return;
}


void start_audio(void)
{
    audio_flag = 1;
    pthread_create(&audio_thread, NULL, &audio_script, NULL);
    return;
}

void stop_audio(void){
    if(audio_flag != 0 && audio_thread != 0) {
        pthread_cancel(audio_thread);
        audio_flag = 0;
    }
}


void start_blastering(void)
{
    blaster_flag = 1;
    printf("\n start blatering\n");
    pthread_create(&blaster_thread, NULL, &blastering, NULL);
}

void main(void)
{
    int x;
    while(1) {
        x = show_menu();

        while (x < 0 || x > 10){
	    x= show_menu();
        }

        printf("you typed in %d\n",x);
        switch(x){
            case 0: 
                 stress_stop();
		 printf("Test is done\n");
		 exit(1);
	    case 1: 
		 wifi_up();
		 wifi_max_throughput(); 
		 break;
	    case 2: 
		 wifi_down();
	         break;
	    case 3: 
		 system("echo \"00c3 6\" > /sys/devices/platform/rtl819x-codec-iic/codec_reg");
		 break;
	    case 4:
		 stop_audio();
		 system("echo \"00c3 0\" > /sys/devices/platform/rtl819x-codec-iic/codec_reg");
		 break;
	    case 5:
		  start_audio();
		  break;
	    case 6: 
	        stress_start();
	        break;
	    case 7: 
	        stress_stop();
	        break;
	    case 8: 
		reset_ue878();
		break;
	    case 9:
		start_blastering();
		break;
	    case 10:
		system("top");
                break;
            case 11:
                system("led_control 1");
                break;
            case 12:
                system("led_control 0");
                break;  
            default:
	        break;
        }
    }
}
