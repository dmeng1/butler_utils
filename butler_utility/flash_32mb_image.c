#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <linux/fs.h>
#include <mtd/mtd-user.h>
#include <unistd.h>


#include "uei_image.h"

#define MB 0x100000
#define FLASH_BLOCK_FILE "block_file.bin"

int write_flash(char *dev_name, int flash_offset, unsigned char *buffer, int buffer_offset, int length)
{
	int fm, flash_len;
        mtd_info_t mtd_info;
        erase_info_t ei;
	unsigned char *tmp;

        fm = open(dev_name, O_RDWR);
	if(fm <= 0){
	   printf("open %s fail\n", dev_name);
	   return(-1); 
	}

	lseek(fm, flash_offset, SEEK_SET);

	if(buffer == NULL){
	    printf("No memory buffer\n");
	    close(fm);
	    return; 
	}

	tmp = (unsigned char *) (buffer + buffer_offset);

        flash_len = write(fm, tmp, length);
        if(flash_len != length){
	    printf("There are less than 0x%x bytes writeable to flash wirte length:0x%x\n", length, flash_len);
	}
        sync();
        if(ioctl(fm, BLKFLSBUF, NULL) < 0){
            printf("flush mtd system cache error\n");
        }
        close(fm);
	return(flash_len);
}

int main(void)
{
    unsigned char *buffer = NULL;
    FILE *fd;
    int i, len, file_offset = 0;

    fd = fopen(FLASH_BLOCK_FILE, "rb");
    if(fd ==NULL ){
	 printf("The file %s is not available\n", FLASH_BLOCK_FILE);
	 return(0);
    }

    printf("read and write Linux section\n");
    len = 2.5 * MB ;
    buffer = (unsigned char *) malloc(len);
    fseek(fd, file_offset, SEEK_SET);
    fread(buffer, len, 1, fd);
    write_flash(linux_dev, 0, buffer, 0, len);
    file_offset += len; 

    printf("read and write root file system\n");
    len = 16 * MB ;
    buffer = (unsigned char *) realloc(buffer, len);
    fseek(fd, file_offset, SEEK_SET);
    fread(buffer, len, 1, fd);
    write_flash(rootfs_dev, 0, buffer, 0, len);
    file_offset += len; 

    printf("read and write jffs2 \n");
    len = 9 * MB ;
    buffer = (unsigned char *) realloc(buffer, len);
    fseek(fd, file_offset, SEEK_SET);
    fread(buffer, len, 1, fd);
    write_flash(jffs_dev, 0, buffer, 0, len);
    file_offset += len; 

    printf("read and write Linux backup part\n");
    len = 2.5 * MB ;
    buffer = (unsigned char *) realloc(buffer, len);
    fseek(fd, file_offset, SEEK_SET);
    fread(buffer, len, 1, fd);
    write_flash(linux_backup, 0, buffer, 0, len);
    file_offset += len; 

    printf("read and write rootfs backup part\n");
    len = 2 * MB ;
    buffer = (unsigned char *) realloc(buffer, len);
    fseek(fd, file_offset, SEEK_SET);
    fread(buffer, len, 1, fd);
    write_flash(rootfs_backup, 0, buffer, 0, len);

    printf("the flash image update is complete. Total length is =0x%x\n last file offset=0x%x\n", (file_offset+len), file_offset);

    free(buffer);
    fclose(fd);

    return(0);
}

