#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <mtd/mtd-user.h>
#include <unistd.h>


#include "uei_image.h"

//#define __DEBUG__ 	0 

int endian_adjust(int input)
{
	int out1, out2;

	out1 =(input >> 16);
	out1 = (out1 >> 8)|(out1 & 0xff) << 8;

	out2 = (input & 0xffff);
        out2 = (out2 >> 8) | (out2 & 0xff) << 8;
	return (out2 << 16| out1);
}

void read_flash(char * dev_name, int flash_offset, unsigned char *buffer, int length)
{
	int fm, i, j, flash_len;

        fm = open(dev_name, O_RDWR);
	if(fm <= 0){
	   printf("open %s fail\n", linux_dev);
	   return; 
	}
	
	lseek(fm,flash_offset, SEEK_SET);

	if(buffer == NULL){
	    printf("No memory buffer\n");
	    close(fm);
	    return; 
	}

        flash_len = read(fm, buffer, length);
        if(flash_len != length){
	    printf("There are less than 0x%x bytes readable from flash\n", length);
	}
#ifdef __DEBUG__
	j = 0;
	for(i = 0; i <  256; i++){	
	      printf("%2x ", buffer[i]);
	      if(j == 15){
		      printf("\n");
		      j=0;
	      }
	      else 
		      j++;
	}
        printf("\n\n");
#endif 
        free(buffer);
        close(fm);
}

int write_flash(char *dev_name, int flash_offset, unsigned char *buffer, int buffer_offset, int length)
{
	int fm, flash_len;
        mtd_info_t mtd_info;
        erase_info_t ei;
	unsigned char *tmp;

        fm = open(dev_name, O_RDWR);
	if(fm <= 0){
	   printf("open %s fail\n", dev_name);
	   return(-1); 
	}
	
	ioctl(fm, MEMGETINFO, &mtd_info);
	printf("MTD Type: %x\n MTD total size: %x bytes\n MTD erase size:%x bytes\n", mtd_info.type, mtd_info.size, mtd_info.erasesize);

        if(flash_offset % mtd_info.erasesize){
	    printf("The flash_offset(0x%x) is NOT multiply of erase_size(0x%x)\n", flash_offset, mtd_info.erasesize);
	}

	ei.length = mtd_info.erasesize;

	for(ei.start = flash_offset; ei.start < mtd_info.size; ei.start += ei.length)
	{
		ioctl(fm, MEMUNLOCK, &ei);
		ioctl(fm, MEMERASE, &ei);
		printf(".");
	}
        printf("\n\n");

	lseek(fm, flash_offset, SEEK_SET);

	if(buffer == NULL){
	    printf("No memory buffer\n");
	    close(fm);
	    return; 
	}

	tmp = (unsigned char *) (buffer + buffer_offset);

        flash_len = write(fm, tmp, length);
        if(flash_len != length){
	    printf("There are less than 0x%x bytes writeable to flash wirte length:0x%x\n", length, flash_len);
	}
        close(fm);
	return(flash_len);
}


unsigned char *read_file(char * file_name,  int in_offset, int *out_len)
{
	FILE *fd;
	int file_len, i, j;
	unsigned char hbuf[16];
	IMG_HEADER_T *header;
	unsigned char *out_buf = NULL;

	fd = fopen(file_name, "rb");

	if(fd ==NULL ){
		printf("The file %s is not available\n", file_name);
		return NULL;
	}
        
	fseek(fd, 0L, SEEK_SET);
	fread(hbuf, sizeof(IMG_HEADER_T), 1, fd);
	header = (IMG_HEADER_T *)hbuf;
        
	header->startAddr = endian_adjust(header->startAddr);
	header->burnAddr = endian_adjust(header->burnAddr);
	header->len = endian_adjust(header->len);
        
	out_buf = (unsigned char *)malloc(header->len + 16);

	if(out_buf == NULL)
	{
		printf("Out of system memory\n");
		return NULL;
	}

#ifdef __DEBUG__
	printf("The file %s signature =%4s, start_addr =0x%x, burn_address=0x%x length is 0x%x\n", file_name, header->signature, header->startAddr, header->burnAddr, header->len);
#endif

	fseek(fd, in_offset, SEEK_SET);
	fread(out_buf, header->len + 16, 1, fd);

#ifdef __DEBUG__
	printf("The first 256 bytes of the file: %s\n", file_name);
	j = 0;
	for(i=0; i< 256; i++) {
	      printf(" 0x%2x ", out_buf[i]);
	      if(j == 15){
		      printf("\n");
		      j=0;
	      }
	      else 
		      j++;
        }
	printf("\n\n");
	
	printf("The last 256 bytes of the file %s\n", file_name);
	j = 0;
	for(i=(header->len + 16 - 256) ; i< (header->len + 16); i++){
		printf(" 0x%2x ", out_buf[i]);
	      if(j == 15){
		      printf("\n");
		      j=0;
	      }
	      else 
		      j++;
	}
	printf("\n\n");
#endif
	fclose(fd);

	*out_len = header->len;
        
	return(out_buf);
}


unsigned char *read_partial_file(FILE *fd,  int file_offset, int *out_len)
{
	IMG_HEADER_T *header;
	unsigned char *out_buf = NULL;
	unsigned char hbuf[20];
	int i, j;

	if(fd == NULL ){
		printf("The file ID(%d) is invalide\n", fd);
		return NULL;
	}
        
	fseek(fd, file_offset, SEEK_SET);
	fread(hbuf, sizeof(IMG_HEADER_T), 1, fd);
	header = (IMG_HEADER_T *)hbuf;
        
	header->startAddr = endian_adjust(header->startAddr);
	header->burnAddr = endian_adjust(header->burnAddr);
	header->len = endian_adjust(header->len);
        
	out_buf = (unsigned char *)malloc(header->len + 16);

	if(out_buf == NULL)
	{
		printf("Out of system memory\n");
		return NULL;
	}

#ifdef __DEBUG__
	printf("The signature =%4s, start_addr =0x%x, burn_address=0x%x length is 0x%x\n", header->signature, header->startAddr, header->burnAddr, header->len);
#endif

	fseek(fd, file_offset, SEEK_SET);
	fread(out_buf, header->len + 16, 1, fd);

#ifdef __DEBUG__
	printf("The first 256 bytes of the file\n");
	j = 0;
	for(i=0; i< 256; i++) {
	      printf(" 0x%2x ", out_buf[i]);
	      if(j == 15){
		      printf("\n");
		      j=0;
	      }
	      else 
		      j++;
        }
	printf("\n\n");
	
	printf("The last 256 bytes of the file\n");
	j = 0;
	for(i=(header->len + 16 - 256) ; i< (header->len + 16); i++){
		printf(" 0x%2x ", out_buf[i]);
	      if(j == 15){
		      printf("\n");
		      j=0;
	      }
	      else 
		      j++;
	}
	printf("\n\n");
#endif

	*out_len = header->len;
        
	return(out_buf);
}

int upgrade_rootfs_from_mem(unsigned char * buffer, int len)
{
    if(buffer == NULL ){
        printf(" rootfs buffer is NULL\n");
        return(-1);
    }

    write_flash(rootfs_backup_write, 0, buffer, ROOTFS_BUFFER_OFFSET, len);
    return(0);
}

int upgrade_linux_from_mem(unsigned char * buffer, int len)
{
    if(buffer == NULL ){
        printf(" rootfs buffer is NULL\n");
        return(-1);
    }

    write_flash(linux_backup_write, LINUX_BACKUP_OFFSET, buffer, 0, (len + 16));
    return(0);
}

void upgrade_fw(void)
{
    FILE *fd;
    int len;
    unsigned char *buf;


    fd = fopen("fw.backup","rb");
    if(fd == NULL) {
	    printf("Could not open file(%s)\n", FW_BIN);
	    return;
    }

    buf = (unsigned char *)read_partial_file(fd, 0, &len); //Read linux part
    if(buf == NULL){
        printf("Read linux part of the fw.bin failed\n");
	return;
    }

    upgrade_linux_from_mem(buf, len);

    free(buf);

    buf = read_partial_file(fd, (len+16), &len); //Read root part
    
    if(buf == NULL){
        printf("Read root part of the fw.bin failed\n");
	return;
    }

    upgrade_rootfs_from_mem(buf, len);
   
    free(buf);

    fclose(fd);
}

int main(void)
{
    upgrade_fw();                   //upgrade fw.bin
    return 1;
}

