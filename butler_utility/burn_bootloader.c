#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <linux/fs.h>
#include <mtd/mtd-user.h>
#include <unistd.h>


#include "uei_image.h"

//#define __DEBUG__ 	1 



void kill_processes(void)
{
    printf("upgrade: killing tasks...\n");
    kill(1, SIGTSTP);               /* Stop init from reforking tasks */
    kill(1, SIGSTOP);
    kill(2, SIGSTOP);
    kill(3, SIGSTOP);
    kill(4, SIGSTOP);
    kill(5, SIGSTOP);
    kill(6, SIGSTOP);
    kill(7, SIGSTOP);
    //atexit(restartinit);          /* If exit prematurely, restart init */
    signal(SIGTERM,SIG_IGN);        /* Don't kill ourselves... */
    setpgrp();                      /* Don't let our parent kill us */
    sleep(1);
    signal(SIGHUP, SIG_IGN);        /* Don't die if our parent dies due to
                                     * a closed controlling terminal */
}

int fwChecksumOk(char *data, int len)
{
    unsigned short sum=0;
    int i;

    for (i=0; i<len; i+=2) {
        sum += WORD_SWAP( *((unsigned short *)&data[i]) );
									        }
    return(sum);
}


int endian_adjust(unsigned int input)
{
	unsigned int out1, out2;

	out1 =(input >> 16);
	out1 = (out1 >> 8)|(out1 & 0xff) << 8;
	out2 = (input & 0xffff);
        out2 = (out2 >> 8) | (out2 & 0xff) << 8;
	return (out2 << 16| out1);
}

void read_flash(char * dev_name, int flash_offset, unsigned char *buffer, int length)
{
	int fm, i, j, flash_len;

        fm = open(dev_name, O_RDWR);
	if(fm <= 0){
	   printf("open %s fail\n", linux_dev);
	   return; 
	}
	
	lseek(fm,flash_offset, SEEK_SET);

	if(buffer == NULL){
	    printf("No memory buffer\n");
	    close(fm);
	    return; 
	}

        flash_len = read(fm, buffer, length);
        if(flash_len != length){
	    printf("There are less than 0x%x bytes readable from flash\n", length);
	}
#ifdef __DEBUG__
	j = 0;
	for(i = 0; i <  256; i++){	
	      printf("%2x ", buffer[i]);
	      if(j == 15){
		      printf("\n");
		      j=0;
	      }
	      else 
		      j++;
	}
        printf("\n\n");
#endif 
        free(buffer);
        close(fm);
}

int write_flash(char *dev_name, int flash_offset, unsigned char *buffer, int buffer_offset, int length)
{
	int fm, flash_len;
        mtd_info_t mtd_info;
        erase_info_t ei;
	unsigned char *tmp;

        fm = open(dev_name, O_RDWR);
	if(fm <= 0){
	   printf("open %s fail\n", dev_name);
	   return(-1); 
	}

	lseek(fm, flash_offset, SEEK_SET);

	if(buffer == NULL){
	    printf("No memory buffer\n");
	    close(fm);
	    return; 
	}

	tmp = (unsigned char *) (buffer + buffer_offset);

        flash_len = write(fm, tmp, length);
        if(flash_len != length){
	    printf("There are less than 0x%x bytes writeable to flash wirte length:0x%x\n", length, flash_len);
	}
        sync();
        if(ioctl(fm, BLKFLSBUF, NULL) < 0){
            printf("flush mtd system cache error\n");
        }
        close(fm);
	return(flash_len);
}


unsigned char *read_file(char * file_name,  int in_offset, int *out_len)
{
	FILE *fd;
	int file_len, i, j;
	unsigned char hbuf[16];
	IMG_HEADER_T *header;
	unsigned char *out_buf = NULL;

	fd = fopen(file_name, "rb");

	if(fd ==NULL ){
		printf("The file %s is not available\n", file_name);
		return NULL;
	}
        
	fseek(fd, 0L, SEEK_SET);
	fread(hbuf, sizeof(IMG_HEADER_T), 1, fd);
	header = (IMG_HEADER_T *)hbuf;
       
	printf("\n\n");
        for(i=0; i<16;i++) {
	    	printf("0x%x ", hbuf[i]);
	}	
	header->startAddr = endian_adjust(header->startAddr);
	header->burnAddr = endian_adjust(header->burnAddr);
	header->len = endian_adjust(header->len);
        
	printf("\nThe file %s signature =%4s, start_addr =0x%x, burn_address=0x%x length is 0x%x\n", file_name, header->signature, header->startAddr, header->burnAddr, header->len);

	out_buf = (unsigned char *)malloc(header->len + 16);

	if(out_buf == NULL)
	{
		printf("Out of system memory\n");
		fclose(fd);
		return NULL;
	}

	fseek(fd, in_offset, SEEK_SET);
	fread(out_buf, header->len + 16, 1, fd);

#ifdef __DEBUG__
	printf("The first 256 bytes of the file: %s\n", file_name);
	j = 0;
	for(i=0; i< 256; i++) {
	      printf(" 0x%2x ", out_buf[i]);
	      if(j == 15){
		      printf("\n");
		      j=0;
	      }
	      else 
		      j++;
        }
	printf("\n\n");
	
	printf("The last 256 bytes of the file %s\n", file_name);
	j = 0;
	for(i=(header->len + 16 - 256) ; i< (header->len + 16); i++){
		printf(" 0x%2x ", out_buf[i]);
	      if(j == 15){
		      printf("\n");
		      j=0;
	      }
	      else 
		      j++;
	}
	printf("\n\n");
#endif
	fclose(fd);

	*out_len = header->len;
        
	return(out_buf);
}


int upgrade_bootloader_from_mem(unsigned char * buffer, int len)
{
    if(buffer == NULL ){
        printf(" boot.bin buffer is NULL\n");
        return(-1);
    }
    printf("Update bootloader: Write_device %s flash_offset=0x%x buffer lenght=0x%x i buffer offset is 16\n", boot_dev, BOOT_LOADER_OFFSET, (len+16));
    printf("bootloader checksum = 0x%X\n", fwChecksumOk(&buffer[16], len));
    write_flash(boot_dev, BOOT_LOADER_OFFSET, buffer, 16, (len + 16)); //The first 16 bytes are header should not write to flash so teh buffer offset is 16
    return(0);
}

int upgrade_bootloader_from_file(void)
{
    unsigned char *buffer;
    int len;

    buffer = read_file(BOOT_BIN, 0, &len); //Read whole file into buffer include 16 byte header 

    if(buffer == NULL ){
        printf("Out of memory\n");
        return(-1);
    }

    upgrade_bootloader_from_mem(buffer, len);

    free(buffer);

    return(0);
}


/*******************************************************************
*  detect_file_type() return image file type			   *
*  return values:                                                  *
*      ROOTFS_TYPE if the file is rootfs                           *
*      LINUX_TYPE  if the file is linux image			   *
*      FW_TYPE	   if the file contains linux and rootfs	   *
*******************************************************************/      

int detect_file_type(void)
{
	FILE *fd;
	int file_len;
	unsigned char hbuf[16];
	IMG_HEADER_T *header;

	fd = fopen(BOOT_BIN, "rb");

	if(fd ==NULL ){
		printf("The file %s is not available\n", FW_BIN);
		return NONE;
	}
        
	fseek(fd, 0L, SEEK_SET);
	fread(hbuf, sizeof(IMG_HEADER_T), 1, fd);
	header = (IMG_HEADER_T *)hbuf;
	header->len = endian_adjust(header->len);
        
	fseek(fd, 0L, SEEK_END);
	file_len = ftell(fd);

#ifdef DEBUG
        printf("The file length is 0x%x\n, data_length =0x%x", file_len, header->len);
#endif        
	fclose(fd);
        
	if(!memcmp(header->signature, BOOT_SIGNATURE, SIG_LEN)){
	    printf("This is boot.bin file\n");  
	    return(BOOT_TYPE);
	   }
        printf("This is not boot.bin file and not able to write to boot section\n");
	return NONE;
}

int main(void)
{
    int file_type;

#if 1 
    system("ifconfig br0 down 2> /dev/null");
    system("ifconfig eth0 down 2> /dev/null");
    system("ifconfig eth1 down 2> /dev/null");
    kill_processes();
    sleep(2);
#endif 

    file_type = detect_file_type();

    if(file_type == BOOT_TYPE)  
        upgrade_bootloader_from_file();  //Upgrade bootloader 
    else {
        printf("Unreconized file type \n");
	return 0;
    }
    
    printf("Boot loader update is done\n");

    return 1;
}

