#include <sys/ioctl.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#define Addr_GND  0x64//7 bit format is 0x64
#define Addr_VCC  0xCE//7 bit format is 0x67
#define Addr_SCL  0xCA//7 bit format is 0x65
#define Addr_SDA  0xCC//7 bit format is 0x66
int file=-1;
int IS_IIC_WriteByte(int file, unsigned char i2caddr, unsigned char addr, unsigned char data);
unsigned char PWM_Gamma64[64]=
{
  0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
  0x08,0x09,0x0b,0x0d,0x0f,0x11,0x13,0x16,
  0x1a,0x1c,0x1d,0x1f,0x22,0x25,0x28,0x2e,
  0x34,0x38,0x3c,0x40,0x44,0x48,0x4b,0x4f,
  0x55,0x5a,0x5f,0x64,0x69,0x6d,0x72,0x77,
  0x7d,0x80,0x88,0x8d,0x94,0x9a,0xa0,0xa7,
  0xac,0xb0,0xb9,0xbf,0xc6,0xcb,0xcf,0xd6,
  0xe1,0xe9,0xed,0xf1,0xf6,0xfa,0xfe,0xff
};



void usage(void ) {
	printf("    Usage: This is LED control for download image\n");
	printf("\t led_download 1------- Connecting to wifi\n" );
	printf("\t led_download 2------- OTA operation NB-1986\n");
	printf("\t led_download 3------- OTA errror NB-1986\n");
	printf("\t led_download 4------- updating small image loop yellow NB-2099\n");
	printf("\t led_download 5------- Update small image success Loop green NB-2099\n");
	printf("\t led_download 6------- Update small image error Loop red NB-2099\n");
	printf("\t led_download 7------- Original Download \n");
	printf("\t led_download 8------- Download error\n");
}

int open_i2c_dev(int i2cbus)
{
	char filename[64];
	int file;
	
	sprintf(filename, "/dev/i2c-%d", i2cbus);
	file = open(filename, O_RDWR);
	if (file < 0) {
		fprintf(stderr, "Error: Could not open file `%s': %s\n", filename, strerror(ENOENT));
	}
	
	return file;
}

static int check_funcs(int file)
{
	unsigned long funcs;
	
	/* check adapter functionality */
	if (ioctl(file, I2C_FUNCS, &funcs) < 0) {
		fprintf(stderr, "Error: Could not get the adapter functionality matrix: %s\n", strerror(errno));
		return -1;
	}
	
	if (!(funcs & I2C_FUNC_I2C)) {
		fprintf(stderr, "Error: Adapter does not have %s capability\n", "I2C transfers");
		return -1;
	}
	
	return 0;
}

int i2c_read(int file, int i2caddr, int addr) {
	__u8 write_buf[2];
	__u8 read_buf[2];
	struct i2c_msg xfer[2];
	struct i2c_rdwr_ioctl_data rdwr;
	int ret, data;
	
	memset(write_buf, 0, sizeof(write_buf));
	memset(read_buf, 0, sizeof(read_buf));
	
	write_buf[0] = (addr & 0xff00) >> 8;
	write_buf[1] = (addr & 0x00ff) >> 0;
	
	xfer[0].addr = i2caddr;
	xfer[0].flags = 0;
	xfer[0].len = 2;
	xfer[0].buf = write_buf;
	
	xfer[1].addr = i2caddr;
	xfer[1].flags = I2C_M_RD;
	xfer[1].len = 2;
	xfer[1].buf = read_buf;
	
	rdwr.msgs = xfer;
	rdwr.nmsgs = 2;
	ret = ioctl(file, I2C_RDWR, &rdwr);
	if (ret != 2) {
		fprintf(stderr, "Error: Sending messages failed: %s\n", strerror(errno));
		return -1;
	}
	
	data = read_buf[1] | read_buf[0] << 8;
	
	return data;
}

int i2c_write(int file, int i2caddr, int addr, int data) {
	__u8 write_buf[4];
	struct i2c_msg xfer[1];
	struct i2c_rdwr_ioctl_data rdwr;
	int ret;
	
	memset(write_buf, 0, sizeof(write_buf));
	
	write_buf[0] = (addr & 0xff00) >> 8;
	write_buf[1] = (addr & 0x00ff) >> 0;
	write_buf[2] = (data & 0xff00) >> 8;
	write_buf[3] = (data & 0x00ff) >> 0;
	
	xfer[0].addr = i2caddr;
	xfer[0].flags = 0;
	xfer[0].len = 4;
	xfer[0].buf = write_buf;
	
	rdwr.msgs = xfer;
	rdwr.nmsgs = 1;
	ret = ioctl(file, I2C_RDWR, &rdwr);
	if (ret != 1) {
		fprintf(stderr, "Error: Sending messages failed: %s\n", strerror(errno));
		return -1;
	}
	
	return 0;
}
void Init_FL3199(void)
{
	int i;
  IS_IIC_WriteByte(file, Addr_GND,0x00,0x01);//normal operation 
  IS_IIC_WriteByte(file, Addr_GND,0x01,0x77);//on/off
  IS_IIC_WriteByte(file, Addr_GND,0x02,0x07);//on/off
  IS_IIC_WriteByte(file, Addr_GND,0x03,0x00); 
  //IS_IIC_WriteByte(file, Addr_GND,0x04,0x00); 
  for(i=0x07;i<0x0F;i++){
	IS_IIC_WriteByte(file, Addr_GND, i, 0x00);
	}//PWM data
  IS_IIC_WriteByte(file, Addr_GND, 0x10, 0x00);//updata
  
}

void set_current_level_FL3199(long level)
{
	switch(level) {
	   case 0:
		IS_IIC_WriteByte(file,Addr_GND,0x04,0x00);
		break;
	   case 1:
		IS_IIC_WriteByte(file,Addr_GND,0x04,0x10);
		break;
	   case 2:
		IS_IIC_WriteByte(file,Addr_GND,0x04,0x20);
		break;
	   case 3:
		IS_IIC_WriteByte(file,Addr_GND,0x04,0x30);
		break;
	   case 4:
		IS_IIC_WriteByte(file,Addr_GND,0x04,0x40);
		break;
	   case 5:
		IS_IIC_WriteByte(file,Addr_GND,0x04,0x50);
		break;
	   case 6:
		IS_IIC_WriteByte(file,Addr_GND,0x04,0x60);
		break;
	   case 7:
		IS_IIC_WriteByte(file,Addr_GND,0x04,0x70);
		break;
	   default:
		IS_IIC_WriteByte(file,Addr_GND,0x04,0x00);
		break;
	}
}

void IS31FL3199_mode2(void)//One shot mode, only in setup
{ 

  IS_IIC_WriteByte(file,Addr_GND,0x00,0x01);//normal operation 
  IS_IIC_WriteByte(file,Addr_GND,0x01,0x77);//on/off
  IS_IIC_WriteByte(file,Addr_GND,0x02,0x07);//on/off
  IS_IIC_WriteByte(file,Addr_GND,0x03,0x70);//One shot mode
  //IS_IIC_WriteByte(file,Addr_GND,0x04,0x00); 
  usleep(500000);
  
  IS_IIC_WriteByte(file,Addr_GND,0x07,0xff);//PWM
  IS_IIC_WriteByte(file,Addr_GND,0x08,0xff);
  IS_IIC_WriteByte(file,Addr_GND,0x09,0xff);
  IS_IIC_WriteByte(file,Addr_GND,0x0a,0xff);
  IS_IIC_WriteByte(file,Addr_GND,0x0b,0xff);
  IS_IIC_WriteByte(file,Addr_GND,0x0c,0xff);
  IS_IIC_WriteByte(file,Addr_GND,0x0d,0xff);
  IS_IIC_WriteByte(file,Addr_GND,0x0e,0xff);
  IS_IIC_WriteByte(file,Addr_GND,0x0f,0xff);
  IS_IIC_WriteByte(file,Addr_GND,0x10,0x00);//update PWM registers

  IS_IIC_WriteByte(file,Addr_GND,0x11,0x00);//T0
  IS_IIC_WriteByte(file,Addr_GND,0x12,0x01);
  IS_IIC_WriteByte(file,Addr_GND,0x13,0x02);
  IS_IIC_WriteByte(file,Addr_GND,0x14,0x05);
  IS_IIC_WriteByte(file,Addr_GND,0x15,0x04);
  IS_IIC_WriteByte(file,Addr_GND,0x16,0x03);
  IS_IIC_WriteByte(file,Addr_GND,0x17,0x06);
  IS_IIC_WriteByte(file,Addr_GND,0x18,0x07);
  IS_IIC_WriteByte(file,Addr_GND,0x19,0x09);

  IS_IIC_WriteByte(file,Addr_GND,0x1a,0xa0);//T1~T3
  IS_IIC_WriteByte(file,Addr_GND,0x1b,0xa0);
  IS_IIC_WriteByte(file,Addr_GND,0x1c,0xa0);

  IS_IIC_WriteByte(file,Addr_GND,0x1d,0x07);//T4
  IS_IIC_WriteByte(file,Addr_GND,0x1e,0x07);
  IS_IIC_WriteByte(file,Addr_GND,0x1f,0x07);
  IS_IIC_WriteByte(file,Addr_GND,0x20,0x07);
  IS_IIC_WriteByte(file,Addr_GND,0x21,0x07);
  IS_IIC_WriteByte(file,Addr_GND,0x22,0x07);
  IS_IIC_WriteByte(file,Addr_GND,0x23,0x07);
  IS_IIC_WriteByte(file,Addr_GND,0x24,0x07);
  IS_IIC_WriteByte(file,Addr_GND,0x25,0x07);
 
  IS_IIC_WriteByte(file,Addr_GND,0x26,0x00);
  IS_IIC_WriteByte(file,Addr_GND,0x03,0x70);//One shot mode
}


void Rgb_Pwm_Control_FL3199(unsigned char datx,unsigned char datR,unsigned char datG,unsigned char datB)//datx RGBx
{
    switch(datx) 
      {
          case 1:IS_IIC_WriteByte(file, Addr_GND, 0x07, datR);//set out1 pwm  R 
                 IS_IIC_WriteByte(file, Addr_GND, 0x08, datG);//set out2 pwm  G
                 IS_IIC_WriteByte(file, Addr_GND, 0x09, datB);//set out3 pwm  B
                 break;
          case 2:IS_IIC_WriteByte(file, Addr_GND, 0x0A, datR);//set out4 pwm  R
                 IS_IIC_WriteByte(file, Addr_GND, 0x0B, datG);//set out5 pwm  G
                 IS_IIC_WriteByte(file, Addr_GND, 0x0C, datB);//set out6 pwm  B
                 break;
          case 3:IS_IIC_WriteByte(file, Addr_GND, 0x0D, datR);//set out7 pwm  R
                 IS_IIC_WriteByte(file, Addr_GND, 0x0E, datG);//set out8 pwm  G
                 IS_IIC_WriteByte(file, Addr_GND, 0x0F, datB);//set out9 pwm  B
                 break;
          default:break;
      }
    IS_IIC_WriteByte(file, Addr_GND, 0x10, 0x00);//update PWM registers
}


int IS_IIC_WriteByte(int file, unsigned char i2caddr, unsigned char addr, unsigned char data) {
	__u8 write_buf[4];
	struct i2c_msg xfer[1];
	struct i2c_rdwr_ioctl_data rdwr;
	int ret;
	
	memset(write_buf, 0, sizeof(write_buf));
	
	write_buf[0] = addr;
	write_buf[1] = data;
	
	xfer[0].addr = i2caddr;
	xfer[0].flags = 0;
	xfer[0].len = 2;
	xfer[0].buf = write_buf;
	
	rdwr.msgs = xfer;
	rdwr.nmsgs = 1;
	ret = ioctl(file, I2C_RDWR, &rdwr);
	if (ret != 1) {
		fprintf(stderr, "Error: Sending messages failed: %s\n", strerror(errno));
		return -1;
	}
	
	return 0;
}

void delight_FL3199( void ) {
       Rgb_Pwm_Control_FL3199(1, 0, 0, 0);
       Rgb_Pwm_Control_FL3199(2, 0, 0, 0);
       Rgb_Pwm_Control_FL3199(3, 0, 0, 0);
       Init_FL3199();
}

void set_all_color(int red, int green, int blue) {
       Rgb_Pwm_Control_FL3199(1, red, green, blue);
       Rgb_Pwm_Control_FL3199(2, red, green, blue);
       Rgb_Pwm_Control_FL3199(3, red, green, blue);
}

void connect_wifi(void )//One shot mode, only in setup
{
  IS_IIC_WriteByte(file, Addr_GND,0x00,0x01);//normal operation 
  IS_IIC_WriteByte(file, Addr_GND,0x01,0x77);//on/off
  IS_IIC_WriteByte(file, Addr_GND,0x02,0x07);//on/off
  IS_IIC_WriteByte(file, Addr_GND,0x03,0x70);//One shot mode
  //IS_IIC_WriteByte(file, Addr_GND,0x04,0x00);
  usleep(500000);

  set_all_color(255, 255, 255);
  
  IS_IIC_WriteByte(file, Addr_GND,0x11,0x00);//T0
  IS_IIC_WriteByte(file, Addr_GND,0x12,0x00);
  IS_IIC_WriteByte(file, Addr_GND,0x13,0x00);

  IS_IIC_WriteByte(file, Addr_GND,0x14,0x01);
  IS_IIC_WriteByte(file, Addr_GND,0x15,0x01);
  IS_IIC_WriteByte(file, Addr_GND,0x16,0x01);

  IS_IIC_WriteByte(file, Addr_GND,0x17,0x02);
  IS_IIC_WriteByte(file, Addr_GND,0x18,0x02);
  IS_IIC_WriteByte(file, Addr_GND,0x19,0x02);

  IS_IIC_WriteByte(file, Addr_GND,0x1a,0x00);//T1=T3=T T2= 0
  IS_IIC_WriteByte(file, Addr_GND,0x1b,0x00);
  IS_IIC_WriteByte(file, Addr_GND,0x1c,0x00);

  IS_IIC_WriteByte(file, Addr_GND,0x1d,0x02);//T4=2T ms
  IS_IIC_WriteByte(file, Addr_GND,0x1e,0x02);
  IS_IIC_WriteByte(file, Addr_GND,0x1f,0x02);

  IS_IIC_WriteByte(file, Addr_GND,0x20,0x00);//T4 = 0
  IS_IIC_WriteByte(file, Addr_GND,0x21,0x00);
  IS_IIC_WriteByte(file, Addr_GND,0x22,0x00);

  IS_IIC_WriteByte(file, Addr_GND,0x23,0x02);
  IS_IIC_WriteByte(file, Addr_GND,0x24,0x02);
  IS_IIC_WriteByte(file, Addr_GND,0x25,0x02);

  IS_IIC_WriteByte(file, Addr_GND,0x26,0x00);
}

void download()//One shot mode, only in setup
{
  IS_IIC_WriteByte(file, Addr_GND,0x00,0x01);//normal operation 
  IS_IIC_WriteByte(file, Addr_GND,0x01,0x77);//on/off
  IS_IIC_WriteByte(file, Addr_GND,0x02,0x07);//on/off
  IS_IIC_WriteByte(file, Addr_GND,0x03,0x70);//One shot mode
  //IS_IIC_WriteByte(file, Addr_GND,0x04,0x00);
  usleep(500000);

  set_all_color(255, 255, 255);
  
  IS_IIC_WriteByte(file, Addr_GND,0x11,0x00);//T0
  IS_IIC_WriteByte(file, Addr_GND,0x12,0x00);
  IS_IIC_WriteByte(file, Addr_GND,0x13,0x00);

  IS_IIC_WriteByte(file, Addr_GND,0x14,0x01);
  IS_IIC_WriteByte(file, Addr_GND,0x15,0x01);
  IS_IIC_WriteByte(file, Addr_GND,0x16,0x01);

  IS_IIC_WriteByte(file, Addr_GND,0x17,0x02);
  IS_IIC_WriteByte(file, Addr_GND,0x18,0x02);
  IS_IIC_WriteByte(file, Addr_GND,0x19,0x02);

  IS_IIC_WriteByte(file, Addr_GND,0x1a,0x17);//T1=T3= 0 ms; T2=520 ms
  IS_IIC_WriteByte(file, Addr_GND,0x1b,0x17);
  IS_IIC_WriteByte(file, Addr_GND,0x1c,0x17);

  IS_IIC_WriteByte(file, Addr_GND,0x1d,0x03);//T4=520 ms
  IS_IIC_WriteByte(file, Addr_GND,0x1e,0x03);
  IS_IIC_WriteByte(file, Addr_GND,0x1f,0x03);

  IS_IIC_WriteByte(file, Addr_GND,0x20,0x03);
  IS_IIC_WriteByte(file, Addr_GND,0x21,0x03);
  IS_IIC_WriteByte(file, Addr_GND,0x22,0x03);

  IS_IIC_WriteByte(file, Addr_GND,0x23,0x03);
  IS_IIC_WriteByte(file, Addr_GND,0x24,0x03);
  IS_IIC_WriteByte(file, Addr_GND,0x25,0x03);

  IS_IIC_WriteByte(file, Addr_GND,0x26,0x00);
}

void connecting_wifi(void) 
{
    Rgb_Pwm_Control_FL3199(1, 255, 255,255);
    Rgb_Pwm_Control_FL3199(2, 0, 0, 0);
    Rgb_Pwm_Control_FL3199(3, 0, 0, 0);
    usleep(500000);
    Rgb_Pwm_Control_FL3199(1, 0, 0, 0);
    Rgb_Pwm_Control_FL3199(2, 255, 255, 255);
    Rgb_Pwm_Control_FL3199(3, 0, 0, 0);
    usleep(500000);
    Rgb_Pwm_Control_FL3199(1, 0, 0, 0);
    Rgb_Pwm_Control_FL3199(2, 0, 0, 0);
    Rgb_Pwm_Control_FL3199(3, 255, 255, 255);
    usleep(500000);
    Rgb_Pwm_Control_FL3199(1, 0, 0, 0);
    Rgb_Pwm_Control_FL3199(2, 255, 255, 255);
    Rgb_Pwm_Control_FL3199(3, 0, 0, 0);
    usleep(500000);
}

void download_updates(void)
{
    Rgb_Pwm_Control_FL3199(1, 255, 255,255);
    Rgb_Pwm_Control_FL3199(2, 0, 0, 0);
    Rgb_Pwm_Control_FL3199(3, 0, 0, 0);
    usleep(250000);
    Rgb_Pwm_Control_FL3199(1, 0, 0, 0);
    Rgb_Pwm_Control_FL3199(2, 255, 255, 255);
    Rgb_Pwm_Control_FL3199(3, 0, 0, 0);
    usleep(250000);
    Rgb_Pwm_Control_FL3199(1, 0, 0, 0);
    Rgb_Pwm_Control_FL3199(2, 0, 0, 0);
    Rgb_Pwm_Control_FL3199(3, 255, 255, 255);
    usleep(250000);
}

void error(void)
{
    Rgb_Pwm_Control_FL3199(1, 255, 85, 0);
    Rgb_Pwm_Control_FL3199(2, 255, 85, 0);
    Rgb_Pwm_Control_FL3199(3, 255, 85, 0);
}

void reverse_round( int red, int green, int blue)
{
  IS_IIC_WriteByte(file, Addr_GND,0x00,0x01);//normal operation
  IS_IIC_WriteByte(file, Addr_GND,0x01,0x77);//on/off
  IS_IIC_WriteByte(file, Addr_GND,0x02,0x07);//on/off
  IS_IIC_WriteByte(file, Addr_GND,0x03,0x70);//One shot mode
  //IS_IIC_WriteByte(file, Addr_GND,0x04,0x00);
  usleep(500000);

  set_all_color(red, green, blue);

  IS_IIC_WriteByte(file, Addr_GND,0x11,0x02);//T0
  IS_IIC_WriteByte(file, Addr_GND,0x12,0x02);
  IS_IIC_WriteByte(file, Addr_GND,0x13,0x02);

  IS_IIC_WriteByte(file, Addr_GND,0x14,0x01);
  IS_IIC_WriteByte(file, Addr_GND,0x15,0x01);
  IS_IIC_WriteByte(file, Addr_GND,0x16,0x01);

  IS_IIC_WriteByte(file, Addr_GND,0x17,0x00);
  IS_IIC_WriteByte(file, Addr_GND,0x18,0x00);
  IS_IIC_WriteByte(file, Addr_GND,0x19,0x00);

  IS_IIC_WriteByte(file, Addr_GND,0x1a,0x17);//T1=T3= 0 ms; T2=520 ms
  IS_IIC_WriteByte(file, Addr_GND,0x1b,0x17);
  IS_IIC_WriteByte(file, Addr_GND,0x1c,0x17);

  IS_IIC_WriteByte(file, Addr_GND,0x1d,0x03);//T4=520 ms
  IS_IIC_WriteByte(file, Addr_GND,0x1e,0x03);
  IS_IIC_WriteByte(file, Addr_GND,0x1f,0x03);

  IS_IIC_WriteByte(file, Addr_GND,0x20,0x03);
  IS_IIC_WriteByte(file, Addr_GND,0x21,0x03);
  IS_IIC_WriteByte(file, Addr_GND,0x22,0x03);

  IS_IIC_WriteByte(file, Addr_GND,0x23,0x03);
  IS_IIC_WriteByte(file, Addr_GND,0x24,0x03);
  IS_IIC_WriteByte(file, Addr_GND,0x25,0x03);

  IS_IIC_WriteByte(file, Addr_GND,0x26,0x00);
}

void ota_nb_1986(void)
{
   reverse_round( 0, 0, 255);
}

void ota_err_nb_1986(void)
{
   reverse_round(255, 0, 0);
}

void small_image_updating(void)
{
    reverse_round(255,255,0);
}

void small_image_update_success(void)
{
    reverse_round(0,255,0);
}

int main(int argc, char* argv[]) {
	
	long int cmd;
	long level;

	file = open_i2c_dev(2);
	if (file < 0 || check_funcs(file)) {
		printf("The LED control is not enabled\n");
		exit(1);

	}
	
	if(argc < 2) {
                usage();
	        exit(1); //no inputr parameter shows usage
        }

	cmd = strtol(argv[1], NULL, 10);

	printf("Input command is (%d)\n", cmd);

        delight_FL3199();
        switch(cmd){
		case 0:
		    usage();
		    break;
                case 1:
		    connect_wifi();
		    break;
		case 2:
		    ota_nb_1986();
		    break;
		case 3:
		    ota_err_nb_1986();
		    break;
		case 4:
		    small_image_updating();
		    break;
		case 5:
		    small_image_update_success();
		    break;
		case 6:
		    ota_err_nb_1986(); //small image upadte error sam as ota_err_nb_1986
		    break;
		case 7:
		    download();
		    break;
		case 8:
		    error();
		    break;
		default:
		    break;
	}
	close(file);
	return 0;
}
