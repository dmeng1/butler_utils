# This is butler log file parser python script
# usage log_parser input_log_file_name outputfile_name(.csv)
# This is ported from logex_parser.cpp without function change
# Tested with C2-1-Client.txt and C2-3-Client.txt files

WAKE_BEGIN = "detected < \"oknevo\" >"
MARK_HEAD  =  "BENCHMARK:"

import sys
import re
import time
from time import strftime
import sys
logBuffer = ""
numConBuffer = []
conCount = 0
marks = []
findFirst = 0
findLast = 1

title = [ "FILE NAME       ,"
         "total Process   ,"
         "wait time       ,"
         "silence detection,"
         "local cmd detection,"
         "post from client,"
         "client process  ,"
         "voice playback  ,"
         "QSE Execution   ,"
         "overhead        ,"
         "log time stamp  ,"
         "voice text      ,"
         "conversation_id ,"
]

def main():

    if len(sys.argv) > 1:
        in_file = sys.argv[1]
        print "input file:" + in_file
    else:
        print "Need input file"
        quit()
        
    if len(sys.argv) > 2:
        out_file = sys.argv[2]
        print "output file:" + out_file
    else:
        print "need output file"
        quit()
        
        
    if len(sys.argv) > 3:
        sg_verbose = 1
        
    #access buffer line in string buffer logBuffer.splitlines()[2]
    #length of list is len(myList)
    getConFromFile(in_file)
    
    
    print(conCount)
    print(numConBuffer)
    generateMarks()
    """
    print marks
    timeStamp = getTimeStamp(0, 0, "Spotted Wakewords")
    print("timestamp=",timeStamp)
    timeStamp = getTimeStamp(0, 1, "TSTMP nb_process_recorded")
    print("timestamp=",timeStamp)

    x = getDuration(0, findFirst, "TSTMP qs_ai_cloud_data_handler_process_data_b64_voice", findLast , "DONE  rtk_playback_BG_start_new <Done")
    print("voice1 %s",x)
    x = getDuration(0, findFirst, "TSTMP qs_ai_cloud_data_handler_process_data_b64_voice", findLast, "DONE  rtk_playback_func <Done") #vocie_total
    print("voice2 %s",x)
    
    x = findString(0, 0, "DONE  qs_ai_cloud_data_handler <Response Voice")
    print(x)
    x = findStringDirect(numConBuffer[0], numConBuffer[1], "Conversation ID sets to")
    print("CoverstaionID = %s" % x)
    print(title)
    """
    
    with open(out_file, "w")as f:
        for item in title:
            f.write(str(item))
        f.write("\n")
        for i in range(conCount):
            x = calcuLateData(i, in_file)
            for item in x:
                f.write(str(item)+",")
            f.write("\n")
    f.close() 
    
def getConFromFile(file):
    global conCount
    global logBuffer
    global numConBuffer
    storeFlag = 0
    i = 0

    f = open(file, "r")
    logBuffer = f.read()
    for line in logBuffer.splitlines():
        i += 1
        if re.search(WAKE_BEGIN, line):
            conCount += 1
            numConBuffer.append(i)
    numConBuffer.append(i)        # This is the last leine number in the buffer
    f.close()

def getMarkList(startIdx, endIdx):
    a_list =[]
    for i in range(startIdx, endIdx):
        if re.search(MARK_HEAD, logBuffer.splitlines()[i]):
            a_list.append(i)
    marks.append(a_list)
    
def generateMarks():
    global numConBuffer
    for i in range(0, (len(numConBuffer) - 1)):
        getMarkList(numConBuffer[i], numConBuffer[i+1])

def getTimeStamp(markListIdx, firstFlag, keyWord):
    global marks
    tmpStr = ""
    rtn = -1.0
    for i in marks[markListIdx]:
        if re.search(keyWord, logBuffer.splitlines()[i]):
            tmpStr =logBuffer.splitlines()[i].split(" ")
            if tmpStr != "":
                rtn = float(tmpStr[1])
            if firstFlag == 0:
                return rtn
    return rtn

def getDuration(markListIdx, firstFlag1, keyWord1, firstFlag2, keyWord2):
    duration = -1.0
    start = getTimeStamp(markListIdx, firstFlag1, keyWord1);
    end = getTimeStamp(markListIdx, firstFlag2, keyWord2);
    if start > 0 and end > 0:
        duration = (end - start)*1000.0
    return duration

def findString(markListIdx, firstFlag, keyWord):
    global marks
    tmpStr = ""
    for i in marks[markListIdx]:
        if re.search(keyWord, logBuffer.splitlines()[i]):
            str_1 = tmpStr =logBuffer.splitlines()[i].split(" ")
            length = len(str_1[0]) + len(str_1[2]) + 5 #5 menas there are 5 from start of the line to the content
            tmpStr =logBuffer.splitlines()[i][(length + len(keyWord)):]
            if firstFlag == 0:
                return tmpStr
    return tmpStr

def findStringDirect(startIdx, endIdx, keyWord):
    tmpStr = ""
    print(startIdx, endIdx)
    for i in range(startIdx, endIdx):
        if re.search(keyWord, logBuffer.splitlines()[i]):
            str_1 = tmpStr =logBuffer.splitlines()[i].split(" ")
            tmpStr =logBuffer.splitlines()[i][len(keyWord)+2:len(keyWord)+len(str_1[4])]
            return tmpStr
    return tmpStr    

def calcuLateData(index, inFile):
    data = []
    data.append(inFile)
    data.append(getDuration(index, findFirst, "TSTMP endpointEvent <detected voice activty ended>" , findLast ,  "DONE  qs_ai_cloud_data_handler <Response")) #e2e
    data.append(getDuration(index, findFirst, "TSTMP endpointEvent <detected voice activty ended>" , findFirst,  "START qs_ai_cloud_data_handler"))   #init wait
    data.append(getDuration(index, findFirst, "TSTMP nb_cb_listenBeginEvent <listen started>", findFirst,  "TSTMP nb_cb_endpointEvent <detected voice activty ended>")) # user_cvoice_len
    data.append(getDuration(index, findFirst, "TSTMP spot_trigger_wav_check_cmd <start check cmd>", findFirst,  "TSTMP spot_trigger_wav_check_cmd <Done check cmd>"))  #local_cmd_dtc
    data.append(getDuration(index, findFirst, "START qs_butler_cloud_post_data", findFirst, "DONE  qs_butler_cloud_post_data")) #client_post
    
    clientProcess1 = getDuration(index, findFirst, "START qs_ai_cloud_data_handler", findFirst, "DONE  qs_ai_cloud_data_handler <Response") #client_process1
    clientProcess2 = getDuration(index, findFirst, "START qs_ai_cloud_data_handler", findLast, "DONE  rtk_playback_func <Done") #client_process2
    if clientProcess2 > clientProcess1:
        clientProcess = clientProcess2
    else:
        clientProcess = clientProcess1
    data.append(clientProcess) #client_process

    voiceTotal = getDuration(index, findFirst, "TSTMP qs_ai_cloud_data_handler_process_data_b64_voice", findLast , "DONE  rtk_playback_BG_start_new <Done")
    if voiceTotal < 0.0:
        voiceTotal = getDuration(index, findFirst, "TSTMP qs_ai_cloud_data_handler_process_data_b64_voice", findLast, "DONE  rtk_playback_func <Done") #vocie_total
    data.append(voiceTotal)
    
    QseTotal1 = getDuration(index, findFirst, "START qs_ai_cloud_data_handler_process_QSE", findLast, "DONE  qs_ai_cloud_data_handler_process_QSE")
    QseTotal2 = getDuration(index, findFirst, "START qs_ai_cloud_data_handler_process_QSE", findLast, "TSTMP qsp_ir_module_blast_pattern <qsp_ir_module_blast_pattern>")
    if QseTotal1 < QseTotal2:
        QseTotal = QseTotal2
    else:
        QseTotal = QseTotal1
    data.append(QseTotal) #QSE_total
    
    processOverhead = clientProcess - QseTotal - voiceTotal;
    if processOverhead < 0:
        processOverhead = 0.0
    data.append(processOverhead)
    
    data.append(getTimeStamp(index, findFirst, "Spotted Wakewords")); #time stamp
    
    data.append(findString(index, findFirst, "DONE  qs_ai_cloud_data_handler <Response Voice")) #voice_text
    
    conversationId = findString(index, findFirst, "Conversation ID") #conversation_id;
    if conversationId == "":
        conversationId = findStringDirect(numConBuffer[index], numConBuffer[index+1], "Conversation ID sets to" )
    data.append(conversationId)
    
    return data



if __name__ == '__main__':
    main()


