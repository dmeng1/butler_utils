// logex.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*******************************************************/
#define MAX_LENGTH 512

#define WAKE_BEGIN  "detected < \"oknevo\" >"
#define MARK_HEAD    "BENCHMARK:"


/*******************************************************/


typedef struct{
    double  e2e;
    double  init_wait;
    double  user_voice_len;
    double  local_cmd_dtc;
    double  client_post;
    double  client_process;
    double  QSE_total;
    double  voice_total;
    double  process_overhead;

    double  tmp_stamp;
    char *  voice_text;
    char *  conversation_id;

}logex_data_t;

enum {
    logex_find_first,
    logex_find_last
};

/*******************************************************/

static char   sg_in_log_file[MAX_LENGTH];
static char   sg_out_log_file[MAX_LENGTH];
static FILE *       sg_in_log_fp;
static FILE *       sg_out_lfp;
static bool			sg_verbose = false;

/*******************************************************/
char *
logex_conv_find_string_direct( const char * text, const char * tag )
{
	char * str = NULL;
	const char * ptr = strstr( text, tag );
	//printf("body\n%s\n================>\n", text );
	if(ptr)
	{
		const char * beg, * end;
		ptr += strlen(tag);
		beg = strchr( ptr, '\"' );
		if( beg ){
			beg++;
			end = strchr( beg, '\"' ); 
			if( end )
			{
				size_t len = end - beg + 1;
				str = (char *)calloc( len, 1 );
				strncpy( str, beg, len -1 );				
			}
		}
	}
	return str;
}

static double 
logex_conv_find_tm ( int find_flag, const char * mark , int num_mks, char ** mks )
{
    int i;
    double rtn = -1.0;
    for ( i = 0 ; i < num_mks; i++ ) 
    {
        if (strstr(mks[i], mark)) 
        {
            char * endptr = NULL;
            char * ptr = mks[i] + (strlen(MARK_HEAD)+1);
            rtn = strtod( ptr, &endptr );
			if( sg_verbose > 1 ){
                 printf("finding time =====> found <%lf> [%s] \n", rtn, ptr );
			}
            if (find_flag == logex_find_first) {
                break; 
            }
        }
    }
    return rtn;
}
char *
logex_conv_find_string( int find_flag, const char * mark , int num_mks, char ** mks )
{
    int i;
    char * rtn = NULL;
    for ( i = 0 ; i < num_mks; i++ ) 
    {
        char * ptr = strstr(mks[i], mark);
        if ( ptr )
        {
            ptr = ptr+strlen(mark);   
			if( sg_verbose ){
				printf("find %s - [%s]\n", mark, ptr );
			}
            if (rtn) 
            {
                free(rtn);
            }
            rtn = strdup(ptr); 
			if( sg_verbose ){
				printf("found <%lf> \n", rtn );
			}
            if (find_flag == logex_find_first) {
                break; 
            }
        }
    }
    if (!rtn) {
        //rtn = strdup("nil");
    }
    return rtn; 
}

static double
logex_conv_find_duration( int num_mks, char ** mks, 
                          int flag1, const char * tag1,
                          int flag2, const char * tag2 )

{
    double rtn       = -1.0;
    double start_tm  = logex_conv_find_tm ( flag1, tag1, num_mks, mks );
    double end_tm    = logex_conv_find_tm ( flag2, tag2, num_mks, mks );
    if ( start_tm > 0 && end_tm  > 0 ) 
    {
        rtn = (end_tm - start_tm) * 1000.0;
    }
    return rtn;
}

/*******************************************************/
/*******************************************************/

static int
logex_conv_fill( logex_data_t * data, int num_mks, char ** mks )
{
    int i;

    data->tmp_stamp       = logex_conv_find_tm ( logex_find_first, "Spotted Wakewords", num_mks, mks );
   
    data->e2e             = logex_conv_find_duration(num_mks,mks, logex_find_first, "TSTMP endpointEvent <detected voice activty ended>"    ,logex_find_last ,  "DONE  qs_ai_cloud_data_handler <Response"           );
    data->init_wait       = logex_conv_find_duration(num_mks,mks, logex_find_first, "TSTMP endpointEvent <detected voice activty ended>"    ,logex_find_first,  "START qs_ai_cloud_data_handler"                     );
    data->user_voice_len  = logex_conv_find_duration(num_mks,mks, logex_find_first, "TSTMP nb_cb_listenBeginEvent <listen started>"    ,logex_find_first,  "TSTMP nb_cb_endpointEvent <detected voice activty ended>" );
    data->local_cmd_dtc   = logex_conv_find_duration(num_mks,mks, logex_find_first, "TSTMP spot_trigger_wav_check_cmd <start check cmd>"    ,logex_find_first,  "TSTMP spot_trigger_wav_check_cmd <Done check cmd>"  );
    data->client_post     = logex_conv_find_duration(num_mks,mks, logex_find_first, "START qs_butler_cloud_post_data"                       ,logex_find_first,  "DONE  qs_butler_cloud_post_data"                    );
    
	data->client_process  = logex_conv_find_duration(num_mks,mks, logex_find_first, "START qs_ai_cloud_data_handler"                        ,logex_find_first,  "DONE  qs_ai_cloud_data_handler <Response"           );
	int client_process2   = logex_conv_find_duration(num_mks,mks, logex_find_first, "START qs_ai_cloud_data_handler"                        ,logex_find_last ,  "DONE  rtk_playback_func <Done"              );
	if(client_process2 > data->client_process ){
		data->client_process = client_process2;
	}
    
	data->QSE_total       = logex_conv_find_duration(num_mks,mks, logex_find_first, "START qs_ai_cloud_data_handler_process_QSE"            ,logex_find_last ,  "DONE  qs_ai_cloud_data_handler_process_QSE"         );
	int   QSE_total2      = logex_conv_find_duration(num_mks,mks, logex_find_first, "START qs_ai_cloud_data_handler_process_QSE"            ,logex_find_last ,  "TSTMP qsp_ir_module_blast_pattern <qsp_ir_module_blast_pattern>"         );
	if(data->QSE_total < QSE_total2){
		data->QSE_total = QSE_total2; // include power delay
	}

    data->voice_total     = logex_conv_find_duration(num_mks,mks, logex_find_first, "TSTMP qs_ai_cloud_data_handler_process_data_b64_voice" ,logex_find_last ,  "DONE  rtk_playback_BG_start_new <Done"              );
	if(data->voice_total < 0 ){
		data->voice_total     = logex_conv_find_duration(num_mks,mks, logex_find_first, "TSTMP qs_ai_cloud_data_handler_process_data_b64_voice" ,logex_find_last ,  "DONE  rtk_playback_func <Done"              );
	}

    data->process_overhead= data->client_process - data->QSE_total - data->voice_total;
    if(data->process_overhead < 0)data->process_overhead = 0.0;

    data->conversation_id = logex_conv_find_string( logex_find_first, "Conversation ID", num_mks, mks ); ;
    data->voice_text      = logex_conv_find_string( logex_find_first, "DONE  qs_ai_cloud_data_handler <Response Voice", num_mks, mks ); ;
    return 0;
}


static int
logex_conv_get_MARKs( char * in_buf, int * num_mks, char *** mks )
{
    int  cnt  = 0;
    char * ptr1;
    ptr1 = in_buf;
    while (ptr1) {
        ptr1 = strstr(ptr1, MARK_HEAD);
        if (ptr1) {
            cnt++;
            ptr1 += strlen(MARK_HEAD);
        }
    }
    if ( cnt > 0 ) 
    {
        int i = 0;
        *num_mks = cnt;
        *mks = (char **) calloc(sizeof(char*), cnt);
        ptr1 = in_buf;
        while (ptr1) 
        {
            ptr1 = strstr(ptr1, MARK_HEAD);
            if (ptr1) 
            {
                size_t len = strcspn( ptr1, "\r\n" );
                (*mks)[i] = (char *)calloc( len + 1, 1);
                strncpy( (*mks)[i], ptr1, len );                
                i++;
                ptr1 += strlen(MARK_HEAD); 
            }
        }
    }
    return 0;

}

static int
logex_main_buf_extrac_convs( char * in_buf, int * num_conv_bufs, char *** conf_bufs )
{
    int  cnt  = 0;
    char * ptr1, * ptr2;
    ptr1 = in_buf;
    while (ptr1) {
        ptr1 = strstr(ptr1, WAKE_BEGIN);
        if (ptr1) {
            cnt++;
            ptr1 += strlen(WAKE_BEGIN);
        }
    }
    if ( cnt > 0 ) 
    {
        int i = 0;
        *num_conv_bufs = cnt;
        *conf_bufs = (char **) calloc(sizeof(char*), cnt);
        ptr1 = in_buf;
        while (ptr1) 
        {
            ptr1 = strstr(ptr1, WAKE_BEGIN);
            if (ptr1) {
                (*conf_bufs)[i] = ptr1;
                i++;
                if (ptr1 > in_buf) {
                    *(ptr1-1) = '\0';
                }
                ptr1 += strlen(WAKE_BEGIN); 
            }
        }
    }
    return 0;
}
static int done_first;
static int
logex_output_data( logex_data_t * data )
{
    FILE * fp = sg_out_lfp;
    if (!fp ) 
    {
        fp = stderr;
    }

    if (!done_first) {
        fprintf(fp,
                "FILE NAME       ,"
                "total Process   ,"
                "wait time       ,"
                "silence detection,"
                "local cmd detection,"
                "post from client,"
                "client process  ,"
                "voice playback  ,"
                "QSE Execution   ,"
                "overhead        ,"
                "log time stamp  ,"
                "voice text      ,"
                "conversation_id ,"
                "\n" );
        done_first++;
    }

    fprintf(fp,
            "%s,"
            "%.2lf,"
            "%.2lf,"
            "%.2lf,"
            "%.2lf,"
            "%.2lf,"
            "%.2lf,"
            "%.2lf,"
            "%.2lf,"
            "%.2lf,"
            "%.2lf,"
            "%s,"
            "%s,"
            "\n"
            , sg_in_log_file
            , data->e2e             
            , data->init_wait
            , data->user_voice_len  
            , data->local_cmd_dtc   
            , data->client_post     
            , data->client_process  
            , data->voice_total     
            , data->QSE_total       
            , data->process_overhead 
            , data->tmp_stamp
            , data->voice_text 
            , data->conversation_id
        );
	return 0;
}


static int
logex_main_buf_porcess( char * main_buf, const char * start_tag )
{
    if (main_buf) 
    {
        char * ptr1, * ptr2;
        int num_conv_bufs = 0;
        char ** conf_bufs = NULL;

        if( start_tag ){
            ptr1 = strstr( main_buf, start_tag );
        }
        else
            ptr1 = main_buf;
	
		//printf("found\n%s\ndone\n", ptr1);

        if (ptr1) {
            //printf("found\n%s\ndone\n", ptr1);
            logex_main_buf_extrac_convs( ptr1, &num_conv_bufs, &conf_bufs );
            int i; 
            for ( i = 0; i< num_conv_bufs; i++ ) {
                //printf("\n\nconv %d\n%s\n\n", i, conf_bufs[i]);
                printf("\n\nconv %d\n", i);
                int num_mks = 0;
                char ** mks = NULL;
				logex_conv_get_MARKs( conf_bufs[i], &num_mks, &mks );
                {
                    int j;
                    for (j = 0; j < num_mks; j++) {
                        // printf("%s\n", mks[j]);
                    }

                    logex_data_t data;
                    memset( &data, 0, sizeof(logex_data_t ));
                    
                    logex_conv_fill( &data, num_mks, mks );
					if( !data.conversation_id )
					{
						printf("no cov id, search direct \n" );
						data.conversation_id = logex_conv_find_string_direct( conf_bufs[i], "Conversation ID sets to" );
						printf("have 2 cov id %s\n", data.conversation_id);
					}
					else{
						printf("have cov id %s\n", data.conversation_id);
					}

                    for (j = 0; j < num_mks; j++) {
                        free( mks[j] );
                    }
                    free(mks);

                    logex_output_data( &data );

                    free(data.voice_text);
                    free(data.conversation_id);
                }
            }
            free(conf_bufs); /* free the pointer list only */
        }
    }
    return 0;
}



int
_tmain( int argc, _TCHAR* argv[] )
{
    const char * start_tag = NULL;
	size_t nNumCharConverted;
#if 1
	memset(sg_in_log_file , 0, sizeof(sg_in_log_file));
	memset(sg_out_log_file, 0, sizeof(sg_out_log_file));

    if (argc > 1) 
    {
		wcstombs_s(&nNumCharConverted, sg_in_log_file  , MAX_LENGTH, argv[1], MAX_LENGTH);
        //sg_in_log_file = argv[1];
    }
    if (argc > 2) 
    {
		wcstombs_s(&nNumCharConverted, sg_out_log_file , MAX_LENGTH, argv[2], MAX_LENGTH);
        //sg_in_log_file = argv[1];
		
        sg_out_lfp = fopen( sg_out_log_file, "r");
        if (!sg_out_lfp) {
            done_first = 0;
        }
        else{
            done_first = 1;
            fclose(sg_out_lfp);
        }

        sg_out_lfp = fopen( sg_out_log_file, "ab");

        if (!sg_out_lfp) 
        {
            fprintf(stderr,"WARNING: unable open output file [%s] \n", sg_out_log_file);
        }
		else
		{
			fprintf(stderr,"open output file %s \n", sg_out_log_file);

		}
    }

	if (argc > 3) 
	{
		sg_verbose = 1 ;
	}

    if ( sg_in_log_file ) {
        sg_in_log_fp = fopen( sg_in_log_file, "rb");
    }

    if (sg_in_log_fp ) 
    {
        char       * main_buf;
		fprintf(stderr,"open input file %s \n", sg_in_log_file);
        size_t fsz;
        fseek(sg_in_log_fp , 0, SEEK_END); // seek to end of file
        fsz = ftell(sg_in_log_fp ); // get current file pointer
        fseek(sg_in_log_fp , 0, SEEK_SET); // seek back to beginning of file

		fprintf(stderr,"open input file fsz %d \n", fsz);
        if (fsz > 0) {
            main_buf = (char *)calloc(fsz+1, 1);

            if (main_buf) 
            {
				int read_sz = 0;
				while( read_sz < fsz )
				{
					int a = fread(main_buf+read_sz, 1, fsz-read_sz, sg_in_log_fp);
					if(a <= 0 )
					{
						break;
					}
					read_sz += a;
					fprintf(stderr,"read read_sz %d \n", read_sz );
				}
#if 1
                if( read_sz == fsz )
                {
                    logex_main_buf_porcess( main_buf, start_tag );
                }
				else{
					fprintf(stderr,"unable read fsz %d \n", read_sz );
				}
#endif
            }
            free( main_buf);
        }
        fclose(sg_in_log_fp);
        sg_in_log_fp = NULL;
    }
	else{
		fprintf(stderr,"Unable open input file %s \n", sg_in_log_file);
	}


    if (sg_out_lfp ) {
        fclose(sg_out_lfp);
        sg_out_lfp = NULL;
    }
#endif
    return 0;
}

int _tmain3(int argc, _TCHAR* argv[])
{
	return 0;
}



