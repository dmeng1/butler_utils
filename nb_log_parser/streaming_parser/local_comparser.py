# This is butler streaming parser python script
# usage streaming_parser input_log_file_name outputfile_name(.csv)
# This is 
# Tested with streaming_latency_log.odt provided by Parchi on 7/8/2019

#Follwoing are the definiations from  https://universal.atlassian.net/wiki/spaces/NB/pages/543784972/Performance+profiling
# T0 Wake word detected
# T1 GRPC call started
# T2 Voice streaming started
# T3 Voice streaming ended
# T4 Got response from cloud
#T45 After T4 
# T5 Started playback voice response
# T6 First QSE execution started
# T7 Last QSE execution ended

# result data definition 
# End-To-End Wait Time                      T5 - T0 NOT USE
# End-To-End Process Time                   T7 - T0 NOT USE
# VAD delay                                 T1 - T0
# GRPC Call Establishment Time              T2 - T1
# Voice Streaming Time                      T3 - T2
# Cloud Process + Network Transport Time    T4 - T3
# Response Message Process Time             T5 - T NOT USE
# QSE Execution Time                        T7 - T6 NOT USE
# QSE Process Time                          T7 - T4 NOT USE
# Latency                                   T45 - T4
# Above including message processing and QSE execution

T00 = "TSTMP nb_cb_listenBeginEvent2"
T0 = "Spotted Wakewords"
T1 = "time\(T1\)"
T2 = "time\(T2\)"
T3 = "time\(T3\)"
T4 = "time\(T4\)"
T45 = "START qs_ai_cloud_data_handler_process_"
T451 = "TSTMP qs_ai_cloud_data_handler_process_"
T5 = "<T5>"
T6 = "<T6>"
T7 = "<T7>"
CONID = "conversation id"
DEVICEID = "device id"
SEQID = "\[DEBUG\]\[id"
SPOTTED_LOCAL = "Spotted local" 

import sys
import re
import time
from time import strftime
import sys

logBuffer = ""
t0Idx = []
t4Idx = 0

title = [ "FILE NAME,"
          "device id,"
          "conversation id,"
          "sequence id,"
          "Local Command,"
          "Local Command time stamp,"
          "T0,"
          "T05,"
          "T1,"
          "T2,"
          "T3,"
          "T4,"
          "T45,"
          "VAD Delay,"
          "GRPC Call Establishment time,"
          "Voice Streaming time,"
          "Cloud Process + Network Transport time,"
          "Latency"
]

def main():
    global t4Idx
    in_file = ""
    if len(sys.argv) > 1:
        in_file = sys.argv[1]
        print("input file:", in_file)
    else:
        print("Need input file")
        quit()
        
    if len(sys.argv) > 2:
        out_file = sys.argv[2]
        print("output file:",out_file)
    else:
        print ("need output file")
        quit()
        
        
    if len(sys.argv) > 3:
        sg_verbose = 1
        
    getLogBufferFromFile(in_file)
    
    getT0Idx();
    print(t0Idx)
   
    with open(out_file, "w")as f:
        for item in title:
            f.write(str(item))
        f.write("\n")
        calculation(f, in_file)  
    f.close()
 
def calculation(fp, infile):
    k = 1
    for i in range(len(t0Idx) - 1):
        a_list = []
        
        print("Parsing Session:",k)
              
        a_list.append(infile)
        
        did = getDviceId(t0Idx[i], t0Idx[i+1])
        a_list.append(did)
        
        cid = getConId(t0Idx[i], t0Idx[i+1])
        a_list.append(cid)

        a_list.append(getSeqId(t0Idx[i], t0Idx[i+1]))
        
        a_list.append(getLocalCommand(t0Idx[i], t0Idx[i+1]))
        
        a_list.append(getLocalCommandTimeStamp(t0Idx[i], t0Idx[i+1]))
        
        if i == 0:
            t0 = getT00(t0Idx[i], 0)
        else:
            t0 = getT00(t0Idx[i], t0Idx[i-1])
            

        a_list.append(t0)
       

        t05 = getT05TimeStamp(t0Idx[i])
        a_list.append(t05)

        t1 = getTsFromEpoch(T1, t0Idx[i], t0Idx[i+1])
        a_list.append(t1)
        
        t2 = getTsFromEpoch(T2,t0Idx[i], t0Idx[i+1])
        
        a_list.append(t2)
        
        t3 = getTsFromEpoch(T3,t0Idx[i], t0Idx[i+1])

        a_list.append(t3)
    
        t4 = getT4FromEpoch(t0Idx[i], t0Idx[i+1])

        a_list.append(t4)      

        t45 = getT45(t4Idx, t0Idx[i+1]);

        a_list.append(t45)

        if( t1 > 0 and t0 > 0 and t1 >= t0):                
            VADelay = t1 -t0
        else:
            VADelay = ""
        a_list.append(VADelay)
        
        if( t1 > 0 and t2 > 0 and t2 > t1):
            GRPC = t2 - t1
        else:
            GRPC = ""
        a_list.append(GRPC)

        if( t3 > 0 and t2 > 0 and t3 > t2):
            voiceTime = t3 - t2
        else:
            voiceTime = ""
        a_list.append(voiceTime)
        
        if( t4 > 0 and t3 > 0 and t4 > t3):
            cloudNetwork = t4 -t3
        else:
            cloudNetwork = ""
        a_list.append(cloudNetwork)

        if( t45 > 0 and t4 > 0 and t45 > 4):
            latency = t45 - t4
        else:
            latency = ""
        a_list.append(latency)
        
        for item in a_list:
                fp.write(str(item)+",")
        fp.write("\n")
        
        k = k + 1
        
def getSeqId(startIdx, endIdx):
    sid = ""
    global totalLines
    global logBuffer
    for i in range(startIdx, endIdx):
        line = logBuffer.splitlines()[i]
        if(re.search(SEQID,line)):
            tmpStr =line.split()
            if(len(tmpStr) > 1):
                length = len(tmpStr[1])
                sid = tmpStr[1][:(length-1)]
                break
    return sid;

def getConId(startIdx, endIdx):
    cid = ""
    global totalLines
    global logBuffer
    for i in range(startIdx, endIdx):
        line = logBuffer.splitlines()[i]
        if(re.search(CONID,line)):
            tmpStr =line.split()
            if(len(tmpStr) > 2):    
                cid = tmpStr[2]
                break
    return cid;

def getDviceId(startIdx, endIdx):
    did = ""
    global totalLines
    global logBuffer
    for i in range(startIdx, endIdx):
        line = logBuffer.splitlines()[i]
        if(re.search(DEVICEID,line)):
            tmpStr =line.split()
            if(len(tmpStr) > 2):    
                did = tmpStr[2]
                break
    return did;

def getTimeStamp(line):
    tmpStr = ""
    rtn = -1.0
    tmpStr =line.split()
    if tmpStr != "":
        if(is_number(tmpStr[1])):
            rtn = float(tmpStr[1])
    return rtn

def getTimeStampFromEpoch(line):
    tmpStr = ""
    rtn = -1.0
    tmpStr =line.split()
    if tmpStr != "" and len(tmpStr) > 3:
        inStr = tmpStr[3]    
        length = len(inStr)
        string = inStr[(length - 7):(length - 3)] + "." + inStr[(length - 3):]
    if(is_number(string)):
        rtn = float(string)
    return rtn
    
def getLogBufferFromFile(file):
    global logBuffer
    global totalLines
    f = open(file, "r")
    logBuffer = f.read()
    totalLines = len(logBuffer.splitlines());
    f.close()
    
def getTs(keyWord, startIdx, endIdx):
    timeStamp = -1.0
    global totalLines
    global logBuffer
    for i in range(startIdx, endIdx):
        line = logBuffer.splitlines()[i]
        if(re.search(keyWord,line)):
            timeStamp = getTimeStamp(line)
            break
    return timeStamp;

def getT45(startIdx, endIdx):
    timeStamp = -1.0
    global totalLines
    global logBuffer
    for i in range(startIdx, endIdx):
        line = logBuffer.splitlines()[i]
        if(re.search(T45,line) or re.search(T451,line)):
            timeStamp = getTimeStamp(line)
            break
    return timeStamp;

#getTsFromEpoch function is used to get T1, T2, T3 time stamp
def getTsFromEpoch(keyWord, startIdx, endIdx):
    timeStamp = -1.0
    global totalLines
    global logBuffer

    for i in range(startIdx + 1 , endIdx - 1):
        line = logBuffer.splitlines()[i]
        if(re.search(keyWord,line)):
            timeStamp = getTimeStampFromEpoch(line)
            break
    return timeStamp;

def getT4StampFromEpoch(line):
    tmpStr = ""
    rtn = -1.0
    tmpStr =line.split()
    if tmpStr != "" and len(tmpStr) > 3:
        inStr = tmpStr[3]
        length = len(inStr)
        inStr = inStr[:(length - 35)]
        length = len(inStr)
        string = inStr[(length - 7):(length - 3)] + "." + inStr[(length - 3):]
    else:
        return rtn
    
    if(is_number(string)):    
        rtn = float(string)
    return rtn

#getT4FromEpoch function is used to get T4 time stamp
def getT4FromEpoch(startIdx, endIdx):
    timeStamp = -1.0
    global totalLines
    global logBuffer
    global t4Idx

    for i in range(startIdx + 1 , endIdx - 1):
        line = logBuffer.splitlines()[i]
        if(re.search(T4,line)):
            timeStamp = getT4StampFromEpoch(line)
            t4Idx = i
            break
    return timeStamp;
#T0 now is defined as T45.  It is need to displayed on spread sheet
def getT0Idx():
    global totalLines
    global logBuffer
    global t0Idx
    j = 0
    for i in range(0, totalLines - 1):
        line = logBuffer.splitlines()[i]
        if(re.search(T0,line)):
            t0Idx.append(i)            
        j = j + 1
    t0Idx.append(j+1);


def getT05TimeStamp(idx):
    line = logBuffer.splitlines()[idx]
    timeStamp = getTimeStamp(line)
    return timeStamp

            
def getT0TimeStamp(line):
    tmpStr = ""
    rtn = -1.0
    tmpStr =line.split()
    if tmpStr != "":
        if tmpStr[1] == "BENCHMARK:":
            if(is_number(tmpStr[2])):
                rtn = float(tmpStr[2])
        else:
            if(is_number(tmpStr[1])):
                rtn = float(tmpStr[1])
    return rtn


#T00 is dipalyed as T0 on the spred sheet 
def getT00(start, end):
    timeStamp = -1
    
    if start < 0 :
        return timeStamp
    end = end -1 
    for i in range(start, end, -1):
        line = logBuffer.splitlines()[i]
        if(re.search(T00,line)):
            timeStamp = getT0TimeStamp(line)
            break
    return timeStamp;

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        pass
    return False

def finLocalComamndIdx(start, end):
    idx = -1
    for i in range(start, end):
        line = logBuffer.splitlines()[i]
        if(re.search(SPOTTED_LOCAL,line)):
            idx = i;
            break
    return idx

def getLocalCommand(start, end):
    idx = finLocalComamndIdx(start, end)
    line = logBuffer.splitlines()[idx]
    tmpStr =line.split()
    localCommand = ""
   
    if(len(tmpStr) > 2):    
        localCommand = tmpStr[2]
        if localCommand[-1] == ",":
           localCommand = localCommand[:-1]
       
    return localCommand

def getLocalCommandTimeStamp(start, end):
    tmpStr = ""
    rtn = -1.0
    idx = finLocalComamndIdx(start, end)
    if(idx > 0):
        idx = idx -1
    else:
        return rtn;
                    
    line = logBuffer.splitlines()[idx]
    tmpStr =line.split()
    if tmpStr != "":
        if tmpStr[0] == "BENCHMARK:":
            if(is_number(tmpStr[1])):
                rtn = float(tmpStr[1])
    return rtn

if __name__ == '__main__':
    main()


