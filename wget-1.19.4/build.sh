#!/bin/bash

VERSION="1.19.4"
PWD=$(pwd)
export TOOLCHAIN_DIR="/opt/toolchain/msdk-4.9.4-mips-EL-3.10-u0.9.33-m32ut-171003"
export TOOLCHAIN_BIN="${TOOLCHAIN_DIR}/bin"
export TOOLCHAIN_PRX="mipsel-linux-uclibc"
export CROSS_COMPILE="${TOOLCHAIN_BIN}/${TOOLCHAIN_PRX}"
export CC=${CROSS_COMPILE}-gcc

if [ "$1" = "1" ]
then
	echo "Build dynamic wget"
	export CFLAGS="-ffunction-sections -fdata-sections -I${PWD}/nevo_include -I${PWD}/nevo_include/openssl"
	export CPPFLAGS="-I${PWD}/nevo_include"
	export LDFLAGS="-L${PWD}/nevo_lib"
	export LIBS="-lc -lssl -lcrypto -lz -ldl"
else
	echo "Build static wget"
	export CFLAGS="-ffunction-sections -fdata-sections -I${PWD}/sdk_include -I${PWD}/sdk_inlcude/openssl"
	export CPPFLAGS="-I${PWD}/sdk_include"
	export LDFLAGS="-L${PWD}/sdk_lib"
#	export LIBS="-static -lc -lssl -lcrypto -lz -ldl"
	export LIBS="-lc -lssl -lcrypto -lz -ldl"
fi

echo $LIBS

#./configure \
#	--host=mips-linux \
#	--with-ssl=openssl --with-zlib 
./configure --host=mips-linux-uclib --with-ssl=openssl --with-libssl-prefix=${PWD}/tempfs --with-zlib 
make clean; make
